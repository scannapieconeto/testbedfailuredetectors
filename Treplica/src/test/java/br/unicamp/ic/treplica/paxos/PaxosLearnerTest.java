/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.ArrayList;
import java.util.LinkedList;

import br.unicamp.ic.treplica.TestStateManager;
import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.common.TestChangeLog;
import br.unicamp.ic.treplica.common.TestTransport;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import junit.framework.TestCase;

public class PaxosLearnerTest extends TestCase {

    private static byte[] APP_MESSAGE_1 = { 23 };
    private static byte[] APP_MESSAGE_2 = { 42 };
    private static byte[] APP_MESSAGE_3 = { 69 };
    private static byte[][] APP_MESSAGES_1 = { APP_MESSAGE_1 };
    private static byte[][] APP_MESSAGES_2 = { APP_MESSAGE_2 };
    private static byte[][] APP_MESSAGES_3 = { APP_MESSAGE_3 };

    private TestTransport t;
    private LinkedList<Delivery> q;
    private Secretary s;
    private Ledger l;

    @Override
    protected void setUp() throws TreplicaException {
	t = new TestTransport();
	q = new LinkedList<Delivery>();
	s = new Secretary(new TestChangeLog(), t, q);
	s.bind(10, new TestStateManager());
	l = s.getLedger();
    }

    public void testVotedCountingSameProposal() {
	LearnerBallot b = new LearnerBallot();
	Proposal p = new Proposal(1, APP_MESSAGES_1);

	assertEquals(0, b.receivedVotes());
	assertEquals(0, b.getWinningQuorum());
	b.registerVoted(2, p);
	assertEquals(1, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	b.registerVoted(2, p);
	assertEquals(1, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	b.registerVoted(3, p);
	assertEquals(2, b.receivedVotes());
	assertEquals(2, b.getWinningQuorum());
	assertEquals(p, b.getWinningProposal());
    }

    public void testVotedCountingSameNullProposal() {
	LearnerBallot b = new LearnerBallot();

	assertEquals(0, b.receivedVotes());
	assertEquals(0, b.getWinningQuorum());
	b.registerVoted(2, null);
	assertEquals(1, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	b.registerVoted(2, null);
	assertEquals(1, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	b.registerVoted(3, null);
	assertEquals(2, b.receivedVotes());
	assertEquals(2, b.getWinningQuorum());
	assertNull(b.getWinningProposal());
    }

    public void testVotedCountingTwoProposals() {
	LearnerBallot b = new LearnerBallot();
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);

	assertEquals(0, b.receivedVotes());
	assertEquals(0, b.getWinningQuorum());
	b.registerVoted(2, p1);
	assertEquals(1, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	assertEquals(p1, b.getWinningProposal());
	b.registerVoted(3, p2);
	assertEquals(2, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	Proposal p = b.getWinningProposal();
	assertTrue(p.equals(p1) || p.equals(p2));
	assertEquals(p, b.getWinningProposal());
	b.registerVoted(4, p2);
	assertEquals(3, b.receivedVotes());
	assertEquals(2, b.getWinningQuorum());
	assertEquals(p2, b.getWinningProposal());
    }

    public void testVotedCountingTwoProposalsWithNull() {
	LearnerBallot b = new LearnerBallot();
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);

	assertEquals(0, b.receivedVotes());
	assertEquals(0, b.getWinningQuorum());
	b.registerVoted(2, p1);
	assertEquals(1, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	assertEquals(p1, b.getWinningProposal());
	b.registerVoted(3, null);
	assertEquals(2, b.receivedVotes());
	assertEquals(1, b.getWinningQuorum());
	Proposal p = b.getWinningProposal();
	assertTrue(p == null || p.equals(p1));
	assertEquals(p, b.getWinningProposal());
	b.registerVoted(4, null);
	assertEquals(3, b.receivedVotes());
	assertEquals(2, b.getWinningQuorum());
	assertEquals(null, b.getWinningProposal());
    }

    /*
     * Counts voted messages and discover decrees are passed for classic
     * ballots. The first for decree 0, then decrees 2 and 1, out of order.
     * Checks to see if the gaps are handled correctly.
     */
    public void testVotedClassic1() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextClassicBallotNumber();
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(0));
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	ballot = l.generateNextClassicBallotNumber();
	p = new Proposal(2, APP_MESSAGES_2);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 2, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 2, ballot, null, new Proposal(2, null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 2, ballot, null, new Proposal(2, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(2));
	assertEquals(0, q.size());

	ballot = l.generateNextClassicBallotNumber();
	p = new Proposal(3, APP_MESSAGES_3);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 1, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 1, ballot, null, new Proposal(3, null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 1, ballot, null, new Proposal(3, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(1));
	assertNotNull(l.read(2));
	assertEquals(2, q.size());
	d = q.removeFirst();
	assertEquals(1, d.getDecree());
	assertEquals(APP_MESSAGE_3[0], d.getMessage()[0]);
	d = q.removeFirst();
	assertEquals(2, d.getDecree());
	assertEquals(APP_MESSAGE_2[0], d.getMessage()[0]);
    }

    /*
     * Counts voted messages and discover decrees are passed for fast ballots.
     * The first for decree 0, then decrees 2 and 1, out of order. Checks to see
     * if the gaps are handled correctly.
     */
    public void testVotedFast1() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextFastBallotNumber();
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(0));
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	ballot = l.generateNextFastBallotNumber();
	p = new Proposal(2, APP_MESSAGES_2);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 2, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 2, ballot, null, new Proposal(2, null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 2, ballot, null, new Proposal(2, null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 2, ballot, null, new Proposal(2, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(2));
	assertEquals(0, q.size());

	ballot = l.generateNextFastBallotNumber();
	p = new Proposal(3, APP_MESSAGES_3);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 1, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 1, ballot, null, new Proposal(3, null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 1, ballot, null, new Proposal(3, null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 1, ballot, null, new Proposal(3, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(1));
	assertNotNull(l.read(2));
	assertEquals(2, q.size());
	d = q.removeFirst();
	assertEquals(1, d.getDecree());
	assertEquals(APP_MESSAGE_3[0], d.getMessage()[0]);
	d = q.removeFirst();
	assertEquals(2, d.getDecree());
	assertEquals(APP_MESSAGE_2[0], d.getMessage()[0]);
    }

    /*
     * Tests if voted messages are kept track correctly for classic ballots.
     * Duplicated voted messages are sent and should be properly ignored.
     */
    public void testVotedClassic2() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextClassicBallotNumber();
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());

	learner.processMessage(
		new PaxosMessage(4, PaxosMessage.VOTED, 0, l.generateNextClassicBallotNumber(), null, null));
	s.synchronousFlush();
	assertEquals(0, q.size());

	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(0));
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	learner.processMessage(
		new PaxosMessage(4, PaxosMessage.VOTED, 0, l.generateNextClassicBallotNumber(), null, null));
	s.synchronousFlush();
	assertEquals(0, q.size());
    }

    /*
     * Tests if voted messages are kept track correctly for fast ballots.
     * Duplicated voted messages are sent and should be properly ignored.
     */
    public void testVotedFast2() {
	Learner learner = new Learner(s, 5, 3, 4, 1000, 0);

	BallotNumber ballot = l.generateNextFastBallotNumber();
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p1);
	learner.processMessage(m);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	m = new PaxosMessage(2, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p2);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	learner.processMessage(m);
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());

	m = new PaxosMessage(4, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	m = new PaxosMessage(5, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	m = new PaxosMessage(6, PaxosMessage.VOTED, 0, ballot, null, new Proposal(2, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());

	learner.processMessage(
		new PaxosMessage(4, PaxosMessage.VOTED, 0, l.generateNextClassicBallotNumber(), null, null));
	s.synchronousFlush();
	assertEquals(0, q.size());

	m = new PaxosMessage(7, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p1, l.read(0));
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	learner.processMessage(
		new PaxosMessage(4, PaxosMessage.VOTED, 0, l.generateNextClassicBallotNumber(), null, null));
	s.synchronousFlush();
	assertEquals(0, q.size());
    }

    /*
     * Tracks voted messages with the null proposal, checks to see if the ledger
     * is updated and the message is ignored.
     */
    public void testVotedClassicNullProposal() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextClassicBallotNumber();
	PaxosMessage m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertNull(l.read(0));
	assertEquals(0, q.size());
    }

    /*
     * Tracks voted messages with the null proposal in a fast ballot, checks to
     * see if the ledger is updated and the message is ignored.
     */
    public void testVotedFastNullProposal() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextFastBallotNumber();
	PaxosMessage m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertFalse(l.isDecided(0));
	m = new PaxosMessage(4, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertNull(l.read(0));
	assertEquals(0, q.size());
    }

    /*
     * Counts voted messages and discovers a collision. Checks to see if nothing
     * is done about it.
     */
    public void testVotedFastCollision() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextFastBallotNumber();
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p1);
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p2);
	learner.processMessage(m);
	PaxosMessage m1 = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(1, null));
	PaxosMessage m2 = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(2, null));
	learner.processMessage(m1);
	learner.processMessage(m2);
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Processes voted messages for proposals this learner knows nothing about.
     */
    public void testVotedClassicUnknownProposal() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextClassicBallotNumber();
	Proposal p = new Proposal(23, null);
	PaxosMessage m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertFalse(l.isDecided(0));
	assertEquals(0, q.size());

	p = new Proposal(23, APP_MESSAGES_1);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p);
	learner.processMessage(m);
	for (byte i = 0; i < 18; i++) {
	    p = new Proposal(i, new byte[][] { { i } });
	    m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, i + 1, ballot, null, p);
	    learner.processMessage(m);
	}
	p = new Proposal(23, null);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertFalse(l.isDecided(0));
	assertEquals(0, q.size());
    }

    /*
     * Processes voted messages in a fast ballot for proposals this learner
     * knows nothing about.
     */
    public void testVotedFastUnknownProposal() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	BallotNumber ballot = l.generateNextFastBallotNumber();
	Proposal p = new Proposal(23, null);
	PaxosMessage m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(4, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertFalse(l.isDecided(0));
	assertEquals(0, q.size());

	p = new Proposal(23, APP_MESSAGES_1);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p);
	learner.processMessage(m);
	for (byte i = 0; i < 18; i++) {
	    p = new Proposal(i, new byte[][] { { i } });
	    m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, i + 1, ballot, null, p);
	    learner.processMessage(m);
	}
	p = new Proposal(23, null);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(4, PaxosMessage.VOTED, 0, ballot, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertFalse(l.isDecided(0));
	assertEquals(0, q.size());
    }

    /*
     * Sends three success messages, the first for decree 0, then decrees 2 and
     * 1, out of order. Checks to see if the gaps are handled correctly and a
     * repeated message is ignored.
     */
    public void testSuccess() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	Proposal p = new Proposal(1, APP_MESSAGES_1);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(0));
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	p = new Proposal(2, APP_MESSAGES_2);
	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 2, null, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(2));
	assertEquals(0, q.size());

	p = new Proposal(3, APP_MESSAGES_3);
	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 1, null, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(p, l.read(1));
	assertNotNull(l.read(2));
	assertEquals(2, q.size());
	d = q.removeFirst();
	assertEquals(1, d.getDecree());
	assertEquals(APP_MESSAGE_3[0], d.getMessage()[0]);
	d = q.removeFirst();
	assertEquals(2, d.getDecree());
	assertEquals(APP_MESSAGE_2[0], d.getMessage()[0]);

	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());
    }

    /*
     * Sends a success with the null proposal, checks to see if the ledger is
     * updated and the message is ignored.
     */
    public void testSuccessNullProposal() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertNull(l.read(0));
	assertEquals(0, q.size());
    }

    /*
     * Sends a success message for decree 1, waits for a timeout and checks if
     * pass messages are sent for the gaps.
     */
    public void testGapTimeout() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	Proposal p = new Proposal(1, APP_MESSAGES_1);
	PaxosMessage m = new PaxosMessage(1, PaxosMessage.SUCCESS, 1, null, null, p);
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(1));
	assertEquals(0, q.size());

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertNull(m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Sees if the messages required to order a proposal are sent for a classic
     * ballot. Sends a success message and sees if the proposal is delivered.
     * Waits for a timeout and checks that nothing else happened.
     */
    public void testOrderSuccessClassic() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());

	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, m.getProposal());
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertEquals(APP_MESSAGE_1[0], l.read(0).getMessages()[0][0]);
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(0, q.size());
	assertNull(t.popMemory());
    }

    /*
     * Sees if the messages required to order a proposal are sent for a fast
     * ballot. Sends a success message and sees if the proposal is delivered.
     * Waits for a timeout and checks that nothing else happened.
     * 
     * This case can't happen in practice because an acceptor will never send a
     * SUCCESS message.
     */
    public void testOrderSuccessFast() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());

	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, m.getProposal());
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertEquals(APP_MESSAGE_1[0], l.read(0).getMessages()[0][0]);
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(0, q.size());
	assertNull(t.popMemory());
    }

    /*
     * Sees if the messages required to order a proposal are sent for a classic
     * ballot. Sends voted messages for a classic ballot and sees if the
     * proposal is delivered. Waits for a timeout and checks that nothing else
     * happened.
     */
    public void testOrderClassicBallot() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());

	Proposal p = m.getProposal();
	BallotNumber ballot = l.generateNextClassicBallotNumber();
	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.BEGIN_BALLOT, 0, ballot, null, p);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertEquals(APP_MESSAGE_1[0], l.read(0).getMessages()[0][0]);
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(0, q.size());
	assertNull(t.popMemory());
    }

    /*
     * Sees if the messages required to order a proposal are sent for a fast
     * ballot. Sends voted messages for a fast ballot and sees if the proposal
     * is delivered. Waits for a timeout and checks that nothing else happened.
     */
    public void testOrderFastBallot() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
	learner.processMessage(m);

	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(m.getProposal().getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(m.getProposal().getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertEquals(0, q.size());
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(m.getProposal().getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(0));
	assertEquals(APP_MESSAGE_1[0], l.read(0).getMessages()[0][0]);
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(0, q.size());
	assertNull(t.popMemory());
    }

    /*
     * Sees if the messages required to order a proposal are sent after a
     * correct choice of classic or fast ballots. In this case no ANY message
     * was received.
     */
    public void testOrderFastOrClassic1() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
    }

    /*
     * Sees if the messages required to order a proposal are sent after a
     * correct choice of classic or fast ballots. In this case an appropriate
     * ANY message was received.
     */
    public void testOrderFastOrClassic2() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertNull(t.popMemory());

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
    }

    /*
     * Sees if the messages required to order a proposal are sent after a
     * correct choice of classic or fast ballots. In this case an ANY message
     * was received but only to a future decree.
     */
    public void testOrderFastOrClassic3() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 10, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertNull(t.popMemory());

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
    }

    /*
     * Sees if the messages required to order a proposal are sent after a
     * correct choice of classic or fast ballots. In this case the ANY message
     * received contains a smaller ballot that the next ballot inactive and
     * should be answered with a LARGER_BALLOT message.
     */
    public void testOrderFastOrClassic4() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot1 = l.generateNextFastBallotNumber();
	BallotNumber ballot2 = l.generateNextFastBallotNumber();
	l.setInactiveNextBallot(ballot2);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot1, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.LARGER_BALLOT, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertEquals(ballot2, m.getBallotNumber());
	assertNull(m.getProposal());
	assertNull(m.getVote());

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
    }

    /*
     * Sees if the messages required to order a proposal are sent after a
     * correct choice of classic or fast ballots. In this case if two ANY
     * messages are received, only the one with the larger ballot should count
     * the other should be answered with a LARGER_BALLOT message.
     */
    public void testOrderFastOrClassic5() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot1 = l.generateNextFastBallotNumber();
	BallotNumber ballot2 = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot2, null, null);
	learner.processMessage(m);
	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot1, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.LARGER_BALLOT, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertEquals(ballot2, m.getBallotNumber());
	assertNull(m.getProposal());
	assertNull(m.getVote());

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(ballot2, m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
    }

    /*
     * Sees if the messages required to order a proposal are sent after a
     * correct choice of classic or fast ballots. In this case if two identical
     * ANY messages are received, the second is ignored.
     */
    public void testOrderFastOrClassic6() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);
	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	assertNull(t.popMemory());

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(APP_MESSAGE_1[0], m.getProposal().getMessages()[0][0]);
	assertNull(m.getVote());
    }

    /*
     * Tries to order two proposals before any of the two is delivered for a
     * classic ballot.
     */
    public void testDoubleOrderClassic() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());

	learner.order(APP_MESSAGE_2);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(1, m.getDecreeNumber());
    }

    /*
     * Tries to order two proposals before any of the two is delivered for a
     * fast ballot.
     */
    public void testDoubleOrderFast() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());

	learner.order(APP_MESSAGE_2);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(1, m.getDecreeNumber());
    }

    /*
     * Tries to order a proposal in a classic ballot, waits for a timeout and
     * checks if the messages are sent again.
     */
    public void testOrderTimeoutClassic() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Tries to order a proposal in a fast ballot, waits for a timeout and
     * checks if a PASS message is sent.
     */
    public void testOrderTimeoutFast() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Tries to order a proposal in a classic ballot, checks if nothing happens
     * before the timeout.
     */
    public void testOrderEarlyTimeoutClassic() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());

	learner.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Tries to order a proposal in a fast ballot, checks if nothing happens
     * before the timeout.
     */
    public void testOrderEarlyTimeoutFast() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());

	learner.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Tries to order a proposal as decree 0 in a classic ballot. Receives a
     * success message for decree 1, waits for a timeout and checks if only the
     * gap timeout fires.
     */
    public void testOrderAndGapTimeout() throws InterruptedException {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	Proposal p = m.getProposal();

	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 1, null, null, new Proposal(1, APP_MESSAGES_2));
	learner.processMessage(m);
	s.synchronousFlush();
	assertTrue(l.isDecided(1));
	assertEquals(0, q.size());

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
	assertNull(t.popMemory());
    }

    /*
     * Tries to order a proposal at 0, activates decree 1, decides a different
     * proposal for decree 0, checks if a new message is sent requesting a new
     * decree number (2). Decides the wrong proposal at 2 before deciding 1,
     * checks if a new message is sent requesting a new decree number (3).
     */
    public void testOrderDecreeTakenClassic1() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();

	l.setNextBallot(1, null);
	Proposal otherP = new Proposal(1, APP_MESSAGES_2);
	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, otherP);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(2, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());

	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 2, null, null, otherP);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(3, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Tries to order a proposal at 0, activates decree 1, decides a different
     * proposal for decree 0, checks if a new message is sent requesting a new
     * decree number (2). Decides the wrong proposal at 2 before deciding 1,
     * checks if a new message is sent requesting a new decree number (3).
     */
    public void testOrderDecreeTakenFast1() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();

	l.setNextBallot(1, null);
	Proposal otherP = new Proposal(1, APP_MESSAGES_2);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, otherP);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(2, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());

	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 2, ballot, null, otherP);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 2, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 2, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 2, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(3, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Tries to order a proposal at 0, decides the null proposal for decree 0,
     * checks if a new message is sent requesting a new decree number (1).
     */
    public void testOrderDecreeTakenClassic2() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();

	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(1, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Tries to order a proposal at 0, decides the null proposal for decree 0,
     * checks if a new message is sent requesting a new decree number (1).
     */
    public void testOrderDecreeTakenFast2() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();

	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(1, m.getDecreeNumber());
	assertEquals(ballot, m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Sees if the PASS message is sent if a leaner discovers a collision for a
     * proposal it created. A collision can only happen in a fast ballot.
     */
    public void testOrderCollisionFast() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(APP_MESSAGE_1);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	Proposal p = m.getProposal();
	learner.processMessage(m);

	PaxosMessage m1 = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(p.getId(), null));
	PaxosMessage m2 = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, null);
	learner.processMessage(m1);
	s.synchronousFlush();
	assertNull(t.popMemory());
	learner.processMessage(m2);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertEquals(p, m.getProposal());
	assertNull(m.getVote());

	assertEquals(0, q.size());
	assertNull(t.popMemory());
    }

    /*
     * Tests is an order call fails fast with NullPointerException.
     */
    public void testOrderNull() {
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);
	try {
	    learner.order(null);
	    fail("NullPointerException expected");
	} catch (NullPointerException e) {
	}
    }

    /*
     * Creates a ledger with two decided decrees (0 and 2) and a hole (1).
     * Checks if the timed deliver and resend are working.
     */
    public void testRestoredLedger() throws InterruptedException {
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	l.write(0, p1);
	l.write(2, p2);
	Learner learner = new Learner(s, 3, 2, 3, 10, 0);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(0, d.getDecree());
	assertEquals(APP_MESSAGE_1[0], d.getMessage()[0]);

	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(1, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertNull(m.getProposal());
	assertNull(m.getVote());
    }

    /*
     * Creates a ledger with three decided decrees (0, 1 and 2) and starts the
     * learner with a specific next to deliver.
     */
    public void testRestoredLedgerNextToDeliver1() throws InterruptedException {
	l.write(0, new Proposal(1, APP_MESSAGES_1));
	l.write(1, new Proposal(2, APP_MESSAGES_2));
	l.write(2, new Proposal(3, APP_MESSAGES_3));
	Learner learner = new Learner(s, 3, 2, 3, 10, 1);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(2, q.size());
	Delivery d = q.removeFirst();
	assertEquals(1, d.getDecree());
	assertEquals(APP_MESSAGE_2[0], d.getMessage()[0]);
	d = q.removeFirst();
	assertEquals(2, d.getDecree());
	assertEquals(APP_MESSAGE_3[0], d.getMessage()[0]);
	assertNull(t.popMemory());
    }

    /*
     * Creates a ledger with three decided decrees (0, 1 and 2), where the
     * decree 1 is a null proposal, and starts the learner with a specific next
     * to deliver.
     */
    public void testRestoredLedgerNextToDeliver2() throws InterruptedException {
	l.write(0, new Proposal(1, APP_MESSAGES_1));
	l.write(1, null);
	l.write(2, new Proposal(3, APP_MESSAGES_3));
	Learner learner = new Learner(s, 3, 2, 3, 10, 1);

	Thread.sleep(15);
	learner.processTick();
	s.synchronousFlush();
	assertEquals(1, q.size());
	Delivery d = q.removeFirst();
	assertEquals(2, d.getDecree());
	assertEquals(APP_MESSAGE_3[0], d.getMessage()[0]);
	assertNull(t.popMemory());
    }

    /*
     * Tries to order 3 messages before any of them is delivered for a classic
     * ballot. The 3rd message should wait until any of the 2 first is delivered
     * to start.
     */
    public void testCongestionClassic() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	learner.order(new byte[] { 0 });
	learner.order(new byte[] { 1 });
	s.synchronousFlush();
	ArrayList<Proposal> proposals = new ArrayList<Proposal>();
	for (int i = 0; i < 2; i++) {
	    PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	    assertEquals(PaxosMessage.PASS, m.getType());
	    assertEquals(i, m.getDecreeNumber());
	    assertEquals(i, m.getProposal().getMessages()[0][0]);
	    proposals.add(m.getProposal());
	}

	learner.order(new byte[] { 2 });
	s.synchronousFlush();
	assertNull(t.popMemory());

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.SUCCESS, 1, null, null, proposals.get(1));
	learner.processMessage(m);
	s.synchronousFlush();
	assertNull(t.popMemory());

	Proposal otherP = new Proposal(1, APP_MESSAGES_1);
	m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, otherP);
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(2, m.getDecreeNumber());
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(3, m.getDecreeNumber());
	assertEquals(2, m.getProposal().getMessages()[0][0]);
    }

    /*
     * Tries to order 3 messages before any of them is delivered for a fast
     * ballot. The 3rd message should wait until any of the 2 first is delivered
     * to start.
     */
    public void testCongestionFast() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(new byte[] { 0 });
	learner.order(new byte[] { 1 });
	s.synchronousFlush();
	ArrayList<Proposal> proposals = new ArrayList<Proposal>();
	for (int i = 0; i < 2; i++) {
	    m = (PaxosMessage) t.popMemory().getPayload();
	    assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	    assertEquals(i, m.getDecreeNumber());
	    assertEquals(i, m.getProposal().getMessages()[0][0]);
	    proposals.add(m.getProposal());
	    learner.processMessage(m);
	}

	learner.order(new byte[] { 2 });
	s.synchronousFlush();
	assertNull(t.popMemory());

	Proposal p = proposals.get(1);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 1, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 1, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 1, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	assertNull(t.popMemory());

	Proposal otherP = new Proposal(1, APP_MESSAGES_1);
	m = new PaxosMessage(1, PaxosMessage.BEGIN_BALLOT, 0, ballot, null, otherP);
	learner.processMessage(m);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(otherP.getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(2, m.getDecreeNumber());
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(3, m.getDecreeNumber());
	assertEquals(2, m.getProposal().getMessages()[0][0]);
    }

    /*
     * Tries to order 4 messages before any of them is delivered for a classic
     * ballot. The 3rd and 4th messages should be combined in the same proposal.
     */
    public void testSegmentClassic() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);

	learner.order(new byte[] { 0 });
	learner.order(new byte[] { 1 });
	s.synchronousFlush();
	ArrayList<Proposal> proposals = new ArrayList<Proposal>();
	for (int i = 0; i < 2; i++) {
	    PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	    assertEquals(PaxosMessage.PASS, m.getType());
	    assertEquals(i, m.getDecreeNumber());
	    assertEquals(i, m.getProposal().getMessages()[0][0]);
	    proposals.add(m.getProposal());
	}

	learner.order(new byte[] { 2 });
	learner.order(new byte[] { 3 });
	s.synchronousFlush();
	assertNull(t.popMemory());

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.SUCCESS, 0, null, null, proposals.get(0));
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.PASS, m.getType());
	assertEquals(2, m.getDecreeNumber());
	assertEquals(2, m.getProposal().getMessages()[0][0]);
	assertEquals(3, m.getProposal().getMessages()[1][0]);
    }

    /*
     * Tries to order 4 messages before any of them is delivered for a fast
     * ballot. The 3rd and 4th messages should be combined in the same proposal.
     */
    public void testSegmentFast() {
	Learner learner = new Learner(s, 3, 2, 3, 1000, 0);
	BallotNumber ballot = l.generateNextFastBallotNumber();

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.ANY, 0, ballot, null, null);
	learner.processMessage(m);

	learner.order(new byte[] { 0 });
	learner.order(new byte[] { 1 });
	s.synchronousFlush();
	ArrayList<Proposal> proposals = new ArrayList<Proposal>();
	for (int i = 0; i < 2; i++) {
	    m = (PaxosMessage) t.popMemory().getPayload();
	    assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	    assertEquals(i, m.getDecreeNumber());
	    assertEquals(i, m.getProposal().getMessages()[0][0]);
	    proposals.add(m.getProposal());
	    learner.processMessage(m);
	}

	learner.order(new byte[] { 2 });
	learner.order(new byte[] { 3 });
	s.synchronousFlush();
	assertNull(t.popMemory());

	Proposal p = proposals.get(0);
	m = new PaxosMessage(1, PaxosMessage.VOTED, 0, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(2, PaxosMessage.VOTED, 0, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	m = new PaxosMessage(3, PaxosMessage.VOTED, 0, ballot, null, new Proposal(p.getId(), null));
	learner.processMessage(m);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(2, m.getDecreeNumber());
	assertEquals(2, m.getProposal().getMessages()[0][0]);
	assertEquals(3, m.getProposal().getMessages()[1][0]);
    }

}
