/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import br.unicamp.ic.treplica.transport.ITransportId;

/**
 * This class implements a test transport id.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TestTransportId implements ITransportId {
    private static final long serialVersionUID = -4244260791468811295L;

    private int id;

    public TestTransportId(int id) {
	this.id = id;
    }

    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object o) {
	if (o instanceof TestTransportId) {
	    return compareTo((ITransportId) o) == 0;
	}
	return false;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
	return id;
    }

    /**
     * @see Comparable#compareTo(Object)
     */
    public int compareTo(ITransportId o) {
	if (id > ((TestTransportId) o).id) {
	    return 1;
	} else if (id < ((TestTransportId) o).id) {
	    return -1;
	}
	return 0;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
	return Integer.toString(id);
    }

}
