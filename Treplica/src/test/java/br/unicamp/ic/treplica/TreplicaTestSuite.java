/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

import br.unicamp.ic.treplica.common.ChangeLogHistoryTest;
import br.unicamp.ic.treplica.common.ChangesStoreTest;
import br.unicamp.ic.treplica.common.CheckpointStoreTest;
import br.unicamp.ic.treplica.common.DiskChangeLogTest;
import br.unicamp.ic.treplica.common.MemoryChangeLogTest;
import br.unicamp.ic.treplica.common.UDPTransportTest;
import br.unicamp.ic.treplica.paxos.PaxosAcceptorTest;
import br.unicamp.ic.treplica.paxos.PaxosCoordinatorTest;
import br.unicamp.ic.treplica.paxos.PaxosDataStructuresTest;
import br.unicamp.ic.treplica.paxos.PaxosLearnerTest;
import br.unicamp.ic.treplica.paxos.PaxosPersistentQueueTest;
import br.unicamp.ic.treplica.paxos.PaxosSecretaryTest;
import br.unicamp.ic.treplica.paxos.ledger.PaxosLedgerTest;
import br.unicamp.ic.treplica.paxos.ledger.PaxosLoggingLedgerTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * This class is the root of Treplica test suite.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TreplicaTestSuite extends TestCase {

    /**
     * Makes the test suite.
     * <p>
     *
     * @return the suite.
     **/
    public static Test suite() {
	TestSuite suite = new TestSuite("Treplica Test Suite");
	suite.addTest(new TestSuite(UDPTransportTest.class));
	suite.addTest(new TestSuite(DiskChangeLogTest.class));
	suite.addTest(new TestSuite(ChangesStoreTest.class));
	suite.addTest(new TestSuite(CheckpointStoreTest.class));
	suite.addTest(new TestSuite(ChangeLogHistoryTest.class));
	suite.addTest(new TestSuite(MemoryChangeLogTest.class));
	suite.addTest(new TestSuite(PaxosDataStructuresTest.class));
	suite.addTest(new TestSuite(PaxosLedgerTest.class));
	suite.addTest(new TestSuite(PaxosLoggingLedgerTest.class));
	suite.addTest(new TestSuite(PaxosSecretaryTest.class));
	suite.addTest(new TestSuite(PaxosCoordinatorTest.class));
	suite.addTest(new TestSuite(PaxosAcceptorTest.class));
	suite.addTest(new TestSuite(PaxosLearnerTest.class));
	suite.addTest(new TestSuite(PaxosPersistentQueueTest.class));
	suite.addTest(new TestSuite(StateMachineTest.class));
	return suite;
    }
}
