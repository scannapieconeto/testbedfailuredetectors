/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import br.unicamp.ic.treplica.TestStateManager;
import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.common.TestChangeLog;
import br.unicamp.ic.treplica.common.TestTransport;
import br.unicamp.ic.treplica.common.TestTransportId;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransportId;
import junit.framework.TestCase;

public class PaxosAcceptorTest extends TestCase {

    private TestTransport t;
    private Secretary s;
    private Ledger l;

    @Override
    protected void setUp() throws TreplicaException {
	t = new TestTransport();
	s = new Secretary(new TestChangeLog(), t, null);
	s.bind(10, new TestStateManager());
	l = s.getLedger();
    }

    /*
     * Sends several ballots to test the receive of next ballot messages. First
     * b2 is sent, with a last vote response. Then b1 and b2 again, both are
     * ignored. Finally b3 also triggers a last vote response, but this time
     * with a non null prev vote.
     */
    public void testNextBallot() {
	BallotNumber b1 = l.generateNextClassicBallotNumber();
	BallotNumber b2 = l.generateNextClassicBallotNumber();
	BallotNumber b3 = l.generateNextClassicBallotNumber();
	Acceptor acceptor = new Acceptor(s);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 0, b2, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.LAST_VOTE, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b2, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 0, b1, null, null);
	acceptor.processMessage(m, t.getId());
	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 0, b2, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	assertNull(t.popMemory());

	Proposal p = new Proposal(1, new byte[][] { { 69 } });
	Vote v = new Vote(b2, p);
	l.setPrevVote(0, v);

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 0, b3, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.LAST_VOTE, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b3, m.getBallotNumber());
	assertEquals(v, m.getVote());
	assertNull(m.getProposal());

	assertEquals(b3, l.getNextBallot(0));
    }

    /*
     * Sends a next ballot message for a decided decree.
     */
    public void testNextBallotSucess() {
	Proposal p = new Proposal(1, new byte[][] { { 69 } });
	l.write(0, p);
	BallotNumber b = l.generateNextClassicBallotNumber();
	Acceptor acceptor = new Acceptor(s);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 0, b, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.SUCCESS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());
	assertEquals(69, m.getProposal().getMessages()[0][0]);

	assertNull(l.getNextBallot(0));
    }

    /*
     * Sends two ballots from different processes to test the larger ballot
     * message. First b2 is sent, with a last vote response, then b1 triggers a
     * larger ballot response.
     */
    public void testNextBallotLargerBallot() {
	Acceptor acceptor = new Acceptor(s);
	ITransportId p2 = new TestTransportId(2);
	BallotNumber b1 = new BallotNumber(1, 5);
	BallotNumber b2 = new BallotNumber(2, 23);

	PaxosMessage m = new PaxosMessage(2, PaxosMessage.NEXT_BALLOT, 0, b2, null, null);
	acceptor.processMessage(m, p2);
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.LAST_VOTE, m.getType());

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 0, b1, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.LARGER_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b2, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	assertEquals(b2, l.getNextBallot(0));
    }

    /*
     * For a given ledger, check if the last vote inactive message is correct.
     * After that, send two next ballots to see if the last vote promise is
     * upheld. Finally, send two next ballot inactives to further check the
     * promises not to vote.
     */
    public void testNextBallotInactive() {
	BallotNumber b1 = l.generateNextFastBallotNumber();
	BallotNumber b2 = l.generateNextClassicBallotNumber();
	BallotNumber b3 = l.generateNextFastBallotNumber();
	BallotNumber b4 = l.generateNextClassicBallotNumber();
	BallotNumber b5 = l.generateNextFastBallotNumber();
	BallotNumber b6 = l.generateNextClassicBallotNumber();
	Proposal p1 = new Proposal(1, new byte[][] { { 69 } });
	Proposal p2 = new Proposal(2, new byte[][] { { 42 } });
	Vote v = new Vote(b1, p2);
	l.write(0, p1);
	l.setNextBallot(2, b1);
	l.setPrevVote(2, v);
	l.setNextBallot(3, b2);
	l.setNextBallot(5, b6);
	Acceptor acceptor = new Acceptor(s);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT_INACTIVE, Ledger.NULL_COUNTER, b3,
		null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.LAST_VOTE_INACTIVE, m.getType());
	assertEquals(6, m.getDecreeNumber());
	assertEquals(b3, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT_INACTIVE, Ledger.NULL_COUNTER, b4, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.LAST_VOTE_INACTIVE, m.getType());
	assertEquals(6, m.getDecreeNumber());
	assertEquals(b4, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT, 1, b3, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	assertNull(t.popMemory());

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT_INACTIVE, Ledger.NULL_COUNTER, b1, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	assertNull(t.popMemory());
	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT_INACTIVE, Ledger.NULL_COUNTER, b5, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.LAST_VOTE_INACTIVE, m.getType());
	assertEquals(6, m.getDecreeNumber());
	assertEquals(b5, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * Sends two next ballot inactive from different processes to test the
     * larger ballot message. First b2 is sent, then b1 triggers a larger ballot
     * response.
     */
    public void testNextBallotInactiveLargerBallot() {
	ITransportId p2 = new TestTransportId(2);
	BallotNumber b1 = new BallotNumber(1, 5, true);
	BallotNumber b2 = new BallotNumber(2, 23, true);
	Acceptor acceptor = new Acceptor(s);

	PaxosMessage m = new PaxosMessage(2, PaxosMessage.NEXT_BALLOT_INACTIVE, Ledger.NULL_COUNTER, b2, null, null);
	acceptor.processMessage(m, p2);
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.LAST_VOTE_INACTIVE, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b2, m.getBallotNumber());

	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.NEXT_BALLOT_INACTIVE, Ledger.NULL_COUNTER, b1, null, null);
	acceptor.processMessage(m, t.getId());
	s.synchronousFlush();
	reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.LARGER_BALLOT, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertEquals(b2, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	assertEquals(b2, l.getInactiveNextBallot());
    }

    /*
     * Using a properly initiated ledger, this test sends begin ballot messages
     * and expects the correct answers. For decree 0 two conflicting begin
     * ballot messages are sent and just the first should be answered. Then
     * decree 0 receives a begin ballot message with a ballot number larger than
     * the next ballot promise and a voted message is sent.
     */
    public void testBeginBallot() {
	BallotNumber b1 = l.generateNextFastBallotNumber();
	BallotNumber b2 = l.generateNextClassicBallotNumber();
	l.setInactiveNextBallot(b1);
	Acceptor acceptor = new Acceptor(s);
	Proposal p1 = new Proposal(1, new byte[][] { { 69 } });
	Proposal p2 = new Proposal(2, new byte[][] { { 42 } });

	PaxosMessage m = new PaxosMessage(2, PaxosMessage.BEGIN_BALLOT, 0, b1, null, p1);
	acceptor.processMessage(m, new TestTransportId(2));
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.VOTED, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b1, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p1, m.getProposal());

	m = new PaxosMessage(3, PaxosMessage.BEGIN_BALLOT, 0, b1, null, p2);
	acceptor.processMessage(m, new TestTransportId(3));
	s.synchronousFlush();
	assertNull(t.popMemory());
	assertEquals(b1, l.getNextBallot(0));
	assertEquals(b1, l.getPrevVote(0).getBallotNumber());
	assertEquals(p1, l.getPrevVote(0).getProposal());

	m = new PaxosMessage(4, PaxosMessage.BEGIN_BALLOT, 0, b2, null, p1);
	acceptor.processMessage(m, new TestTransportId(4));
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.VOTED, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b2, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p1, m.getProposal());
	assertEquals(b2, l.getNextBallot(0));
	assertEquals(b2, l.getPrevVote(0).getBallotNumber());
	assertEquals(p1, l.getPrevVote(0).getProposal());
    }

    /*
     * Using a properly initiated ledger, this test sends begin ballot messages
     * and expects the correct answers. Decree 0 receives a begin ballot message
     * with a ballot number larger than the next ballot promise and a voted
     * message is sent. Then two begin ballot messages are sent, both with a
     * ballot number smaller than the one a vote was cast. Only the classic
     * ballot is answered with a larger ballot message.
     */
    public void testBeginBallotLargerBallot() {
	BallotNumber b1 = l.generateNextFastBallotNumber();
	BallotNumber b2 = l.generateNextClassicBallotNumber();
	BallotNumber b3 = l.generateNextClassicBallotNumber();
	l.setInactiveNextBallot(b1);
	Acceptor acceptor = new Acceptor(s);
	Proposal p1 = new Proposal(1, new byte[][] { { 69 } });
	Proposal p2 = new Proposal(2, new byte[][] { { 42 } });

	PaxosMessage m = new PaxosMessage(2, PaxosMessage.BEGIN_BALLOT, 0, b3, null, p1);
	acceptor.processMessage(m, new TestTransportId(2));
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.VOTED, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b3, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p1, m.getProposal());

	m = new PaxosMessage(3, PaxosMessage.BEGIN_BALLOT, 0, b1, null, p2);
	acceptor.processMessage(m, new TestTransportId(3));
	s.synchronousFlush();
	assertNull(t.popMemory());
	assertEquals(b3, l.getNextBallot(0));
	assertEquals(b3, l.getPrevVote(0).getBallotNumber());
	assertEquals(p1, l.getPrevVote(0).getProposal());

	m = new PaxosMessage(4, PaxosMessage.BEGIN_BALLOT, 0, b2, null, p2);
	acceptor.processMessage(m, new TestTransportId(4));
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.LARGER_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b3, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());
	assertEquals(b3, l.getNextBallot(0));
	assertEquals(b3, l.getPrevVote(0).getBallotNumber());
	assertEquals(p1, l.getPrevVote(0).getProposal());
    }

    /*
     * This test sends a begin ballot message when no next ballot was received.
     */
    public void testBeginBallotNullNextBallot() {
	BallotNumber b = l.generateNextFastBallotNumber();
	Acceptor acceptor = new Acceptor(s);
	Proposal p = new Proposal(1, new byte[][] { { 69 } });

	PaxosMessage m = new PaxosMessage(2, PaxosMessage.BEGIN_BALLOT, 0, b, null, p);
	acceptor.processMessage(m, new TestTransportId(2));
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.VOTED, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());
	assertEquals(b, l.getNextBallot(0));
	assertEquals(b, l.getPrevVote(0).getBallotNumber());
	assertEquals(p, l.getPrevVote(0).getProposal());
    }

    /*
     * Using a properly initiated ledger, this test sends a begin ballot message
     * with the null proposal.
     */
    public void testBeginBallotNullProposal() {
	BallotNumber b = l.generateNextFastBallotNumber();
	l.setInactiveNextBallot(b);
	Acceptor acceptor = new Acceptor(s);

	PaxosMessage m = new PaxosMessage(2, PaxosMessage.BEGIN_BALLOT, 0, b, null, null);
	acceptor.processMessage(m, new TestTransportId(2));
	s.synchronousFlush();
	IMessage reply = t.popMemory();
	assertEquals(t.getId(), reply.getSender());
	m = (PaxosMessage) reply.getPayload();
	assertEquals(PaxosMessage.VOTED, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(b, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

}
