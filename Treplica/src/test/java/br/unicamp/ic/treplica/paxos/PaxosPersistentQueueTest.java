/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import br.unicamp.ic.treplica.PersistentQueue;
import br.unicamp.ic.treplica.StateManager;
import br.unicamp.ic.treplica.TestStateManager;
import br.unicamp.ic.treplica.common.Marshall;
import br.unicamp.ic.treplica.common.TestChangeLog;
import br.unicamp.ic.treplica.common.TestChangeLogClient;
import br.unicamp.ic.treplica.common.TestTransport;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import junit.framework.TestCase;

public class PaxosPersistentQueueTest extends TestCase {

    @Override
    protected void setUp() {
    }

    @Override
    protected void tearDown() {
    }

    public void testClassic() throws Exception {
	TestTransport t = new TestTransport();
	PersistentQueue paxos = new PaxosPersistentQueue(10, 1, false, t, new TestChangeLog());
	paxos.bind(new TestStateManager());
	Thread.sleep(20);
	paxos.enqueue("hello1");
	paxos.enqueue("hello2");
	assertEquals("hello1", paxos.dequeue());
	assertEquals("hello2", paxos.dequeue());
	paxos.enqueue("hello3");
	paxos.enqueue("hello4");
	assertEquals("hello3", paxos.dequeue());
	assertEquals("hello4", paxos.dequeue());
    }

    public void testFast() throws Exception {
	TestTransport t = new TestTransport();
	PersistentQueue paxos = new PaxosPersistentQueue(10, 1, true, t, new TestChangeLog());
	paxos.bind(new TestStateManager());
	Thread.sleep(20);
	paxos.enqueue("hello1");
	paxos.enqueue("hello2");
	assertEquals("hello1", paxos.dequeue());
	assertEquals("hello2", paxos.dequeue());
	paxos.enqueue("hello3");
	paxos.enqueue("hello4");
	assertEquals("hello3", paxos.dequeue());
	assertEquals("hello4", paxos.dequeue());
    }

    public void testNull() throws Exception {
	TestTransport t = new TestTransport();
	PersistentQueue paxos = new PaxosPersistentQueue(10, 1, true, t, new TestChangeLog());
	paxos.bind(new TestStateManager());
	Thread.sleep(20);
	paxos.enqueue(null);
	assertNull(paxos.dequeue());
    }

    public void testCheckpoint() throws Exception {
	TestTransport t = new TestTransport();
	TestChangeLog l = new TestChangeLog();
	PaxosPersistentQueue paxos = new PaxosPersistentQueue(20, 1, true, t, l);
	paxos.bind(new TestStateManager());
	Thread.sleep(40);
	paxos.enqueue("hello1");
	Thread.sleep(40);
	paxos.enqueue("hello2");
	assertEquals("hello1", paxos.dequeue());
	Thread.sleep(40);
	paxos.checkpoint();
	paxos.enqueue("hello3");
	Thread.sleep(40);

	Secretary secretary = new Secretary(l.copy(), null, null);
	assertEquals(1, secretary.bind(10, new TestStateManager()));
	Ledger ledger = secretary.getLedger();
	assertEquals(Marshall.unmarshall(ledger.read(0).getMessages()[0]), "hello1");
	assertEquals(Marshall.unmarshall(ledger.read(1).getMessages()[0]), "hello2");
	assertEquals(Marshall.unmarshall(ledger.read(2).getMessages()[0]), "hello3");
	assertNull(ledger.read(3));
	TestChangeLogClient client = new TestChangeLogClient();
	l.copy().open(client);
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(1, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	ledger = ((PaxosCheckpoint) client.lastCheckpoint).getLedger();
	assertEquals(Marshall.unmarshall(ledger.read(0).getMessages()[0]), "hello1");
	assertEquals(Marshall.unmarshall(ledger.read(1).getMessages()[0]), "hello2");
	assertNull(ledger.read(2));
	assertEquals(2, client.changes.size());
    }

    public void testRecoveryNoCheckpoint() throws Exception {
	TestTransport t = new TestTransport();
	TestChangeLog l = new TestChangeLog();
	Secretary secretary = new Secretary(l, null, null);
	secretary.bind(10, new TestStateManager());
	Ledger ledger = secretary.getLedger();
	ledger.write(0, new Proposal(0, new byte[][] { Marshall.marshall("hello1") }));
	ledger.write(1, new Proposal(1, new byte[][] { Marshall.marshall("hello2") }));
	secretary.synchronousFlush();
	PersistentQueue paxos = new PaxosPersistentQueue(10, 1, true, t, l);
	paxos.bind(new TestStateManager());
	Thread.sleep(20);
	paxos.enqueue("hello3");
	assertEquals("hello1", paxos.dequeue());
	assertEquals("hello2", paxos.dequeue());
	paxos.enqueue("hello4");
	assertEquals("hello3", paxos.dequeue());
	assertEquals("hello4", paxos.dequeue());
    }

    public void testRecoveryCheckpoint1() throws Exception {
	TestTransport t = new TestTransport();
	TestChangeLog l = new TestChangeLog();
	Secretary secretary = new Secretary(l, null, null);
	secretary.bind(10, new TestStateManager());
	Ledger ledger = secretary.getLedger();
	ledger.write(0, new Proposal(0, new byte[][] { Marshall.marshall("hello1") }));
	ledger.write(1, new Proposal(1, new byte[][] { Marshall.marshall("hello2") }));
	secretary.queueCheckpoint("checkpoint", 1);
	secretary.synchronousFlush();
	PersistentQueue paxos = new PaxosPersistentQueue(10, 1, true, t, l);
	StateManager state = new TestStateManager();
	paxos.bind(state);
	Thread.sleep(20);
	assertEquals("checkpoint", state.getState());
	assertEquals("hello2", paxos.dequeue());
    }

    public void testRecoveryCheckpoint2() throws Exception {
	TestTransport t = new TestTransport();
	TestChangeLog l = new TestChangeLog();
	Secretary secretary = new Secretary(l, null, null);
	secretary.bind(10, new TestStateManager());
	Ledger ledger = secretary.getLedger();
	ledger.write(0, new Proposal(0, new byte[][] { Marshall.marshall("hello1") }));
	ledger.write(1, new Proposal(1, new byte[][] { Marshall.marshall("hello2") }));
	secretary.queueCheckpoint("checkpoint", 1);
	secretary.synchronousFlush();
	ledger.write(2, new Proposal(1, new byte[][] { Marshall.marshall("hello3") }));
	secretary.synchronousFlush();
	PersistentQueue paxos = new PaxosPersistentQueue(10, 1, true, t, l);
	StateManager state = new TestStateManager();
	paxos.bind(state);
	Thread.sleep(20);
	assertEquals("checkpoint", state.getState());
	assertEquals("hello2", paxos.dequeue());
	assertEquals("hello3", paxos.dequeue());
    }

}
