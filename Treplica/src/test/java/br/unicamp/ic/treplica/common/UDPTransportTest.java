/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.transport.DefaultMessageReceiver;
import br.unicamp.ic.treplica.transport.DefaultMessageSender;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.IMessageReceiver;
import br.unicamp.ic.treplica.transport.IMessageSender;
import br.unicamp.ic.treplica.transport.ITransportId;
import br.unicamp.ic.treplica.transport.UDPTransport;
import br.unicamp.ic.treplica.transport.UDPTransportId;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.TreplicaConfig;
import junit.framework.TestCase;

public class UDPTransportTest extends TestCase {

    private static final TreplicaConfig TREPLICA_CONFIG = JarConfig.INST.getTreplica();
    private static final String DEFAULT_MULT_IP = TREPLICA_CONFIG.getIp();
    private static final int DEFAULT_MULT_PORT = TREPLICA_CONFIG.getPort();
    private final IMessageReceiver defaultMessageReceiver = new DefaultMessageReceiver();
    private final IMessageSender defaultMessageSender = new DefaultMessageSender();

    @Override
    protected void setUp() {
    }

    public void testTransportId() {
        byte[] address1 = { 127, 0, 0, 1 };
        byte[] address2 = { 127, 0, 1, 0 };

        UDPTransportId id1 = new UDPTransportId(address1, 23);
        UDPTransportId id2 = new UDPTransportId(address1, 5);
        UDPTransportId id3 = new UDPTransportId(address2, 23);
        UDPTransportId id4 = new UDPTransportId(address1, 23);

        assertEquals(address1, id1.getAddress());
        assertEquals(23, id1.getPort());
        assertTrue(id1.equals(id4));
        assertTrue(id1.hashCode() == id4.hashCode());
        assertFalse(id1.equals(id2));
        assertFalse(id1.hashCode() == id2.hashCode());
        assertFalse(id1.equals(id3));
        assertFalse(id1.hashCode() == id3.hashCode());
        assertEquals(0, id1.compareTo(id4));
        assertTrue(id1.compareTo(id2) > 0);
        assertTrue(id1.compareTo(id3) < 0);
    }

    public void testUnicast() throws TreplicaException {
        UDPTransport t1 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        UDPTransport t2 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);

        ITransportId id1 = t1.getId();
        ITransportId id2 = t2.getId();
        assertFalse(id1.equals(id2));

        t1.sendMessage("1", id2);
        IMessage m = t2.receiveMessage();
        assertNotNull(m);
        assertEquals(id1, m.getSender());
        assertEquals(id2, m.getReceiver());
    }

    public void testMulticast() throws TreplicaException {
        UDPTransport t1 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        UDPTransport t2 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        UDPTransport t3 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        ITransportId id1 = t1.getId();
        ITransportId id2 = t2.getId();

        t1.sendMessage("2");
        IMessage m = t2.receiveMessage();
        assertNotNull(m);
        assertEquals(id1, m.getSender());
        assertFalse(id2.equals(m.getReceiver()));
        m = t1.receiveMessage();
        assertNotNull(m);
        assertEquals("2", m.getPayload());
        m = t3.receiveMessage();
        assertNotNull(m);
    }

    /*
     * public void testFragmentation() throws TreplicaException { UDPTransport
     * t1 = new UDPTransport(); UDPTransport t2 = new UDPTransport();
     * t1.sendMessage(new byte[100000]); Message m = t2.receiveMessage();
     * assertNotNull(m); assertEquals(100000, ((byte[]) m.getPayload()).length);
     * }
     */

    public void testAdressing() throws TreplicaException {
        UDPTransport t1 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        UDPTransport t2 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        t1.sendMessage("1");
        IMessage m = t2.receiveMessage();
        t2.sendMessage("3", m.getSender());
        m = t1.receiveMessage();
        assertNotNull(m);
        IMessage m2 = t1.receiveMessage();
        assertNotNull(m2);
        assertTrue(t2.getId().equals(m.getSender()) || t2.getId().equals(m2.getSender()));
    }

    public void testTimeout() throws TreplicaIOException {
        UDPTransport t1 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        new Thread(new TimedSender(t1, 100)).start();
        IMessage m = t1.receiveMessage(20);
        assertNull(m);
        m = t1.receiveMessage(200);
        assertNotNull(m);
    }

    public void testConcurrency() throws TreplicaIOException {
        UDPTransport t1 = new UDPTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, defaultMessageReceiver, defaultMessageSender);
        new Thread(new TimedSender(t1, 100)).start();
        IMessage m = t1.receiveMessage();
        assertNotNull(m);
    }

    public void testConstructor() throws TreplicaException {
        UDPTransport t1 = new UDPTransport("225.0.0.1", 6667, defaultMessageReceiver, defaultMessageSender);
        t1.sendMessage("5");
        IMessage m = t1.receiveMessage();
        assertNotNull(m);
        assertEquals("5", m.getPayload());
    }

    private class TimedSender implements Runnable {

        private UDPTransport transport;
        private int timeout;

        protected TimedSender(UDPTransport transport, int timeout) {
            this.transport = transport;
            this.timeout = timeout;
        }

        public void run() {
            try {
                Thread.sleep(timeout);
                transport.sendMessage("4");
            } catch (Exception e) {
            }
        }
    }

}
