/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

import java.io.Serializable;
import java.util.LinkedList;

import junit.framework.TestCase;

public class StateMachineTest extends TestCase {

    @Override
    protected void setUp() {
    }

    @Override
    protected void tearDown() {
    }

    public void testSimpleOperation() throws Exception {
	TestPersistentQueue queue = new TestPersistentQueue();
	StateMachine sm = new StateMachine(new StringBuilder("a"), queue);
	assertEquals("ab", sm.execute(new AppendAction("b")));
	assertEquals("abc", sm.execute(new AppendAction("c")));
	assertEquals("abc", sm.getState().toString());
    }

    public void testException() throws Exception {
	TestPersistentQueue queue = new TestPersistentQueue();
	StateMachine sm = new StateMachine(new StringBuilder("a"), queue);
	try {
	    sm.execute(new AppendAction(""));
	    fail("Exception not thrown");
	} catch (AppendException e) {
	}
	assertEquals("a", sm.getState().toString());
    }

    public void testCheckpoint() throws Exception {
	TestPersistentQueue queue = new TestPersistentQueue();
	StateMachine sm = new StateMachine(new StringBuilder("a"), queue);
	sm.execute(new AppendAction("b"));
	sm.checkpoint();
	sm.execute(new AppendAction("c"));
	assertEquals("ab", queue.initialState.toString());
	assertEquals(1, queue.initialQueue.size());
    }

    public void testRecovery() throws Exception {
	StringBuilder oldState = new StringBuilder("ab");
	LinkedList<Serializable> messages = new LinkedList<Serializable>();
	messages.addLast(new SMMessage(200, new AppendAction("c")));
	messages.addLast(new SMMessage(201, new AppendAction("d")));
	TestPersistentQueue queue = new TestPersistentQueue(oldState, messages);
	StateMachine sm = new StateMachine(new StringBuilder("a"), queue);
	sm.execute(new AppendAction("e"));
	assertEquals("abcde", sm.getState().toString());
    }

    public static class AppendAction implements Action, Serializable {
	private static final long serialVersionUID = 464743809653453149L;

	private String value;

	public AppendAction(String value) {
	    this.value = value;
	}

	public Object executeOn(Object stateMachine) throws AppendException {
	    if ("".equals(value)) {
		throw new AppendException();
	    }
	    StringBuilder string = (StringBuilder) stateMachine;
	    string.append(value);
	    return string.toString();
	}
    }

    public static class AppendException extends Exception {
	private static final long serialVersionUID = 494473551250681118L;
    }
}
