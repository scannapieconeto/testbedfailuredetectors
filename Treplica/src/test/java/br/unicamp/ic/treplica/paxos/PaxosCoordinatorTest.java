/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import br.unicamp.ic.treplica.TestStateManager;
import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.common.TestChangeLog;
import br.unicamp.ic.treplica.common.TestTransport;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import junit.framework.TestCase;

public class PaxosCoordinatorTest extends TestCase {

    private static byte[] APP_MESSAGE_1 = { 23 };
    private static byte[] APP_MESSAGE_2 = { 42 };
    private static byte[] APP_MESSAGE_3 = { 69 };
    private static byte[][] APP_MESSAGES_1 = { APP_MESSAGE_1 };
    private static byte[][] APP_MESSAGES_2 = { APP_MESSAGE_2 };
    private static byte[][] APP_MESSAGES_3 = { APP_MESSAGE_3 };

    private TestTransport t;
    private Secretary s;
    private Ledger l;

    @Override
    protected void setUp() throws TreplicaException {
	t = new TestTransport();
	s = new Secretary(new TestChangeLog(), t, null);
	s.bind(10, new TestStateManager());
	l = s.getLedger();
    }

    public void testLastVoteCountingClassic() throws InterruptedException {
	CoordinatorBallot b = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE, null);
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 5);
	BallotNumber b3 = new BallotNumber(2, 23);
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	Proposal p3 = new Proposal(3, APP_MESSAGES_3);
	Vote v1 = new Vote(b1, p1);
	Vote v2 = new Vote(b2, p2);
	Vote v3 = new Vote(b3, p3);

	long timestamp = b.getTimestamp();
	Thread.sleep(10);
	assertEquals(0, b.receivedLastVote());
	assertNull(b.getWinningProposal());
	b.registerLastVote(2, v1);
	assertEquals(1, b.receivedLastVote());
	assertEquals(p1, b.getWinningProposal());
	assertEquals(APP_MESSAGE_1[0], b.getWinningProposal().getMessages()[0][0]);
	b.registerLastVote(2, v1);
	assertEquals(1, b.receivedLastVote());
	assertEquals(p1, b.getWinningProposal());
	assertEquals(APP_MESSAGE_1[0], b.getWinningProposal().getMessages()[0][0]);
	b.registerLastVote(3, v2);
	assertEquals(2, b.receivedLastVote());
	assertEquals(p1, b.getWinningProposal());
	assertEquals(APP_MESSAGE_1[0], b.getWinningProposal().getMessages()[0][0]);
	b.registerLastVote(4, v3);
	assertEquals(3, b.receivedLastVote());
	assertEquals(p3, b.getWinningProposal());
	assertEquals(APP_MESSAGE_3[0], b.getWinningProposal().getMessages()[0][0]);
	assertTrue(timestamp < b.getTimestamp());
    }

    public void testLastVoteCountingClassicWithNullProposal() {
	CoordinatorBallot b = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE, null);
	BallotNumber b1 = new BallotNumber(1, 5);
	BallotNumber b2 = new BallotNumber(1, 23);
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Vote v1 = new Vote(b1, p1);
	Vote v2 = new Vote(b2, null);

	assertEquals(0, b.receivedLastVote());
	assertNull(b.getWinningProposal());
	b.registerLastVote(2, v1);
	assertEquals(1, b.receivedLastVote());
	assertEquals(p1, b.getWinningProposal());
	b.registerLastVote(3, v2);
	assertEquals(2, b.receivedLastVote());
	assertNull(b.getWinningProposal());
    }

    public void testLastVoteCountingFast() {
	CoordinatorBallot b = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE, null);
	BallotNumber b1 = new BallotNumber(1, 5);
	BallotNumber b2 = new BallotNumber(2, 23, true);
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	Proposal p3 = new Proposal(3, APP_MESSAGES_3);
	Vote v1 = new Vote(b1, p1);
	Vote v2 = new Vote(b2, p2);
	Vote v3 = new Vote(b2, p3);

	assertEquals(0, b.receivedLastVote());
	assertNull(b.getWinningProposal());
	b.registerLastVote(2, v1);
	assertEquals(1, b.receivedLastVote());
	assertEquals(p1, b.getWinningProposal());
	b.registerLastVote(3, v1);
	assertEquals(2, b.receivedLastVote());
	assertEquals(p1, b.getWinningProposal());
	b.registerLastVote(4, v2);
	assertEquals(3, b.receivedLastVote());
	assertEquals(p2, b.getWinningProposal());
	b.registerLastVote(5, v3);
	assertEquals(4, b.receivedLastVote());
	Proposal p = b.getWinningProposal();
	assertTrue(p.equals(p2) || p.equals(p3));
	b.registerLastVote(6, v3);
	assertEquals(5, b.receivedLastVote());
	assertEquals(p3, b.getWinningProposal());
    }

    public void testLastVoteCountingFastWithNullProposal() {
	CoordinatorBallot b = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE, null);
	BallotNumber bn = new BallotNumber(2, 23, true);
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	Vote v1 = new Vote(bn, p);
	Vote v2 = new Vote(bn, null);

	assertEquals(0, b.receivedLastVote());
	assertNull(b.getWinningProposal());
	b.registerLastVote(2, v1);
	assertEquals(1, b.receivedLastVote());
	assertEquals(p, b.getWinningProposal());
	b.registerLastVote(3, v2);
	assertEquals(2, b.receivedLastVote());
	Proposal prop = b.getWinningProposal();
	assertTrue(prop == null || prop.equals(p));
	b.registerLastVote(4, v2);
	assertEquals(3, b.receivedLastVote());
	assertNull(b.getWinningProposal());
    }

    public void testLastVoteWithNullVote() {
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	CoordinatorBallot e = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE, p);

	e.registerLastVote(2, null);
	assertEquals(1, e.receivedLastVote());
	assertEquals(p, e.getWinningProposal());
    }

    /*
     * Test activation with simple activation: no past incomplete ballots.
     */
    public void testSimpleActivationClassic() {
	Coordinator coordinator = new Coordinator(s, 2, 3, 1000, false);

	coordinator.activate();
	s.synchronousFlush();
	BallotNumber nextBallot = l.getInactiveLastTried();
	assertFalse(nextBallot.isFast());
	assertEquals(0, l.firstUndecidedDecree());
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	m = new PaxosMessage(s.getPaxosId(), PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertEquals(p, m.getProposal());
    }

    /*
     * Test activation with simple activation: no past incomplete ballots.
     */
    public void testSimpleActivationFast() {
	Coordinator coordinator = new Coordinator(s, 2, 3, 1000, true);

	coordinator.activate();
	s.synchronousFlush();
	BallotNumber nextBallot = l.getInactiveLastTried();
	assertTrue(nextBallot.isFast());
	assertEquals(0, l.firstUndecidedDecree());
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	assertNull(t.popMemory());
	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();

	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.ANY, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * Test double activation.
     */
    public void testDoubleActivation() {
	Coordinator coordinator = new Coordinator(s, 2, 3, 1000, true);

	coordinator.activate();
	s.synchronousFlush();
	BallotNumber nextBallot = l.getInactiveLastTried();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());

	coordinator.activate();
	s.synchronousFlush();
	assertEquals(nextBallot, l.getInactiveLastTried());
	assertNull(t.popMemory());
    }

    /*
     * Test deactivation of the coordinator while activating and inactive.
     */
    public void testDeactivation1() throws InterruptedException {
	Coordinator coordinator = new Coordinator(s, 2, 3, 10, true);

	coordinator.activate();
	s.synchronousFlush();
	t.popMemory();

	coordinator.deactivate();
	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());

	coordinator.deactivate();
    }

    /*
     * Test deactivation of the coordinator while active.
     */
    public void testDeactivation2() throws InterruptedException {
	l.setLastTried(0, l.getLastTried(0));
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);

	coordinator.deactivate();
	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Test the data gathering that happens during activation. In this test one
     * of the acceptors knows the larger decree number.
     */
    public void testActivationClassic1() throws InterruptedException {
	Coordinator coordinator = new Coordinator(s, 2, 3, 10, false);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());

	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE_INACTIVE, 1, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(4, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * Test the data gathering that happens during activation. In this test one
     * of the acceptors knows the larger decree number.
     */
    public void testActivationFast1() throws InterruptedException {
	Coordinator coordinator = new Coordinator(s, 2, 3, 10, true);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();
	assertTrue(nextBallot.isFast());

	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE_INACTIVE, 1, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(4, PaxosMessage.LAST_VOTE_INACTIVE, 1, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	assertNull(t.popMemory());
	m = new PaxosMessage(5, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);

	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.ANY, m.getType());
	assertEquals(1, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * Test the data gathering that happens during activation. In this test the
     * coordinator knows the larger decree number.
     */
    public void testActivationClassic2() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextClassicBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = new Coordinator(s, 2, 3, 10, false);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());

	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(4, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * Test the data gathering that happens during activation. In this test the
     * coordinator knows the larger decree number.
     */
    public void testActivationFast2() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextClassicBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = new Coordinator(s, 2, 3, 10, true);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();
	assertTrue(nextBallot.isFast());

	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(4, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	assertNull(t.popMemory());
	m = new PaxosMessage(5, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);

	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.ANY, m.getType());
	assertEquals(1, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * Test the data gathering that happens during activation. If a larger
     * ballot message is received, the activation processes is restarted.
     */
    public void testActivationLargerBallotClassic() {
	BallotNumber b = new BallotNumber(1, 42);
	Coordinator coordinator = new Coordinator(s, 2, 3, 1000, false);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(b.compareTo(nextBallot) > 0);

	m = new PaxosMessage(2, PaxosMessage.LARGER_BALLOT, Ledger.NULL_COUNTER, b, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();

	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(b.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(2, PaxosMessage.LARGER_BALLOT, Ledger.NULL_COUNTER, b, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Test the data gathering that happens during activation. If a larger
     * ballot message is received, the activation processes is restarted.
     */
    public void testActivationLargerBallotFast() {
	BallotNumber b = new BallotNumber(1, 42);
	Coordinator coordinator = new Coordinator(s, 2, 3, 1000, true);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();
	assertTrue(m.getBallotNumber().isFast());
	assertTrue(b.compareTo(nextBallot) > 0);

	m = new PaxosMessage(2, PaxosMessage.LARGER_BALLOT, Ledger.NULL_COUNTER, b, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();

	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertTrue(m.getBallotNumber().isFast());
	assertTrue(b.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(2, PaxosMessage.LARGER_BALLOT, Ledger.NULL_COUNTER, b, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Test creation of ballots for undecided but active decrees. In this test
     * we have decrees: 0) inactive, 1) decided, 2) active, undecided, this
     * should generate two new ballots.
     */
    public void testActivationUndecided() throws InterruptedException {
	l.write(1, null);
	l.setLastTried(2, l.generateNextClassicBallotNumber());
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	long decreeNumber1 = m.getDecreeNumber();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	long decreeNumber2 = m.getDecreeNumber();
	assertTrue((decreeNumber1 == 0 && decreeNumber2 == 2) || (decreeNumber1 == 2 && decreeNumber2 == 0));
	assertNull(t.popMemory());
    }

    /*
     * This test checks if each message received during activation affects
     * activation restart.
     */
    public void testActivationTimeout() throws InterruptedException {
	Coordinator coordinator = new Coordinator(s, 3, 4, 100, true);
	coordinator.activate();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	BallotNumber nextBallot = m.getBallotNumber();

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	Thread.sleep(60);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE_INACTIVE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	Thread.sleep(60);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
	Thread.sleep(60);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
    }

    /*
     * This test checks if the activation timeout before any message is
     * received.
     */
    public void testActivationTimeoutBeforeLastvote() throws InterruptedException {
	Coordinator coordinator = new Coordinator(s, 3, 4, 150, true);
	coordinator.activate();
	s.synchronousFlush();
	t.popMemory().getPayload();

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * This test checks if the coordinator regularly renews the activation for
     * fast ballots.
     */
    public void testActivationRenewFast() throws InterruptedException {
	Coordinator coordinator = activate(t, s, 2, 3, 1, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.ANY, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(l.getInactiveLastTried(), m.getBallotNumber());
    }

    /*
     * This test checks if the coordinator don't regularly renew the activation
     * for classic ballots.
     */
    public void testActivationRenewClassic() throws InterruptedException {
	Coordinator coordinator = activate(t, s, 2, 3, 1, false);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * This test shows the behavior of the PASS message received while the
     * coordinator holds a classic last tried inactive. A PASS sent for decree 0
     * (decided) creates a SUCCESS message, for decree 1 (active, undecided) is
     * ignored and for decree 2 (inactive) creates a BEGIN_BALLOT message.
     */
    public void testPassClassic1() {
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	l.write(0, p1);
	l.setLastTried(1, l.generateNextClassicBallotNumber());
	Coordinator coordinator = activate(t, s, 2, 3, 1000, false);

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.PASS, 0, null, null, p2);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.SUCCESS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p1, m.getProposal());

	m = new PaxosMessage(1, PaxosMessage.PASS, 1, null, null, p2);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	assertNull(t.popMemory());

	m = new PaxosMessage(1, PaxosMessage.PASS, 2, null, null, p2);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(2, m.getDecreeNumber());
	assertEquals(l.getInactiveLastTried(), m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p2, m.getProposal());
    }

    /*
     * This test shows the behavior of the PASS message received while the
     * coordinator holds a fast last tried inactive. A PASS sent for decree 0
     * (decided) creates a SUCCESS message, for decree 1 (active, undecided) is
     * ignored and for decree 2 (inactive) creates a NEXT_BALLOT message.
     */
    public void testPassFast1() {
	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	l.write(0, p1);
	l.setLastTried(1, l.generateNextClassicBallotNumber());
	Coordinator coordinator = activate(t, s, 2, 3, 1000, true);

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.PASS, 0, null, null, p2);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.SUCCESS, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertNull(m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p1, m.getProposal());

	m = new PaxosMessage(1, PaxosMessage.PASS, 1, null, null, p2);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	assertNull(t.popMemory());

	m = new PaxosMessage(1, PaxosMessage.PASS, 2, null, null, p2);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(2, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(l.getInactiveLastTried().compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * This test shows the behavior of the PASS message received while the
     * coordinator holds a classic last tried inactive. A PASS sent for decrees
     * 0 and 1 (active, undecided) but the coordinator hasn't a ballot started
     * for them (unlikely, but possible). The coordinator ignores decree 0 but
     * timeouts and tries to pass it with a full election and decree 1 is tried
     * with the suggested proposal and a fast election.
     */
    public void testPassClassic2() throws InterruptedException {
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	l.setLastTried(0, l.getLastTried(0));
	Coordinator coordinator = activate(t, s, 2, 3, 10, false);
	l.setLastTried(1, l.getLastTried(1));

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	assertNull(t.popMemory());

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(l.getInactiveLastTried().compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(1, PaxosMessage.PASS, 1, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(1, m.getDecreeNumber());
	assertNotNull(m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());
    }

    /*
     * This test shows the behavior of the PASS message received while the
     * coordinator holds a fast last tried inactive. A PASS sent for decree 0
     * (active, undecided) but the coordinator hasn't a ballot started for it
     * (unlikely, but possible). The coordinator always tries to pass this
     * decree with a full election.
     */
    public void testPassFast2() {
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);
	l.setLastTried(0, l.getLastTried(0));

	PaxosMessage m = new PaxosMessage(1, PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertFalse(m.getBallotNumber().isFast());
	assertTrue(l.getInactiveLastTried().compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * This test sees if last votes are correctly processed for classic ballots.
     * First, an incomplete election is created in the ledger. The activation
     * process should mark it as in progress. After the timeout, a NEXT_BALLOT
     * message is sent. LAST_VOTE messages are processed and a BEGIN_BALLOT with
     * the correct proposal should be generated.
     */
    public void testLastVoteClassic() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextClassicBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());
	assertTrue(nextBallot.compareTo(previousBallot) > 0);

	Proposal p = new Proposal(1, APP_MESSAGES_1);
	Vote v = new Vote(previousBallot, p);

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE, 0, nextBallot, v, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());
    }

    /*
     * This test sees if last votes are correctly processed for fast ballots.
     * First, an incomplete election is created in the ledger. The activation
     * process should mark it as in progress. After the timeout, a NEXT_BALLOT
     * message is sent. LAST_VOTE messages are processed and a BEGIN_BALLOT with
     * the correct proposal should be generated.
     */
    public void testLastVoteFast() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextFastBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = activate(t, s, 3, 4, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());
	assertTrue(nextBallot.compareTo(previousBallot) > 0);

	Proposal p1 = new Proposal(1, APP_MESSAGES_1);
	Proposal p2 = new Proposal(2, APP_MESSAGES_2);
	Vote v1 = new Vote(previousBallot, p1);
	Vote v2 = new Vote(previousBallot, p2);

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE, 0, nextBallot, v1, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, v1, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE, 0, nextBallot, v2, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p1, m.getProposal());
    }

    /*
     * This test sees if last votes are correctly processed for classic ballots.
     * First, an incomplete election is created in the ledger but with no
     * proposal (null proposal), no other process has voted and the null
     * proposal is voted.
     */
    public void testLastVoteNullProposalClassic() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextClassicBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());
	assertTrue(nextBallot.compareTo(previousBallot) > 0);

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * This test sees if last votes are correctly processed for fast ballots.
     * First, an incomplete election is created in the ledger but with no
     * proposal (null proposal), no other process has voted and the null
     * proposal is voted.
     */
    public void testLastVoteNullProposalFast() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextFastBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = activate(t, s, 3, 4, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());
	assertTrue(nextBallot.compareTo(previousBallot) > 0);

	m = new PaxosMessage(1, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertNull(m.getProposal());
    }

    /*
     * This test walks through a five message slow election. First, an
     * incomplete election is created in the ledger. The activation process
     * should mark it as in progress. After the timeout, a sequence of
     * NEXT_BALLOT, LAST_VOTE, BEGIN_BALLOT, VOTED and SUCCESS is initiated. One
     * of the processes will have voted in this election, this will decide the
     * winning proposal.
     */
    public void testSlowVoteSequence() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextClassicBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	BallotNumber nextBallot = m.getBallotNumber();
	assertFalse(nextBallot.isFast());
	assertTrue(!nextBallot.equals(l.getInactiveLastTried()));
	assertNull(m.getVote());
	assertNull(m.getProposal());

	Proposal p = new Proposal(1, APP_MESSAGES_1);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, new Vote(previousBallot, p), null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());

	l.write(0, p);
	Thread.sleep(20);
	coordinator.processTick();
	assertNull(t.popMemory());
    }

    /*
     * This test shows the behavior of the LARGER_BALLOT message. A
     * LARGER_BALLOT received when the coordinator is ACTIVE or ACTIVATING
     * (tested above) should re-start activation.
     */
    public void testLargerBallot() {
	BallotNumber b = new BallotNumber(1, 42);
	Proposal p = new Proposal(1, APP_MESSAGES_1);
	Coordinator coordinator = activate(t, s, 2, 3, 1000, true);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertTrue(b.compareTo(m.getBallotNumber()) > 0);

	m = new PaxosMessage(2, PaxosMessage.LARGER_BALLOT, 0, b, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();

	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT_INACTIVE, m.getType());
	assertEquals(Ledger.NULL_COUNTER, m.getDecreeNumber());
	assertTrue(b.compareTo(m.getBallotNumber()) < 0);
	assertNull(m.getVote());
	assertNull(m.getProposal());

	m = new PaxosMessage(2, PaxosMessage.LARGER_BALLOT, 0, b, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * This test walks through a three message fast election. The PASS message
     * immediately generates a BEGIN_BALLOT that is promptly replied with VOTED
     * messages, finally a SUCCESS indicates the end of the election.
     */
    public void testClassicFastVoteSequence() throws InterruptedException {
	Coordinator coordinator = activate(t, s, 2, 3, 10, false);
	BallotNumber nextBallot = l.getInactiveLastTried();
	Proposal p = new Proposal(1, APP_MESSAGES_1);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());

	l.write(0, p);
	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
    }

    /*
     * Checks if messages are ignored after a ballot changes state.
     */
    public void testIgnoreAfterMajority() throws InterruptedException {
	BallotNumber previousBallot = l.generateNextClassicBallotNumber();
	l.setLastTried(0, previousBallot);
	Coordinator coordinator = activate(t, s, 2, 3, 10, true);

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	PaxosMessage m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	BallotNumber nextBallot = m.getBallotNumber();

	Proposal p = new Proposal(1, APP_MESSAGES_1);
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, new Vote(previousBallot, p), null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(4, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertNull(t.popMemory());
    }

    /*
     * This test checks if the proposal is correctly remembered after a ballot
     * is restarted. It sends a PASS and BEGIN_BALLOT messages, times out with
     * no voters, and goes all the way to BEGIN_BALLOT again.
     */
    public void testRestart() throws InterruptedException {
	Coordinator coordinator = activate(t, s, 2, 3, 10, false);
	Proposal p = new Proposal(1, APP_MESSAGES_1);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	BallotNumber nextBallot = m.getBallotNumber();

	Thread.sleep(20);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
	nextBallot = m.getBallotNumber();

	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	m = new PaxosMessage(3, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	assertEquals(0, m.getDecreeNumber());
	assertEquals(nextBallot, m.getBallotNumber());
	assertNull(m.getVote());
	assertEquals(p, m.getProposal());
    }

    /*
     * This test checks if each message received for a ballot affects ballot
     * restart.
     */
    public void testRestartTimeout() throws InterruptedException {
	Coordinator coordinator = activate(t, s, 2, 3, 100, false);
	Proposal p = new Proposal(1, APP_MESSAGES_1);

	PaxosMessage m = new PaxosMessage(s.getPaxosId(), PaxosMessage.PASS, 0, null, null, p);
	coordinator.processMessage(m, t.getId());
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.BEGIN_BALLOT, m.getType());
	Thread.sleep(200);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	BallotNumber nextBallot = m.getBallotNumber();

	Thread.sleep(60);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
	m = new PaxosMessage(2, PaxosMessage.LAST_VOTE, 0, nextBallot, null, null);
	coordinator.processMessage(m, null);
	Thread.sleep(60);
	coordinator.processTick();
	s.synchronousFlush();
	assertNull(t.popMemory());
	Thread.sleep(60);
	coordinator.processTick();
	s.synchronousFlush();
	m = (PaxosMessage) t.popMemory().getPayload();
	assertEquals(PaxosMessage.NEXT_BALLOT, m.getType());
	assertTrue(nextBallot.compareTo(m.getBallotNumber()) < 0);
    }

    private static Coordinator activate(TestTransport transport, Secretary secretary, int classicMajority,
	    int fastMajority, int timeout, boolean fast) {
	Coordinator coordinator = new Coordinator(secretary, classicMajority, fastMajority, timeout, fast);
	coordinator.activate();
	secretary.synchronousFlush();
	PaxosMessage m = (PaxosMessage) transport.popMemory().getPayload();
	int majority = fast ? fastMajority : classicMajority;
	for (int i = 1; i <= majority; i++) {
	    m = new PaxosMessage(i, PaxosMessage.LAST_VOTE_INACTIVE, 0, m.getBallotNumber(), null, null);
	    coordinator.processMessage(m, null);
	}
	secretary.synchronousFlush();
	if (fast) {
	    transport.popMemory();
	}
	return coordinator;
    }
}
