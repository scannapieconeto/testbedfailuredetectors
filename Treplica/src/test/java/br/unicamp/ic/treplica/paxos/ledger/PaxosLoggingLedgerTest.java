/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos.ledger;

import br.unicamp.ic.treplica.paxos.BallotNumber;
import br.unicamp.ic.treplica.paxos.Proposal;
import br.unicamp.ic.treplica.paxos.Vote;
import junit.framework.TestCase;

public class PaxosLoggingLedgerTest extends TestCase {

    LoggingLedger l;

    @Override
    protected void setUp() {
	l = new LoggingLedger(1);
    }

    public void testFlushLog1() {
	BallotNumber b = new BallotNumber(1, 23);

	LedgerChange[] log = l.flushLog();
	assertEquals(0, log.length);
	l.setInactiveLastTried(b);
	l.setLastTried(0, b);
	log = l.flushLog();
	assertEquals(2, log.length);
	log = l.flushLog();
	assertEquals(0, log.length);
    }

    public void testFlushLog2() {
	BallotNumber b = new BallotNumber(1, 23);

	LedgerChange[] log = l.flushLog();
	assertEquals(0, log.length);
	l.setInactiveLastTried(b);
	log = l.flushLog();
	assertEquals(1, log.length);
	l.setLastTried(0, b);
	log = l.flushLog();
	assertEquals(1, log.length);
	log = l.flushLog();
	assertEquals(0, log.length);
    }

    public void testReplayLog() {
	BallotNumber b = new BallotNumber(1, 23);
	l.setInactiveLastTried(b);
	l.setLastTried(0, b);
	LedgerChange[] log = l.flushLog();

	LoggingLedger copy = new LoggingLedger(1);
	copy.replayLog(log);
	assertEquals(b, copy.getInactiveLastTried());
	assertEquals(b, copy.getLastTried(0));
    }

    public void testGenerateBallot() {
	BallotNumber b1 = l.generateNextClassicBallotNumber();
	BallotNumber b2 = l.generateNextClassicBallotNumber();
	BallotNumber b3 = l.generateNextFastBallotNumber();

	assertFalse(b1.isFast());
	assertFalse(b2.isFast());
	assertTrue(b3.isFast());
	assertTrue(b1.compareTo(b2) < 0);
	assertTrue(b2.compareTo(b3) < 0);

	LedgerChange[] log = l.flushLog();
	assertEquals(3, log.length);
	assertTrue(log[0] instanceof GenerateNextClassicBallotNumber);
	assertTrue(log[1] instanceof GenerateNextClassicBallotNumber);
	assertTrue(log[2] instanceof GenerateNextFastBallotNumber);
    }

    public void testGenerateBallotWithReference() {
	BallotNumber b1 = new BallotNumber(1, 7);
	BallotNumber b2 = new BallotNumber(2, 23);

	l.ensureHigherBallotNumber(b1);
	assertTrue(b1.compareTo(l.generateNextClassicBallotNumber()) < 0);
	l.ensureHigherBallotNumber(b2);
	assertTrue(b2.compareTo(l.generateNextClassicBallotNumber()) < 0);

	LedgerChange[] log = l.flushLog();
	assertEquals(4, log.length);
	assertTrue(log[0] instanceof EnsureHigherBallotNumber);
	assertTrue(log[1] instanceof GenerateNextClassicBallotNumber);
	assertTrue(log[2] instanceof EnsureHigherBallotNumber);
	assertTrue(log[3] instanceof GenerateNextClassicBallotNumber);
    }

    public void testLastTried() {
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 42);

	l.setLastTried(1, b1);
	assertEquals(b1, l.getLastTried(1));
	l.setLastTried(1, b2);
	assertEquals(b2, l.getLastTried(1));
	assertNull(l.getLastTried(3));

	LedgerChange[] log = l.flushLog();
	assertEquals(2, log.length);
	assertTrue(log[0] instanceof SetLastTried);
	assertTrue(log[1] instanceof SetLastTried);
    }

    public void testInactiveLastTried() {
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 42);

	l.setInactiveLastTried(b1);
	assertEquals(b1, l.getInactiveLastTried());
	assertEquals(b1, l.getLastTried(1));
	assertEquals(b1, l.getLastTried(2));
	l.setLastTried(1, b1);
	l.setInactiveLastTried(b2);
	assertEquals(b2, l.getInactiveLastTried());
	assertEquals(b1, l.getLastTried(1));
	assertEquals(b2, l.getLastTried(2));
	assertEquals(b2, l.getLastTried(3));

	LedgerChange[] log = l.flushLog();
	assertEquals(3, log.length);
	assertTrue(log[0] instanceof SetInactiveLastTried);
	assertTrue(log[1] instanceof SetLastTried);
	assertTrue(log[2] instanceof SetInactiveLastTried);
    }

    public void testNextBallot() {
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 42);

	l.setNextBallot(1, b1);
	assertEquals(b1, l.getNextBallot(1));
	l.setNextBallot(1, b2);
	assertEquals(b2, l.getNextBallot(1));
	assertNull(l.getNextBallot(3));

	LedgerChange[] log = l.flushLog();
	assertEquals(2, log.length);
	assertTrue(log[0] instanceof SetNextBallot);
	assertTrue(log[1] instanceof SetNextBallot);
    }

    public void testInactiveNextBallot() {
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 42);

	l.setInactiveNextBallot(b1);
	assertEquals(b1, l.getInactiveNextBallot());
	assertEquals(b1, l.getNextBallot(1));
	assertEquals(b1, l.getNextBallot(2));
	l.setNextBallot(1, b1);
	l.setInactiveNextBallot(b2);
	assertEquals(b2, l.getInactiveNextBallot());
	assertEquals(b1, l.getNextBallot(1));
	assertEquals(b2, l.getNextBallot(2));
	assertEquals(b2, l.getNextBallot(3));

	LedgerChange[] log = l.flushLog();
	assertEquals(3, log.length);
	assertTrue(log[0] instanceof SetInactiveNextBallot);
	assertTrue(log[1] instanceof SetNextBallot);
	assertTrue(log[2] instanceof SetInactiveNextBallot);
    }

    public void testPrevVote() {
	BallotNumber b = new BallotNumber(1, 23);
	Vote v1 = new Vote(b, new Proposal(1, new byte[][] { { 69 } }));
	Vote v2 = new Vote(b, new Proposal(2, new byte[][] { { 42 } }));

	l.setPrevVote(1, v1);
	assertEquals(v1, l.getPrevVote(1));
	l.setPrevVote(1, v2);
	assertEquals(v2, l.getPrevVote(1));
	assertNull(l.getPrevVote(2));

	LedgerChange[] log = l.flushLog();
	assertEquals(2, log.length);
	assertTrue(log[0] instanceof SetPrevVote);
	assertTrue(log[1] instanceof SetPrevVote);
    }

    public void testReadWrite() {
	Proposal p1 = new Proposal(1, new byte[][] { { 69 } });
	Proposal p2 = new Proposal(2, new byte[][] { { 42 } });

	l.write(1, p1);
	assertEquals(p1, l.read(1));
	l.write(1, p2);
	assertEquals(p2, l.read(1));
	l.write(1, null);
	assertNull(l.read(1));
	assertNull(l.read(0));
	assertNull(l.read(3));

	LedgerChange[] log = l.flushLog();
	assertEquals(3, log.length);
	assertTrue(log[0] instanceof Write);
	assertTrue(log[1] instanceof Write);
	assertTrue(log[2] instanceof Write);
    }

    public void testActive() {
	assertFalse(l.isActive(1));
	l.setNextBallot(2, null);
	assertFalse(l.isActive(1));
	l.setNextBallot(1, null);
	assertTrue(l.isActive(1));

	LedgerChange[] log = l.flushLog();
	assertEquals(2, log.length);
	assertTrue(log[0] instanceof SetNextBallot);
	assertTrue(log[1] instanceof SetNextBallot);
    }

    public void testDecided() {
	Proposal p = new Proposal(1, new byte[][] { { 69 } });

	assertFalse(l.isDecided(1));
	l.write(1, p);
	assertTrue(l.isDecided(1));
	l.setNextBallot(2, null);
	assertFalse(l.isDecided(2));
	l.write(2, null);
	assertTrue(l.isDecided(2));

	LedgerChange[] log = l.flushLog();
	assertEquals(3, log.length);
	assertTrue(log[0] instanceof Write);
	assertTrue(log[1] instanceof SetNextBallot);
	assertTrue(log[2] instanceof Write);
    }

    public void testLastActive() {
	Proposal p = new Proposal(1, new byte[][] { { 69 } });

	assertEquals(Ledger.NULL_COUNTER, l.lastActiveDecree());
	l.setNextBallot(2, null);
	assertEquals(2, l.lastActiveDecree());
	l.write(2, p);
	assertEquals(2, l.lastActiveDecree());
	l.setLastTried(4, null);
	assertEquals(4, l.lastActiveDecree());
	l.write(6, p);
	assertEquals(6, l.lastActiveDecree());
	l.setPrevVote(8, null);
	assertEquals(8, l.lastActiveDecree());
	l.read(23);
	l.getNextBallot(23);
	l.getLastTried(23);
	l.getPrevVote(23);
	assertEquals(8, l.lastActiveDecree());

	LedgerChange[] log = l.flushLog();
	assertEquals(5, log.length);
	assertTrue(log[0] instanceof SetNextBallot);
	assertTrue(log[1] instanceof Write);
	assertTrue(log[2] instanceof SetLastTried);
	assertTrue(log[3] instanceof Write);
	assertTrue(log[4] instanceof SetPrevVote);
    }

    public void testFirstUndecidedDecree() {
	Proposal p = new Proposal(1, new byte[][] { { 69 } });

	assertEquals(Ledger.FIRST_COUNTER, l.firstUndecidedDecree());
	l.setNextBallot(Ledger.FIRST_COUNTER + 2, null);
	assertEquals(Ledger.FIRST_COUNTER, l.firstUndecidedDecree());
	l.write(Ledger.FIRST_COUNTER + 2, p);
	assertEquals(Ledger.FIRST_COUNTER, l.firstUndecidedDecree());
	l.write(Ledger.FIRST_COUNTER, p);
	assertEquals(Ledger.FIRST_COUNTER + 1, l.firstUndecidedDecree());
	l.write(Ledger.FIRST_COUNTER + 1, p);
	assertEquals(Ledger.FIRST_COUNTER + 3, l.firstUndecidedDecree());

	LedgerChange[] log = l.flushLog();
	assertEquals(4, log.length);
	assertTrue(log[0] instanceof SetNextBallot);
	assertTrue(log[1] instanceof Write);
	assertTrue(log[2] instanceof Write);
	assertTrue(log[3] instanceof Write);
    }

    public void testLastDecidedDecree() {
	Proposal p = new Proposal(1, new byte[][] { { 69 } });

	assertEquals(Ledger.NULL_COUNTER, l.lastDecidedDecree());
	l.setNextBallot(Ledger.FIRST_COUNTER + 2, null);
	assertEquals(Ledger.NULL_COUNTER, l.lastDecidedDecree());
	l.write(Ledger.FIRST_COUNTER + 2, p);
	assertEquals(Ledger.FIRST_COUNTER + 2, l.lastDecidedDecree());
	l.write(Ledger.FIRST_COUNTER, p);
	assertEquals(Ledger.FIRST_COUNTER + 2, l.lastDecidedDecree());
	l.write(Ledger.FIRST_COUNTER + 4, p);
	assertEquals(Ledger.FIRST_COUNTER + 4, l.lastDecidedDecree());

	LedgerChange[] log = l.flushLog();
	assertEquals(4, log.length);
	assertTrue(log[0] instanceof SetNextBallot);
	assertTrue(log[1] instanceof Write);
	assertTrue(log[2] instanceof Write);
	assertTrue(log[3] instanceof Write);
    }
}
