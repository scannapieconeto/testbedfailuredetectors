/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import junit.framework.TestCase;

public class ChangeLogHistoryTest extends TestCase {

    private ChangeLogHistory ledgerHistory;

    @Override
    protected void setUp() {
	ledgerHistory = new ChangeLogHistory();
    }

    @Override
    public void tearDown() {
    }

    public void testEmptyLedgerHistory() {

	// General
	ledgerHistory.isEmpty();

	// Checkpoints
	Serializable checkpoint = ledgerHistory.readCheckpoint(10);
	assertNull(checkpoint);

	try {
	    ledgerHistory.deleteCheckpoint(0);
	    fail("deleteCheckpoint NOT ok.");
	} catch (NoSuchElementException e) {
	}

	checkpoint = ledgerHistory.readMostRecentCheckpoint();
	assertNull(checkpoint);

	long numberOfCheckpoints = -1;
	numberOfCheckpoints = ledgerHistory.numberOfCheckpoints();
	assertEquals(0, numberOfCheckpoints);

	long lastCheckpointId = 0;
	// if there are no checkpoints stored should return -1;
	lastCheckpointId = ledgerHistory.mostRecentCheckpoint();
	assertEquals(-1, lastCheckpointId);

	// Changes and Change Blocks
	LinkedList<Serializable> changes = null;
	changes = ledgerHistory.readChanges(0, 0);
	assertNull(changes);
	changes = ledgerHistory.readChanges(0, 1);
	assertNull(changes);
	changes = ledgerHistory.readChanges(10, 15);
	assertNull(changes);

	try {
	    ledgerHistory.deleteChanges(0, 0);
	    fail("deleteChanges NOT ok.");
	} catch (NoSuchElementException e) {
	}

	try {
	    ledgerHistory.deleteChanges(10, 13);
	    fail("deleteChanges NOT ok.");
	} catch (NoSuchElementException e) {
	}

	// Exceptional use of changesStoreReset
	// nOfChanges returned is unpredictable
	ledgerHistory.changesStoreReset(0);

	ledgerHistory.changesStoreReset(12);

	boolean hasNext = true;
	hasNext = ledgerHistory.changesStoreHasNextChange(2);
	assertFalse(hasNext);

	try {
	    checkpoint = ledgerHistory.changesStoreNextChange(1);
	    fail("changesStoreNextChange NOT ok.");
	} catch (NoSuchElementException e) {
	}
    }

    public void testWriteCheckpointNoChanges() {

	long lastCheckpoint = -1;

	long nextCheckpoint = 1;
	long nextChangeBlock = 0;

	ledgerHistory.writeCheckpoint(nextCheckpoint, "checkpoint1");

	Serializable checkpoint = ledgerHistory.readCheckpoint(nextCheckpoint);
	assertEquals("checkpoint1", checkpoint);

	LinkedList<Serializable> changes = null;
	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNull(changes);

	checkpoint = ledgerHistory.readMostRecentCheckpoint();
	assertEquals("checkpoint1", checkpoint);

	lastCheckpoint = ledgerHistory.mostRecentCheckpoint();
	assertEquals(nextCheckpoint, lastCheckpoint);

	nextCheckpoint++;

	ledgerHistory.writeCheckpoint(nextCheckpoint, "checkpoint2");

	checkpoint = ledgerHistory.readCheckpoint(nextCheckpoint);
	assertEquals("checkpoint2", checkpoint);

	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNull(changes);

	checkpoint = ledgerHistory.readMostRecentCheckpoint();
	assertEquals("checkpoint2", checkpoint);

	lastCheckpoint = ledgerHistory.mostRecentCheckpoint();
	assertEquals(nextCheckpoint, lastCheckpoint);

    }

    public void testWriteChangesNoCheckpoints() {
	long lastCheckpoint = -1;

	long nextCheckpoint = 1;
	long nextChangeBlock = 0;

	ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock, "change0");
	ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock, "change1");
	ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock, "change2");
	ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock, "change3");
	ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock, "change4");
	ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock, "change5");

	LinkedList<Serializable> changes = null;
	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(6, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("change" + i, changes.get(i));
	}

	ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock, "1change0");

	changes = null;
	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(1, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("1change" + i, changes.get(i));
	}

	lastCheckpoint = ledgerHistory.mostRecentCheckpoint();
	assertEquals(-1, lastCheckpoint);

	assertFalse(ledgerHistory.isEmpty());

	assertEquals(0, ledgerHistory.numberOfCheckpoints());

    }

    public void testWriteChangesAndCheckpoints() {

	long lastCheckpoint = -1;
	long nextCheckpoint = 1;
	long nextChangeBlock = 0;

	/*
	 * initializing NextCheckpoint to 1 and writing NextCheckpoint - 1 to
	 * mean 0 seems silly, but it is actually how Gustavo initiates his
	 * ChangeLog because he considers Checkpoint 0 as inexistent. so only
	 * the changes for Checkpoint 0 are written but not the actual
	 * checkpoint.
	 */

	ledgerHistory.writeCheckpoint(nextCheckpoint - 1, "checkpoint " + (nextCheckpoint - 1));

	for (long i = 0; i < 25; i++) {
	    ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock,
		    "[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock);
	}

	Serializable checkpoint = ledgerHistory.readCheckpoint(nextCheckpoint - 1);
	assertEquals("checkpoint 0", checkpoint);

	LinkedList<Serializable> changes = null;
	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(25, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock, changes.get(i));
	}

	ledgerHistory.writeCheckpoint(nextCheckpoint, "checkpoint " + nextCheckpoint);

	for (long i = 0; i < 1; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	changes = null;
	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(1, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + nextCheckpoint + "] change: " + nextChangeBlock, changes.get(i));
	}

	lastCheckpoint = ledgerHistory.mostRecentCheckpoint();
	assertEquals(nextCheckpoint, lastCheckpoint);

	assertFalse(ledgerHistory.isEmpty());

	assertEquals(2, ledgerHistory.numberOfCheckpoints());

    }

    public void testAllLedgerHistoryOperations() {
	long lastCheckpoint = -1;
	long nextCheckpoint = 1;
	long nextChangeBlock = 0;

	LinkedList<Serializable> changes = null;

	ledgerHistory.clear();

	assertTrue(ledgerHistory.isEmpty());

	/*
	 * checkpoint: 0 change block: 0 (128 changes) change block: 1 (16
	 * changes) change block: 2 (1 change) change block: 3 (323 changes)
	 */
	// checkpoint 0
	ledgerHistory.writeCheckpoint(nextCheckpoint - 1, "checkpoint " + (nextCheckpoint - 1));

	// change block 0
	for (long i = 0; i < 128; i++) {
	    ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock,
		    "[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock);
	}

	Serializable checkpoint = ledgerHistory.readCheckpoint(nextCheckpoint - 1);
	assertEquals("checkpoint 0", checkpoint);

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(128, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock, changes.get(i));
	}

	// change block 1
	nextChangeBlock++;

	for (long i = 0; i < 16; i++) {
	    ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock,
		    "[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock);
	}

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(16, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock, changes.get(i));
	}

	// change block 2
	nextChangeBlock++;

	for (long i = 0; i < 1; i++) {
	    ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock,
		    "[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock);
	}

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(1, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock, changes.get(i));
	}

	// change block 3
	nextChangeBlock++;

	for (long i = 0; i < 323; i++) {
	    ledgerHistory.writeChange(nextCheckpoint - 1, nextChangeBlock,
		    "[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock);
	}

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(323, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + (nextCheckpoint - 1) + "] change: " + nextChangeBlock, changes.get(i));
	}

	assertEquals(0, ledgerHistory.mostRecentCheckpoint());
	assertEquals(1, ledgerHistory.numberOfCheckpoints());

	// begin: verification of change blocks
	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertEquals(323, changes.size());

	nextChangeBlock--;

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertEquals(1, changes.size());

	nextChangeBlock--;

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertEquals(16, changes.size());

	nextChangeBlock--;

	changes = ledgerHistory.readChanges(nextCheckpoint - 1, nextChangeBlock);
	assertEquals(128, changes.size());

	// checkpoint 1
	nextChangeBlock = 0;
	ledgerHistory.writeCheckpoint(nextCheckpoint, "checkpoint " + nextCheckpoint);

	for (long i = 0; i < 1; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	changes = null;
	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(1, changes.size());

	for (int i = (changes.size() - 1); i >= 0; i--) {
	    assertEquals("[" + nextCheckpoint + "] change: " + nextChangeBlock, changes.get(i));
	}

	lastCheckpoint = ledgerHistory.mostRecentCheckpoint();
	assertEquals(nextCheckpoint, lastCheckpoint);

	assertFalse(ledgerHistory.isEmpty());

	assertEquals(2, ledgerHistory.numberOfCheckpoints());

	// checkpoint 2
	nextCheckpoint++;

	ledgerHistory.writeCheckpoint(nextCheckpoint, "checkpoint " + nextCheckpoint);

	nextChangeBlock = 0;
	for (long i = 0; i < 8; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	nextChangeBlock++;
	for (long i = 0; i < 1; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	nextChangeBlock++;
	for (long i = 0; i < 1028; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	nextChangeBlock++;
	for (long i = 0; i < 6; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	assertEquals(3, ledgerHistory.numberOfCheckpoints());
	assertEquals(2, ledgerHistory.mostRecentCheckpoint());

	nextCheckpoint = 0;

	ledgerHistory.changesStoreReset(nextCheckpoint);

	Serializable change = null;
	int numberOfChanges = 0;
	while (ledgerHistory.changesStoreHasNextChange(nextCheckpoint)) {
	    try {
		change = ledgerHistory.changesStoreNextChange(nextCheckpoint);
		assertNotNull(change);
	    } catch (NoSuchElementException e) {
		fail("no such element");
	    }
	    numberOfChanges++;
	}
	assertEquals(468, numberOfChanges);

	nextCheckpoint++;

	ledgerHistory.changesStoreReset(nextCheckpoint);

	change = null;
	numberOfChanges = 0;
	while (ledgerHistory.changesStoreHasNextChange(nextCheckpoint)) {
	    try {
		change = ledgerHistory.changesStoreNextChange(nextCheckpoint);
		assertNotNull(change);
	    } catch (NoSuchElementException e) {
		fail("no such element");
	    }
	    numberOfChanges++;
	}
	assertEquals(1, numberOfChanges);

	nextCheckpoint++;
	ledgerHistory.changesStoreReset(nextCheckpoint);

	change = null;
	numberOfChanges = 0;
	while (ledgerHistory.changesStoreHasNextChange(nextCheckpoint)) {
	    try {
		change = ledgerHistory.changesStoreNextChange(nextCheckpoint);
		assertNotNull(change);
	    } catch (NoSuchElementException e) {
		fail("no such element");
	    }
	    numberOfChanges++;
	}
	assertEquals(1043, numberOfChanges);

	nextCheckpoint = 1;
	nextChangeBlock = 0;
	try {
	    ledgerHistory.deleteCheckpoint(nextCheckpoint);
	} catch (NoSuchElementException e) {
	    fail("no such element");
	}

	ledgerHistory.changesStoreReset(nextCheckpoint);

	assertEquals(2, ledgerHistory.numberOfCheckpoints());
	assertEquals(2, ledgerHistory.mostRecentCheckpoint());

	try {
	    ledgerHistory.deleteChanges(nextCheckpoint, nextChangeBlock);
	} catch (NoSuchElementException e) {
	    fail("no such element");
	}

	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNull(changes);

	// re-write checkpoint 1 and changes
	// start with changes as changes and checkpoints
	// can be manipulated independently of each other.

	nextCheckpoint = 1;
	nextChangeBlock = 0;
	for (long i = 0; i < 17; i++) {
	    ledgerHistory.writeChange(nextCheckpoint, nextChangeBlock,
		    "[" + nextCheckpoint + "] change: " + nextChangeBlock);
	}

	changes = ledgerHistory.readChanges(nextCheckpoint, nextChangeBlock);
	assertNotNull(changes);
	assertEquals(17, changes.size());

	ledgerHistory.writeCheckpoint(nextCheckpoint, nextChangeBlock);

	assertEquals(3, ledgerHistory.numberOfCheckpoints());

	// remove everything and make sure that ledgerHistory
	// is empty

	nextCheckpoint = 0;
	long numberOfCheckpoints = ledgerHistory.numberOfCheckpoints();
	assertEquals(3, numberOfCheckpoints);
	while (numberOfCheckpoints > 0) {
	    nextCheckpoint = ledgerHistory.mostRecentCheckpoint();
	    try {
		ledgerHistory.deleteCheckpoint(nextCheckpoint);
		ledgerHistory.deleteChanges(nextCheckpoint, nextChangeBlock);
	    } catch (NoSuchElementException e) {
		fail("no such element");
	    }
	    numberOfCheckpoints--;
	}

	assertEquals(0, ledgerHistory.numberOfCheckpoints());

	ledgerHistory.clear();

    }

}
