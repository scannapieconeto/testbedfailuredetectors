/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.Serializable;

import junit.framework.TestCase;

public class CheckpointStoreTest extends TestCase {

    private CheckpointStore checkpointStore;

    @Override
    protected void setUp() {
	checkpointStore = new CheckpointStore();
    }

    @Override
    protected void tearDown() {
    }

    public void testExerciseCheckpointStore() {
	long lastCheckpoint = -666;
	Serializable checkpoint = null;

	lastCheckpoint = checkpointStore.mostRecentCheckpoint();
	assertEquals(-1, lastCheckpoint);
	checkpoint = checkpointStore.readMostRecentCheckpoint();
	assertNull(checkpoint);

	checkpointStore.writeCheckpoint(0, "checkpoint 0");
	checkpointStore.writeCheckpoint(1, "checkpoint 1");
	checkpointStore.writeCheckpoint(2, "checkpoint 2");
	checkpointStore.writeCheckpoint(3, "checkpoint 3");
	checkpointStore.writeCheckpoint(4, "checkpoint 4");
	assertEquals(checkpointStore.numberOfCheckpoints(), 5);

	checkpoint = checkpointStore.readMostRecentCheckpoint();
	assertEquals(checkpoint, "checkpoint 4");
	lastCheckpoint = checkpointStore.mostRecentCheckpoint();
	assertEquals(4, lastCheckpoint);

	String deletedCheckpoint = (String) checkpointStore.deleteCheckpoint(4);
	assertEquals(checkpoint, deletedCheckpoint);

	checkpoint = checkpointStore.readMostRecentCheckpoint();
	assertEquals(checkpoint, "checkpoint 3");

	deletedCheckpoint = (String) checkpointStore.deleteCheckpoint(3);
	assertEquals(checkpoint, deletedCheckpoint);

	checkpointStore.writeCheckpoint(3, "new checkpoint 3");
	checkpoint = checkpointStore.readMostRecentCheckpoint();
	assertEquals(checkpoint, "new checkpoint 3");

	String aCheckpoint = (String) checkpointStore.readCheckpoint(0);
	assertEquals(aCheckpoint, "checkpoint 0");

	aCheckpoint = (String) checkpointStore.readCheckpoint(1);
	assertEquals(aCheckpoint, "checkpoint 1");

	// should read "new checkpoint 3"
	aCheckpoint = (String) checkpointStore.readMostRecentCheckpoint();
	assertEquals(aCheckpoint, checkpoint);

	aCheckpoint = (String) checkpointStore.deleteCheckpoint(3);
	aCheckpoint = (String) checkpointStore.deleteCheckpoint(2);
	aCheckpoint = (String) checkpointStore.deleteCheckpoint(1);
	aCheckpoint = (String) checkpointStore.deleteCheckpoint(0);

	lastCheckpoint = checkpointStore.mostRecentCheckpoint();
	assertEquals(-1, lastCheckpoint);
	aCheckpoint = (String) checkpointStore.readMostRecentCheckpoint();
	assertEquals(aCheckpoint, null);
	assertEquals(0, checkpointStore.numberOfCheckpoints());

	checkpointStore.writeCheckpoint(0, "new checkpoint 0");

	checkpoint = checkpointStore.readMostRecentCheckpoint();
	assertEquals("new checkpoint 0", checkpoint);

	assertEquals(1, checkpointStore.numberOfCheckpoints());

	checkpointStore.writeCheckpoint(1, "new checkpoint 1");

	checkpoint = checkpointStore.readMostRecentCheckpoint();
	assertEquals("new checkpoint 1", checkpoint);

	assertEquals(2, checkpointStore.numberOfCheckpoints());

    }

}
