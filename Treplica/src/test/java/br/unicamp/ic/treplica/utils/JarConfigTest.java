package br.unicamp.ic.treplica.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

import org.junit.Test;

import br.unicamp.ic.treplica.utils.JarConfig.FailureDetectorConfig;
import br.unicamp.ic.treplica.utils.JarConfig.LoadGeneratorConfig;
import br.unicamp.ic.treplica.utils.JarConfig.OpponentConfig;
import br.unicamp.ic.treplica.utils.JarConfig.TreplicaConfig;

public class JarConfigTest {

    @Test
    public void getJarConfig() {
        final TreplicaConfig treplica = JarConfig.INST.getTreplica();
        final FailureDetectorConfig failureDetector = JarConfig.INST.getFailureDetector();
        final OpponentConfig opponent = JarConfig.INST.getOpponent();
        final LoadGeneratorConfig loadGenerator = JarConfig.INST.getLoadGenerator();

        // Checking Treplica config.
        assertThat("treplica.getPaxosAlgorithm()", treplica.getPaxosAlgorithm(),
                anyOf(is("BASIC_PAXOS"), is("FAST_PAXOS")));
        assertThat("treplica.getRoundTrip()", treplica.getRoundTrip(), greaterThanOrEqualTo(0));
        assertThat("treplica.getIp()", treplica.getIp(), not(isEmptyOrNullString()));
        assertThat("treplica.getPort()", treplica.getRoundTrip(), greaterThanOrEqualTo(0));

        // Checking Failure Detector config.
        assertThat("failureDetector.getAlgorithm()", failureDetector.getAlgorithm(),
                anyOf(is("LARREA_VANILLA"), is("LARREA_EPOCH"), is("AGUILERA"), is("CHEN")));
        assertThat("failureDetector.getDelta()", failureDetector.getDelta(), greaterThanOrEqualTo(0));
        assertThat("failureDetector.getIp()", failureDetector.getIp(), not(isEmptyOrNullString()));
        assertThat("failureDetector.getPort()", failureDetector.getPort(), greaterThanOrEqualTo(0));
        assertThat("failureDetector.getOutputDir()", failureDetector.getOutputDir(), not(isEmptyOrNullString()));

        // Checking Opponent config.
        assertThat("opponent.isTreplicaOpponentEnabled()", opponent.isTreplicaOpponentEnabled(),
                anyOf(is(true), is(false)));
        assertThat("opponent.isFdOpponentEnabled()", opponent.isFdOpponentEnabled(), anyOf(is(true), is(false)));
        assertThat("opponent.isProcessFailureLeaderOnly()", opponent.isProcessFailureLeaderOnly(),
                anyOf(is(true), is(false)));
        assertThat("opponent.isNetworkFailureLeaderOnly()", opponent.isNetworkFailureLeaderOnly(),
                anyOf(is(true), is(false)));
        assertThat("opponent.getDelayToStabilizeElection()", opponent.getDelayToStabilizeElection(),
                greaterThanOrEqualTo(0));
        assertThat("opponent.getNetworkStrategy()", opponent.getNetworkStrategy(),
                anyOf(is("NO_NETWORK_STRATEGY"), is("ENUMERATED_DISTRIBUTION_NETWORK_STRATEGY"),
                		is("NORMAL_DISTRIBUTION_NETWORK_STRATEGY")));
        assertThat("opponent.getNetworkStrategyMean()", opponent.getNetworkStrategyMean(),
				greaterThanOrEqualTo(0));
		assertThat("opponent.getNetworkStrategyStandardDeviation()", opponent.getNetworkStrategyStandardDeviation(),
				greaterThanOrEqualTo(0));

        // Checking Load Generator config.
        assertThat("loadGenerator.getNumberOfReplicas()", loadGenerator.getNumberOfReplicas(), greaterThanOrEqualTo(0));
        assertThat("loadGenerator.getStableMedia()", loadGenerator.getStableMedia(), not(isEmptyOrNullString()));
        assertThat("loadGenerator.getOutputFile()", loadGenerator.getOutputFile(), not(isEmptyOrNullString()));
        assertThat("loadGenerator.getRequestRate()", loadGenerator.getRequestRate(), greaterThanOrEqualTo(0L));
        assertThat("loadGenerator.getExecDuration()", loadGenerator.getExecDuration(), greaterThanOrEqualTo(0L));
        assertThat("loadGenerator.getStartExecution()", loadGenerator.getStartExecution(), greaterThanOrEqualTo(0L));
    }
}
