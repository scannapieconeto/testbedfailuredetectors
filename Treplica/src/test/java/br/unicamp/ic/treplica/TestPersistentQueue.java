/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

import java.io.Serializable;
import java.util.LinkedList;

import br.unicamp.ic.treplica.common.Marshall;

/**
 * This class implements a test persistent queue using a simple local queue.
 * <p>
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class TestPersistentQueue implements PersistentQueue {

    public Serializable initialState;
    public LinkedList<Serializable> initialQueue;
    private LinkedList<Serializable> queue;
    private StateManager stateManager;

    /**
     * Creates a new test persistent queue with previous state and queued
     * messages.
     * <p>
     * 
     * @param state
     *            the initial state.
     * @param messages
     *            a list of queued messages after the initial state.
     */
    public TestPersistentQueue(Serializable state, LinkedList<Serializable> messages) {
	initialState = state;
	initialQueue = messages;
	queue = null;
	stateManager = null;
    }

    /**
     * Creates a new empty test persistent queue with no previous state.
     * <p>
     */
    public TestPersistentQueue() {
	this(null, new LinkedList<Serializable>());
    }

    /*
     * (non-Javadoc)
     * 
     * @see PersistentQueue#bind(StateManager)
     */
    public void bind(StateManager manager) throws TreplicaIOException, TreplicaSerializationException {
	if (initialState == null && initialQueue.isEmpty()) {
	    initialState = Marshall.unmarshall(Marshall.marshall(manager.getState()));
	} else {
	    manager.setState(Marshall.unmarshall(Marshall.marshall(initialState)));
	}
	queue = new LinkedList<Serializable>(initialQueue);
	stateManager = manager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see PersistentQueue#enqueue(Serializable)
     */
    public synchronized void enqueue(Serializable message) throws TreplicaIOException, TreplicaSerializationException {
	initialQueue.addLast(message);
	queue.addLast(message);
	this.notify();
    }

    /*
     * (non-Javadoc)
     * 
     * @see PersistentQueue#dequeue()
     */
    public synchronized Serializable dequeue() {
	if (queue.size() == 0) {
	    try {
		this.wait();
	    } catch (InterruptedException e) {
	    }
	}
	return queue.removeFirst();
    }

    /*
     * (non-Javadoc)
     * 
     * @see PersistentQueue#checkpoint()
     */
    public synchronized void checkpoint() throws TreplicaSerializationException {
	initialState = Marshall.unmarshall(Marshall.marshall(stateManager.getState()));
	initialQueue = new LinkedList<Serializable>(queue);
    }

    @Override
    public void shutdown() {
    }
}
