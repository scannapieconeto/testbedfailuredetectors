/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import junit.framework.TestCase;

public class PaxosDataStructuresTest extends TestCase {

    @Override
    protected void setUp() {
    }

    public void testBallotNumberEqualsHashCode() {
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 5);
	BallotNumber b3 = new BallotNumber(2, 23);
	BallotNumber b4 = new BallotNumber(1, 23);

	assertTrue(b1.equals(b4));
	assertTrue(b4.equals(b1));
	assertTrue(b1.hashCode() == b4.hashCode());
	assertFalse(b1.equals(b2));
	assertFalse(b1.hashCode() == b2.hashCode());
	assertFalse(b1.equals(b3));
	assertFalse(b1.hashCode() == b3.hashCode());
    }

    public void testBallotNumberEqualsHashCodeFast() {
	BallotNumber b1 = new BallotNumber(1, 23, true);
	BallotNumber b2 = new BallotNumber(1, 23, false);
	BallotNumber b3 = new BallotNumber(1, 23, true);

	assertTrue(b1.equals(b3));
	assertTrue(b1.hashCode() == b3.hashCode());
	assertFalse(b1.equals(b2));
	assertFalse(b1.hashCode() == b2.hashCode());
    }

    public void testBallotNumberCompareTo() {
	BallotNumber b1 = new BallotNumber(1, 23);
	BallotNumber b2 = new BallotNumber(1, 5);
	BallotNumber b3 = new BallotNumber(2, 23);
	BallotNumber b4 = new BallotNumber(1, 23);
	BallotNumber b5 = new BallotNumber(3, 7);

	assertEquals(0, b1.compareTo(b4));
	assertTrue(b1.compareTo(b2) > 0);
	assertTrue(b2.compareTo(b1) < 0);
	assertTrue(b1.compareTo(b3) < 0);
	assertTrue(b3.compareTo(b1) > 0);
	assertTrue(b1.compareTo(b5) > 0);
	assertTrue(b5.compareTo(b1) < 0);
    }

    public void testBallotNumberCompareToFast() {
	BallotNumber b1 = new BallotNumber(1, 23, true);
	BallotNumber b2 = new BallotNumber(1, 23, false);
	BallotNumber b3 = new BallotNumber(1, 23, true);

	assertEquals(0, b1.compareTo(b3));
	assertTrue(b1.compareTo(b2) < 0);
	assertTrue(b2.compareTo(b1) > 0);
    }

    public void testBallotNumberToString() {
	assertEquals("1|5(c)", new BallotNumber(1, 5).toString());
	assertEquals("2|23(f)", new BallotNumber(2, 23, true).toString());
    }

    public void testProposalEqualsHashCode() {
	Proposal p1 = new Proposal(0, new byte[][] { { 5 } });
	Proposal p2 = new Proposal(1, new byte[][] { { 5 } });
	Proposal p3 = new Proposal(0, new byte[][] { { 5 } });
	Proposal p4 = new Proposal(0, null);

	assertTrue(p1.equals(p3));
	assertTrue(p3.equals(p1));
	assertTrue(p1.hashCode() == p3.hashCode());
	assertFalse(p1.equals(p2));
	assertFalse(p1.hashCode() == p2.hashCode());
	assertTrue(p1.equals(p4));
	assertTrue(p4.equals(p1));
	assertTrue(p1.hashCode() == p4.hashCode());
    }

    public void testProposalNullArguments() {
	try {
	    new Proposal(0, null);
	} catch (NullPointerException e) {
	    fail();
	}
    }

    public void testProposalToString() {
	assertEquals("0", new Proposal(0, new byte[][] { { 23 } }).toString());
	assertEquals("0", new Proposal(0, null).toString());
    }

    public void testVoteEqualsHashCode() {
	BallotNumber b1 = new BallotNumber(1, 2);
	BallotNumber b2 = new BallotNumber(1, 3);
	Proposal p1 = new Proposal(1, new byte[][] { { 5 } });
	Proposal p2 = new Proposal(2, new byte[][] { { 23 } });

	Vote v1 = new Vote(b1, p1);
	Vote v2 = new Vote(b1, p2);
	Vote v3 = new Vote(b2, p1);
	Vote v4 = new Vote(b1, p1);
	Vote v5 = new Vote(b1, null);

	assertTrue(v1.equals(v4));
	assertTrue(v4.equals(v1));
	assertTrue(v1.hashCode() == v4.hashCode());
	assertFalse(v1.equals(v2));
	assertFalse(v1.hashCode() == v2.hashCode());
	assertFalse(v1.equals(v3));
	assertFalse(v1.hashCode() == v3.hashCode());
	assertFalse(v1.equals(v5));
	assertFalse(v5.equals(v1));
	assertFalse(v1.hashCode() == v5.hashCode());
    }

    public void testVoteNullArguments() {
	try {
	    new Vote(new BallotNumber(1, 23), null);
	} catch (NullPointerException e) {
	    fail();
	}
	try {
	    new Vote(null, null);
	} catch (NullPointerException e) {
	    return;
	}
	fail();
    }

    public void testVoteToString() {
	assertEquals("2e0001", new Vote(new BallotNumber(1, 23), new Proposal(1, new byte[][] { { 23 } })).toString());
	assertEquals("2e0000", new Vote(new BallotNumber(1, 23), null).toString());
    }

    public void testPaxosMessageToString() {
	BallotNumber b = new BallotNumber(1, 2);
	Proposal p = new Proposal(1, new byte[][] { { 5 } });
	Vote v = new Vote(b, p);

	assertEquals("1:10:0:1|2(c):40001:1", new PaxosMessage(1, PaxosMessage.ANY, 0, b, v, p).toString());
	assertEquals("1:10:0:1|2(c):40001:null", new PaxosMessage(1, PaxosMessage.ANY, 0, b, v, null).toString());
	assertEquals("1:10:0:1|2(c):null:null", new PaxosMessage(1, PaxosMessage.ANY, 0, b, null, null).toString());
	assertEquals("1:10:0:null:null:null", new PaxosMessage(1, PaxosMessage.ANY, 0, null, null, null).toString());
    }

}
