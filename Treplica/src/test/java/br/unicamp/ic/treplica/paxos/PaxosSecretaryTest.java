/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.LinkedList;

import br.unicamp.ic.treplica.TestStateManager;
import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.common.TestChangeLog;
import br.unicamp.ic.treplica.common.TestChangeLogClient;
import br.unicamp.ic.treplica.common.TestTransport;
import br.unicamp.ic.treplica.common.TestTransportId;
import br.unicamp.ic.treplica.paxos.ledger.GenerateNextClassicBallotNumber;
import br.unicamp.ic.treplica.paxos.ledger.GenerateNextFastBallotNumber;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import br.unicamp.ic.treplica.paxos.ledger.LedgerChange;
import br.unicamp.ic.treplica.paxos.ledger.LoggingLedger;
import br.unicamp.ic.treplica.paxos.ledger.SetInactiveLastTried;
import br.unicamp.ic.treplica.paxos.ledger.SetInactiveNextBallot;
import junit.framework.TestCase;

public class PaxosSecretaryTest extends TestCase {

    private TestTransport t;
    private LinkedList<Delivery> q;
    private TestChangeLog log;
    private Secretary s;

    @Override
    protected void setUp() throws Exception {
	t = new TestTransport();
	q = new LinkedList<Delivery>();
	log = new TestChangeLog();
	s = new Secretary(log, t, q);
	s.bind(10, new TestStateManager());
    }

    public void testBind() throws TreplicaException {
	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());
    }

    public void testMessages() throws Exception {
	s.queueMessage("hello1");
	s.queueMessage("hello2", new TestTransportId(2));
	assertNull(t.popMemory());
	s.synchronousFlush();
	assertEquals("hello1", t.popMemory().getPayload());
	assertEquals("hello2", t.popMemory().getPayload());
	assertNull(t.popMemory());
	assertNull(q.peekFirst());
    }

    public void testDeliveries() throws Exception {
	Delivery d1 = new Delivery(0, new byte[] { 23 });
	Delivery d2 = new Delivery(1, new byte[] { 42 });
	s.queueDelivery(d1);
	s.queueDelivery(d2);
	assertNull(q.peekFirst());
	s.synchronousFlush();
	assertEquals(d1, q.removeFirst());
	assertEquals(d2, q.removeFirst());
	assertNull(q.peekFirst());
	assertNull(t.popMemory());
    }

    public void testUnchangedLedger() throws Exception {
	Delivery d = new Delivery(0, new byte[] { 23 });
	s.queueMessage("hello1");
	s.queueDelivery(d);
	s.synchronousFlush();
	assertEquals("hello1", t.popMemory().getPayload());
	assertEquals(d, q.removeFirst());
	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());
    }

    public void testLedger1() throws Exception {
	Ledger ledger = s.getLedger();
	ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());

	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());

	s.synchronousFlush();
	client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(1, client.changes.size());
	LedgerChange[] changes = (LedgerChange[]) client.changes.get(0);
	assertEquals(2, changes.length);
	assertTrue(changes[0] instanceof GenerateNextFastBallotNumber);
	assertTrue(changes[1] instanceof SetInactiveLastTried);
    }

    public void testLedger2() throws Exception {
	Ledger ledger = s.getLedger();
	ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
	s.synchronousFlush();
	ledger.setInactiveNextBallot(ledger.generateNextClassicBallotNumber());

	s.synchronousFlush();
	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(2, client.changes.size());
	LedgerChange[] changes = (LedgerChange[]) client.changes.get(0);
	assertEquals(2, changes.length);
	assertTrue(changes[0] instanceof GenerateNextFastBallotNumber);
	assertTrue(changes[1] instanceof SetInactiveLastTried);
	changes = (LedgerChange[]) client.changes.get(1);
	assertEquals(2, changes.length);
	assertTrue(changes[0] instanceof GenerateNextClassicBallotNumber);
	assertTrue(changes[1] instanceof SetInactiveNextBallot);
    }

    public void testCheckpoint() throws Exception {
	s.queueCheckpoint("checkpoint", 2);

	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());

	s.synchronousFlush();
	client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals("checkpoint", ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(2, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());
    }

    public void testCheckpointAndChanges1() throws Exception {
	Ledger ledger = s.getLedger();
	BallotNumber ballot = ledger.generateNextFastBallotNumber();
	ledger.setInactiveLastTried(ballot);
	s.queueCheckpoint("checkpoint", 2);

	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	assertEquals(10, ((PaxosCheckpoint) client.lastCheckpoint).getLedger().getPaxosId());
	assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());

	s.synchronousFlush();
	client = new TestChangeLogClient();
	log.copy().open(client);
	ledger = ((PaxosCheckpoint) client.lastCheckpoint).getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals("checkpoint", ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(2, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());
    }

    public void testCheckpointAndChanges2() throws Exception {
	s.queueCheckpoint("checkpoint", 2);
	s.synchronousFlush();
	Ledger ledger = s.getLedger();
	ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
	s.synchronousFlush();
	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	ledger = ((PaxosCheckpoint) client.lastCheckpoint).getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertNull(ledger.getInactiveLastTried());
	assertEquals("checkpoint", ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(2, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(1, client.changes.size());
	LedgerChange[] changes = (LedgerChange[]) client.changes.get(0);
	assertEquals(2, changes.length);
	assertTrue(changes[0] instanceof GenerateNextFastBallotNumber);
	assertTrue(changes[1] instanceof SetInactiveLastTried);
    }

    public void testRecoveryNoClientCheckpoint() throws Exception {
	LoggingLedger loggingLedger = new LoggingLedger(10);
	BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
	loggingLedger.setInactiveLastTried(ballot);
	TestChangeLog newLog = new TestChangeLog();
	newLog.open(new TestChangeLogClient());
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL, Ledger.FIRST_COUNTER));
	newLog.writeChange(loggingLedger.flushLog());
	loggingLedger.setInactiveNextBallot(ballot);
	newLog.writeChange(loggingLedger.flushLog());

	s = new Secretary(newLog.copy(), t, q);
	TestStateManager state = new TestStateManager();
	assertEquals(Ledger.FIRST_COUNTER, s.bind(20, state));
	assertEquals(TestStateManager.INITIAL, state.state);
	Ledger ledger = s.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals(ballot, ledger.getInactiveNextBallot());
    }

    public void testRecoveryNoChanges() throws Exception {
	LoggingLedger loggingLedger = new LoggingLedger(10);
	BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
	loggingLedger.setInactiveLastTried(ballot);
	loggingLedger.setInactiveNextBallot(ballot);
	TestChangeLog newLog = new TestChangeLog();
	newLog.open(new TestChangeLogClient());
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL, Ledger.FIRST_COUNTER));
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, "checkpoint", 2));

	s = new Secretary(newLog.copy(), t, q);
	TestStateManager state = new TestStateManager();
	assertEquals(2, s.bind(20, state));
	assertEquals("checkpoint", state.state);
	Ledger ledger = s.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals(ballot, ledger.getInactiveNextBallot());
    }

    public void testRecoveryCheckpointAndChanges1() throws Exception {
	LoggingLedger loggingLedger = new LoggingLedger(10);
	BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
	loggingLedger.setInactiveLastTried(ballot);
	TestChangeLog newLog = new TestChangeLog();
	newLog.open(new TestChangeLogClient());
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL, Ledger.FIRST_COUNTER));
	newLog.writeChange(loggingLedger.flushLog());
	loggingLedger.setInactiveNextBallot(ballot);
	newLog.writeChange(loggingLedger.flushLog());
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, "checkpoint", 2));

	s = new Secretary(newLog.copy(), t, q);
	TestStateManager state = new TestStateManager();
	assertEquals(2, s.bind(20, state));
	assertEquals("checkpoint", state.state);
	Ledger ledger = s.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals(ballot, ledger.getInactiveNextBallot());
    }

    public void testRecoveryCheckpointAndChanges2() throws Exception {
	LoggingLedger loggingLedger = new LoggingLedger(10);
	BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
	TestChangeLog newLog = new TestChangeLog();
	newLog.open(new TestChangeLogClient());
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL, Ledger.FIRST_COUNTER));
	newLog.writeCheckpoint(new PaxosCheckpoint(loggingLedger, "checkpoint", 2));
	loggingLedger.setInactiveLastTried(ballot);
	newLog.writeChange(loggingLedger.flushLog());
	loggingLedger.setInactiveNextBallot(ballot);
	newLog.writeChange(loggingLedger.flushLog());

	s = new Secretary(newLog.copy(), t, q);
	TestStateManager state = new TestStateManager();
	assertEquals(2, s.bind(20, state));
	assertEquals("checkpoint", state.state);
	Ledger ledger = s.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals(ballot, ledger.getInactiveNextBallot());
    }

    public void testReBind() throws TreplicaException {
	Ledger ledger = s.getLedger();
	ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
	s.queueCheckpoint("checkpoint", 2);
	s.synchronousFlush();
	BallotNumber ballot = ledger.generateNextFastBallotNumber();
	ledger.setInactiveLastTried(ballot);

	TestStateManager state = new TestStateManager();
	assertEquals(2, s.bind(20, state));
	assertEquals("checkpoint", state.state);
	ledger = s.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
    }

    public void testTimedMessages() throws Exception {
	s.queueMessage("hello1");
	s.queueMessage("hello2", new TestTransportId(2));
	assertNull(t.popMemory());
	s.flush();
	Thread.sleep(20);
	assertEquals("hello1", t.popMemory().getPayload());
	assertEquals("hello2", t.popMemory().getPayload());
	assertNull(t.popMemory());
	assertNull(q.peekFirst());
    }

    public void testTimedDeliveries() throws Exception {
	Delivery d1 = new Delivery(0, new byte[] { 23 });
	Delivery d2 = new Delivery(1, new byte[] { 42 });
	s.queueDelivery(d1);
	s.queueDelivery(d2);
	assertNull(q.peekFirst());
	s.flush();
	Thread.sleep(20);
	assertEquals(d1, q.removeFirst());
	assertEquals(d2, q.removeFirst());
	assertNull(q.peekFirst());
	assertNull(t.popMemory());
    }

    public void testTimedLedger() throws Exception {
	Ledger ledger = s.getLedger();
	BallotNumber ballot = ledger.generateNextFastBallotNumber();
	ledger.setInactiveLastTried(ballot);

	Secretary probe = new Secretary(log.copy(), t, q);
	TestStateManager state = new TestStateManager();
	assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
	assertEquals(TestStateManager.INITIAL, state.state);
	ledger = probe.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertNull(ledger.getInactiveLastTried());
	s.flush();
	Thread.sleep(20);
	probe = new Secretary(log.copy(), t, q);
	state = new TestStateManager();
	assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
	assertEquals(TestStateManager.INITIAL, state.state);
	ledger = probe.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertNull(t.popMemory());
	assertNull(q.peekFirst());
    }

    public void testTimedAll() throws Exception {
	Ledger ledger = s.getLedger();
	BallotNumber ballot = ledger.generateNextFastBallotNumber();
	ledger.setInactiveLastTried(ballot);
	s.queueMessage("send");
	Delivery d = new Delivery(0, new byte[] { 23 });
	s.queueDelivery(d);

	Secretary probe = new Secretary(log.copy(), t, q);
	TestStateManager state = new TestStateManager();
	assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
	assertEquals(TestStateManager.INITIAL, state.state);
	ledger = probe.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertNull(ledger.getInactiveLastTried());
	assertNull(t.popMemory());
	assertNull(q.peekFirst());
	s.flush();
	Thread.sleep(20);
	probe = new Secretary(log.copy(), t, q);
	state = new TestStateManager();
	assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
	assertEquals(TestStateManager.INITIAL, state.state);
	ledger = probe.getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals("send", t.popMemory().getPayload());
	assertEquals(d, q.removeFirst());
    }

    public void testTimedLedgerAndCheckpoint() throws Exception {
	Ledger ledger = s.getLedger();
	BallotNumber ballot = ledger.generateNextFastBallotNumber();
	ledger.setInactiveLastTried(ballot);
	s.flush();
	s.queueCheckpoint("checkpoint", 2);
	s.synchronousFlush();
	TestChangeLogClient client = new TestChangeLogClient();
	log.copy().open(client);
	ledger = ((PaxosCheckpoint) client.lastCheckpoint).getLedger();
	assertEquals(10, ledger.getPaxosId());
	assertEquals(ballot, ledger.getInactiveLastTried());
	assertEquals("checkpoint", ((PaxosCheckpoint) client.lastCheckpoint).getApplication());
	assertEquals(2, ((PaxosCheckpoint) client.lastCheckpoint).getNextDecree());
	assertEquals(0, client.changes.size());
    }
}
