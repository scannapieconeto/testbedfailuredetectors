/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos.ledger;

import java.util.LinkedList;

import br.unicamp.ic.treplica.paxos.BallotNumber;
import br.unicamp.ic.treplica.paxos.Proposal;
import br.unicamp.ic.treplica.paxos.Vote;

/**
 * This class implements a version of the Paxos ledger that registers the
 * changes that should be made persistent.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class LoggingLedger implements Ledger {
    private static final long serialVersionUID = 3518490027955837534L;

    private TransientLedger ledger;
    private LinkedList<LedgerChange> log;

    /**
     * Creates a new ledger.
     * <p>
     *
     * @param paxosId
     *            the id of this Paxos instance, used to create ballots.
     */
    public LoggingLedger(final int paxosId) {
        ledger = new TransientLedger(paxosId);
        log = new LinkedList<LedgerChange>();
    }

    /**
     * Returns and clears the current log of changes of this ledger.
     * <p>
     *
     * @return the current log of changes to this ledger.
     */
    public LedgerChange[] flushLog() {
        LedgerChange[] oldLog = new LedgerChange[log.size()];
        oldLog = log.toArray(oldLog);
        log = new LinkedList<LedgerChange>();
        return oldLog;
    }

    /**
     * Replays the provided log of changes in this logging ledger. The replayed
     * operations won't appear in the current log of changes of this ledger.
     * <p>
     *
     * @param changeLog
     *            a log of changes to be replayed.
     */
    public void replayLog(final LedgerChange[] changeLog) {
        for (int i = 0; i < changeLog.length; i++) {
            changeLog[i].executeOnLedger(ledger);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getPaxosId()
     */
    @Override
    public int getPaxosId() {
        return ledger.getPaxosId();
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getLastTried(long)
     */
    @Override
    public BallotNumber getLastTried(final long decreeNumber) {
        return ledger.getLastTried(decreeNumber);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setLastTried(long, BallotNumber)
     */
    @Override
    public void setLastTried(final long decreeNumber, final BallotNumber ballot) {
        performChange(new SetLastTried(decreeNumber, ballot));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getInactiveLastTried()
     */
    @Override
    public BallotNumber getInactiveLastTried() {
        return ledger.getInactiveLastTried();
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setInactiveLastTried(BallotNumber)
     */
    @Override
    public void setInactiveLastTried(final BallotNumber ballot) {
        performChange(new SetInactiveLastTried(ballot));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getNextBallot(long)
     */
    @Override
    public BallotNumber getNextBallot(final long decreeNumber) {
        return ledger.getNextBallot(decreeNumber);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setNextBallot(long, BallotNumber)
     */
    @Override
    public void setNextBallot(final long decreeNumber, final BallotNumber ballot) {
        performChange(new SetNextBallot(decreeNumber, ballot));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getInactiveNextBallot()
     */
    @Override
    public BallotNumber getInactiveNextBallot() {
        return ledger.getInactiveNextBallot();
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setInactiveNextBallot(BallotNumber)
     */
    @Override
    public void setInactiveNextBallot(final BallotNumber ballot) {
        performChange(new SetInactiveNextBallot(ballot));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getPrevVote(long)
     */
    @Override
    public Vote getPrevVote(final long decreeNumber) {
        return ledger.getPrevVote(decreeNumber);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setPrevVote(long, Vote)
     */
    @Override
    public void setPrevVote(final long decreeNumber, final Vote vote) {
        performChange(new SetPrevVote(decreeNumber, vote));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#read(long)
     */
    @Override
    public Proposal read(final long decreeNumber) {
        return ledger.read(decreeNumber);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#write(long, Proposal)
     */
    @Override
    public void write(final long decreeNumber, final Proposal proposal) {
        performChange(new Write(decreeNumber, proposal));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#generateNextClassicBallotNumber()
     */
    @Override
    public BallotNumber generateNextClassicBallotNumber() {
        return (BallotNumber) performChange(new GenerateNextClassicBallotNumber());
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#generateNextFastBallotNumber()
     */
    @Override
    public BallotNumber generateNextFastBallotNumber() {
        return (BallotNumber) performChange(new GenerateNextFastBallotNumber());
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#ensureHigherBallotNumber(BallotNumber)
     */
    @Override
    public void ensureHigherBallotNumber(final BallotNumber ballot) {
        performChange(new EnsureHigherBallotNumber(ballot));
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#lastActiveDecree()
     */
    @Override
    public long lastActiveDecree() {
        return ledger.lastActiveDecree();
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#firstUndecidedDecree()
     */
    @Override
    public long firstUndecidedDecree() {
        return ledger.firstUndecidedDecree();
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#lastDecidedDecree()
     */
    @Override
    public long lastDecidedDecree() {
        return ledger.lastDecidedDecree();
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#isActive(long)
     */
    @Override
    public boolean isActive(final long decreeNumber) {
        return ledger.isActive(decreeNumber);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#isDecided(long)
     */
    @Override
    public boolean isDecided(final long decreeNumber) {
        return ledger.isDecided(decreeNumber);
    }

    /**
     * Performs a change on this ledger.
     * <p>
     *
     * @param change
     *            the change.
     * @return the return value of the change.
     * @see LedgerChange#executeOnLedger(Ledger)
     */
    protected Object performChange(final LedgerChange change) {
        log.addLast(change);
        return change.executeOnLedger(ledger);
    }

}
