/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos.ledger;

import java.io.Serializable;
import java.util.ArrayList;

import br.unicamp.ic.treplica.paxos.BallotNumber;
import br.unicamp.ic.treplica.paxos.Proposal;
import br.unicamp.ic.treplica.paxos.Vote;

/**
 * This class implements a transient version of the Paxos ledger.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TransientLedger implements Ledger {
    private static final long serialVersionUID = -4696672903888980180L;

    private int paxosId;
    private long nextBallotCounter;
    private ArrayList<Decree> decrees;
    private BallotNumber inactiveLastTried;
    private BallotNumber inactiveNextBallot;
    private long lastActiveDecree;
    private long firstUndecidedDecree;
    private long lastDecidedDecree;

    /**
     * Creates a new ledger.
     * <p>
     *
     * @param paxosId
     *            the id of this Paxos instance, used to create ballots.
     */
    public TransientLedger(final int paxosId) {
        this.paxosId = paxosId;
        nextBallotCounter = Ledger.FIRST_COUNTER;
        decrees = new ArrayList<Decree>();
        inactiveLastTried = null;
        inactiveNextBallot = null;
        lastActiveDecree = NULL_COUNTER;
        firstUndecidedDecree = Ledger.FIRST_COUNTER;
        lastDecidedDecree = NULL_COUNTER;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getPaxosId()
     */
    @Override
    public int getPaxosId() {
        return paxosId;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getLastTried(long)
     */
    @Override
    public BallotNumber getLastTried(final long decreeNumber) {
        BallotNumber ballot = inactiveLastTried;
        if (isActive(decreeNumber)) {
            ballot = decrees.get(decreeNumberToIndex(decreeNumber)).lastTried;
        }
        return ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setLastTried(long, BallotNumber)
     */
    @Override
    public void setLastTried(final long decreeNumber, final BallotNumber ballot) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.lastTried = ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getInactiveLastTried()
     */
    @Override
    public BallotNumber getInactiveLastTried() {
        return inactiveLastTried;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setInactiveLastTried(BallotNumber)
     */
    @Override
    public void setInactiveLastTried(final BallotNumber ballot) {
        inactiveLastTried = ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getNextBallot(long)
     */
    @Override
    public BallotNumber getNextBallot(final long decreeNumber) {
        BallotNumber ballot = inactiveNextBallot;
        if (isActive(decreeNumber)) {
            ballot = decrees.get(decreeNumberToIndex(decreeNumber)).nextBallot;
        }
        return ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setNextBallot(long, BallotNumber)
     */
    @Override
    public void setNextBallot(final long decreeNumber, final BallotNumber ballot) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.nextBallot = ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getInactiveNextBallot()
     */
    @Override
    public BallotNumber getInactiveNextBallot() {
        return inactiveNextBallot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setInactiveNextBallot(BallotNumber)
     */
    @Override
    public void setInactiveNextBallot(final BallotNumber ballot) {
        inactiveNextBallot = ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#getPrevVote(long)
     */
    @Override
    public Vote getPrevVote(final long decreeNumber) {
        Vote vote = null;
        if (isActive(decreeNumber)) {
            vote = decrees.get(decreeNumberToIndex(decreeNumber)).prevVote;
        }
        return vote;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#setPrevVote(long, Vote)
     */
    @Override
    public void setPrevVote(final long decreeNumber, final Vote vote) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.prevVote = vote;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#read(long)
     */
    @Override
    public Proposal read(final long decreeNumber) {
        Proposal proposal = null;
        if (isActive(decreeNumber)) {
            proposal = decrees.get(decreeNumberToIndex(decreeNumber)).finalProposal;
        }
        return proposal;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#write(long, Proposal)
     */
    @Override
    public void write(final long decreeNumber, final Proposal proposal) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.finalProposal = proposal;
        decree.isDecided = true;

        if (decreeNumber == firstUndecidedDecree) {
            firstUndecidedDecree++;
            while (isDecided(firstUndecidedDecree)) {
                firstUndecidedDecree++;
            }
        }
        if (decreeNumber > lastDecidedDecree) {
            lastDecidedDecree = decreeNumber;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#generateNextClassicBallotNumber()
     */
    @Override
    public BallotNumber generateNextClassicBallotNumber() {
        return new BallotNumber(paxosId, nextBallotCounter++);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#generateNextFastBallotNumber()
     */
    @Override
    public BallotNumber generateNextFastBallotNumber() {
        return new BallotNumber(paxosId, nextBallotCounter++, true);
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#ensureHigherBallotNumber(BallotNumber)
     */
    @Override
    public void ensureHigherBallotNumber(final BallotNumber ballot) {
        if (ballot.getCounter() >= nextBallotCounter) {
            nextBallotCounter = ballot.getCounter() + 1;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#lastActiveDecree()
     */
    @Override
    public long lastActiveDecree() {
        return lastActiveDecree;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#firstUndecidedDecree()
     */
    @Override
    public long firstUndecidedDecree() {
        return firstUndecidedDecree;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#lastDecidedDecree()
     */
    @Override
    public long lastDecidedDecree() {
        return lastDecidedDecree;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#isActive(long)
     */
    @Override
    public boolean isActive(final long decreeNumber) {
        int index = decreeNumberToIndex(decreeNumber);
        return index < decrees.size() && decrees.get(index) != null;
    }

    /*
     * (non-Javadoc)
     *
     * @see Ledger#isDecided(long)
     */
    @Override
    public boolean isDecided(final long decreeNumber) {
        boolean decided = false;
        if (isActive(decreeNumber)) {
            decided = decrees.get(decreeNumberToIndex(decreeNumber)).isDecided;
        }
        return decided;
    }

    /**
     * Activates a decree, initializing its data to values consistent with
     * previous promises.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     */
    private void activateDecree(final long decreeNumber) {
        if (!isActive(decreeNumber)) {
            Decree decree = new Decree();
            decree.lastTried = inactiveLastTried;
            decree.nextBallot = inactiveNextBallot;
            decree.prevVote = null;
            decree.finalProposal = null;
            decree.isDecided = false;
            int index = decreeNumberToIndex(decreeNumber);
            for (int i = decrees.size(); i <= index; i++) {
                decrees.add(null);
            }
            decrees.set(index, decree);

            if (decreeNumber > lastActiveDecree) {
                lastActiveDecree = decreeNumber;
            }
        }
    }

    /**
     * Converts a decree number to the index in local data structure used to
     * store it.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return the index in local data structure used to store a decree.
     */
    private int decreeNumberToIndex(final long decreeNumber) {
        return (int) decreeNumber;
    }

    /**
     * This class holds the data for a decree.
     * <p>
     */
    protected class Decree implements Serializable {
        private static final long serialVersionUID = -5274189340573380319L;
        protected BallotNumber lastTried;
        protected BallotNumber nextBallot;
        protected Vote prevVote;
        protected Proposal finalProposal;
        protected boolean isDecided;
    }
}
