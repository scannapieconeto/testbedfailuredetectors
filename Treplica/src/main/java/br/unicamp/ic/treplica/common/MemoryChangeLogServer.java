/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright �� 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.unicamp.ic.treplica.TreplicaIOException;

public class MemoryChangeLogServer extends Thread {

    protected Logger logger;

    private static final int DEFAULT_TCP_PORT = 6667;
    private ServerSocket server;

    protected HashMap<Integer, ChangeLogHistory> histories = new HashMap<Integer, ChangeLogHistory>();

    public static void main(final String[] args) throws IOException {

        int serverPort;

        if (args.length != 1) {
            System.out.println("Usage: MemoryChangeLogServer [serverPort]");
            System.out.println();
            System.out.println("Warning: serverPort not specified, using default.");
            System.out.println();
            serverPort = DEFAULT_TCP_PORT;
        } else {
            serverPort = Integer.parseInt(args[0]);
            System.out.println("Server IP entered: " + args[0]);
        }

        System.out.println("Treplica Main-Memory Store");
        System.out.println("--------------------------");
        System.out.println();

        try {
            new MemoryChangeLogServer(serverPort);
        } catch (TreplicaIOException e) {
            throw new IOException(e.getMessage());
        }
    }

    MemoryChangeLogServer(final int serverPort) throws TreplicaIOException {
        logger = LoggerFactory.getLogger(this.getClass());

        try {
            server = new ServerSocket(serverPort);
            logger.info("Listening on port: {}", serverPort);
        } catch (IOException e) {
            throw new TreplicaIOException(e);
        }
        this.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket aClient = server.accept();
                logger.info("Connection accepted [client: {} ].", aClient.getInetAddress());

                // Spawns a thread per request accepted
                Thread persistor = new Thread(new Persistor(aClient));
                persistor.start();
            } catch (IOException e) {
                logger.error("Method run of MemoryChangeLogServer: ", e);
                return;
            }
        }
    }

    public void shutdown() throws IOException {
        server.close();
    }

    private class Persistor implements Runnable {

        private ChangeLogHistory thisHistory = null;

        private static final long UNKNOWN_VALUE = -1;
        private static final String NULL_CHECKPOINT = "null checkpoint";

        private Socket client = null;
        private ObjectInputStream fromClient = null;
        private ObjectOutputStream toClient = null;

        private long activeCheckpoint;
        private long activeChangeBlock;

        private ServerAction action;
        private int processId;

        Persistor(final Socket client) {

            this.client = client;

            action = ServerAction.WAITING;

            try {

                /* Acquire streams from persistorClient (Socket) */
                toClient = new ObjectOutputStream(client.getOutputStream());
                toClient.flush();

                fromClient = new ObjectInputStream(client.getInputStream());

            } catch (Exception se) {
                logger.error(se.getMessage(), se);
            }

        }

        private void closeStreams() {
            try {
                toClient.close();
                fromClient.close();
            } catch (IOException e) {
            }
        }

        @Override
        public void run() {

            outerloop: while (true) {

                // begin: identification, action
                try {
                    processId = fromClient.readInt();
                } catch (EOFException e0) {
                    if (client.isInputShutdown()) {
                        logger.error("Client closed its output stream.");
                    }
                    break outerloop;
                } catch (IOException e2) {
                    logger.error("IOException returned: ", e2);
                    break outerloop;
                }

                try {
                    action = (ServerAction) fromClient.readObject();
                } catch (EOFException e0) {
                    if (client.isInputShutdown()) {
                        logger.error("Client closed its output stream.");
                    } else {
                        logger.error("EOFException returned: ", e0);
                    }
                    break outerloop;
                } catch (ClassNotFoundException e1) {
                    logger.error("ClassNotFoundException returned: ", e1);
                    break outerloop;
                } catch (IOException e2) {
                    logger.error("IOException returned: ", e2);
                    break outerloop;
                }

                try {
                    toClient.writeInt(processId);
                    toClient.flush();
                } catch (EOFException e0) {
                    break outerloop;
                } catch (IOException e1) {
                    logger.error("IOException writeObject processId", e1);
                    break outerloop;
                }
                // end: identification, action

                switch (action) {

                    case INIT: {

                        /*
                         * Initialization of these variables depend on the
                         * previous state of the server
                         *
                         * FIXME: Recovery should be treated here in the near
                         * future
                         */

                        logger.debug("INIT: BEGIN");

                        activeCheckpoint = UNKNOWN_VALUE;
                        activeChangeBlock = UNKNOWN_VALUE;

                        logger.debug("INIT: activeCheckpoint : {}", activeCheckpoint);
                        logger.debug("INIT: activeChangeBlock : {}", activeChangeBlock);

                        // ***** IMPORTANT *****
                        // Exceptional processing.
                        // FIXME: Recovery should be treated here!
                        // FIXME: At normal thread death should update
                        // ledgerStore
                        // ***** IMPORTANT *****

                        if (histories.containsKey(processId)) {
                            thisHistory = histories.get(processId);
                        } else {
                            thisHistory = new ChangeLogHistory();
                            histories.put(processId, thisHistory);
                        }

                        logger.debug("INIT:   END");

                        break;
                    }

                    case LOAD_LAST_CHECKPOINT: {

                        logger.debug("LOAD_LAST_CHECKPOINT: BEGIN");

                        Serializable checkpoint = null;

                        /*
                         * An empty checkpoint store returns -1, null as
                         * lastCheckpoint and checkpoint, respectively
                         */

                        activeCheckpoint = thisHistory.mostRecentCheckpoint();
                        checkpoint = thisHistory.readMostRecentCheckpoint();

                        logger.debug("LOAD_LAST_CHECKPOINT: activeCheckpoint: {}", activeCheckpoint);
                        logger.debug("LOAD_LAST_CHECKPOINT: checkpoint: {}", checkpoint);

                        try {
                            toClient.writeLong(activeCheckpoint);
                            toClient.flush();
                        } catch (IOException e) {
                            logger.error("LOAD_LAST_CHECKPOINT: writeLong: {}", activeCheckpoint);
                            break outerloop;
                        }

                        if (checkpoint == null) {
                            checkpoint = NULL_CHECKPOINT;
                        }

                        try {
                            toClient.writeObject(checkpoint);
                            toClient.flush();
                        } catch (IOException e) {
                            logger.error("LOAD_LAST_CHECKPOINT: writeLong: {}", activeCheckpoint);
                            break outerloop;
                        }

                        logger.debug("LOAD_LAST_CHECKPOINT: activeCheckpoint : {}", activeCheckpoint);
                        logger.debug("LOAD_LAST_CHECKPOINT: END");

                        break;
                    }

                    case LOAD_CHANGE_BLOCKS: {
                        Serializable change = null;

                        logger.debug("LOAD_CHANGE_BLOCS: BEGIN");

                        long numberOfChangeBlocks = 0;

                        try {
                            activeCheckpoint = fromClient.readLong();
                        } catch (EOFException e0) {
                            logger.error("EOFException message: ", e0);
                            break outerloop;
                        } catch (IOException e1) {
                            logger.error("IOException message: ", e1);
                            break outerloop;
                        }

                        numberOfChangeBlocks = thisHistory.changesStoreReset(activeCheckpoint);

                        logger.debug("LOAD_CHANGE_BLOCKS: activeCheckpoint : {}", activeCheckpoint);
                        logger.debug("LOAD_CHANGE_BLOCKS: numberOfChangeBlocks : {}", numberOfChangeBlocks);

                        try {
                            toClient.writeLong(numberOfChangeBlocks);
                            toClient.flush();
                        } catch (IOException e) {
                            logger.error("LOAD_CHANGE_BLOCKS: writeLong: numberOfChangeBlocks: {}",
                                    numberOfChangeBlocks);
                            break outerloop;
                        }

                        while (thisHistory.changesStoreHasNextChange(activeCheckpoint)) {

                            try {
                                change = thisHistory.changesStoreNextChange(activeCheckpoint);

                                logger.debug("LOAD_CHANGE_BLOCKS: change : {}", change);

                            } catch (NoSuchElementException e) {
                                break outerloop;
                            }

                            try {
                                toClient.writeObject(change);
                                toClient.flush();

                            } catch (IOException e1) {
                                logger.error(e1.getMessage(), e1);
                                break outerloop;
                            }

                        }

                        logger.debug("LOAD_CHANGE_BLOCS: nextChangeBlock : {}", activeChangeBlock);
                        logger.debug("LOAD_CHANGE_BLOCS:   END");

                        break;
                    }

                    case WRITE_CHANGE: {
                        Serializable change = null;

                        logger.debug("WRITE_CHANGE: BEGIN");

                        try {
                            activeCheckpoint = fromClient.readLong();
                        } catch (EOFException e0) {
                            logger.error("EOFException message: ", e0);
                            break outerloop;
                        } catch (IOException e1) {
                            logger.error("IOException message: ", e1);
                            break outerloop;
                        }

                        try {
                            activeChangeBlock = fromClient.readLong();
                        } catch (EOFException e0) {
                            logger.error("EOFException message: ", e0);
                            break outerloop;
                        } catch (IOException e1) {
                            logger.error("IOException message: ", e1);
                            break outerloop;
                        }

                        try {
                            change = (Serializable) fromClient.readObject();
                        } catch (EOFException e0) {
                            logger.error(e0.getMessage(), e0);
                            break outerloop;
                        } catch (ClassNotFoundException e1) {
                            logger.error(e1.getMessage(), e1);
                            break outerloop;
                        } catch (IOException e2) {
                            logger.error(e2.getMessage(), e2);
                            break outerloop;
                        }

                        logger.debug("WRITE_CHANGE: nextCheckpoint : {}", activeCheckpoint);
                        logger.debug("WRITE_CHANGE: nextChangeBlock: {}", activeChangeBlock);
                        logger.debug("WRITE_CHANGE: change : {}", change);

                        thisHistory.writeChange(activeCheckpoint, activeChangeBlock, change);

                        try {
                            toClient.writeLong(activeChangeBlock);
                            toClient.flush();
                        } catch (IOException e) {
                            logger.error(e.getMessage(), e);
                            break outerloop;
                        }

                        logger.debug("WRITE_CHANGE: END");

                        break;
                    }

                    case WRITE_CHECKPOINT: {
                        Serializable checkpoint = null;

                        logger.debug("WRITE_CHECKPOINT: BEGIN");

                        try {
                            activeCheckpoint = fromClient.readLong();
                        } catch (IOException e) {
                            logger.error(e.getMessage(), e);
                            break outerloop;
                        }

                        try {
                            checkpoint = (Serializable) fromClient.readObject();
                        } catch (IOException e) {
                            logger.error(e.getMessage(), e);
                            break outerloop;
                        } catch (ClassNotFoundException e1) {
                            logger.error(e1.getMessage(), e1);
                            break outerloop;
                        }

                        thisHistory.writeCheckpoint(activeCheckpoint, checkpoint);

                        try {
                            toClient.writeLong(activeCheckpoint);
                            toClient.flush();
                        } catch (IOException e) {
                            logger.error("writeLong IOException: ", e);
                            break outerloop;
                        }

                        logger.debug("WRITE_CHECKPOINT: activeCheckpoint: {}", activeCheckpoint);
                        logger.debug("WRITE_CHECKPOINT: nextChangeBlock: {}", activeChangeBlock);
                        logger.debug("WRITE_CHECKPOINT: END");

                        break;

                    }

                    default:
                        logger.error("Persistor: Unexpected persistor state.");
                }

            } // end of outerloop

        closeStreams();
        }
    }
}
