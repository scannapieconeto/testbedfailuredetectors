package br.unicamp.ic.treplica.election.chen;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.election.IFailureDetector;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;

/**
 * Failure detector algorithm based on paper 'On the Quality of Service of
 * Failure Detectors' (ChenTouegAguilera2002).
 */
public class ChenFD extends IFailureDetector {

    private final ScheduledExecutorService leaderElectionExecutor = Executors.newScheduledThreadPool(1);

    private final Map<Integer, ChenFDElement> processesMap = new ConcurrentHashMap<Integer, ChenFDElement>();

    private final Set<ChenFDElement> processSet = new TreeSet<>(new ChenFDElement.IdComparator());

    private volatile long sequenceNumber;

    /**
     * Constructor.
     *
     * @param delta
     *            Delta in milliseconds.
     * @param client
     *            Election client.
     * @param transport
     *            Transport.
     */
    public ChenFD(final int delta, final ElectionClient client, final ITransport transport) {
        // Call super class constructor.
        super(delta, client, transport);

        this.sequenceNumber = 0;
    }

    /*
     * IFailureDetector implementation.
     */

    @Override
    protected void onTimeoutEvent() throws TreplicaIOException, TreplicaSerializationException {
        /*
         * The process will multicast a heartbeat message every 'onTimeoutDelta'
         * milliseconds.
         */
        this.transport.sendMessage(new ChenFDMsg(this.myId, ++this.sequenceNumber));
    }

    @Override
    protected void onMessageEvent(final IMessage message) throws TreplicaIOException, TreplicaSerializationException {
        final ChenFDMsg msgPayload = (ChenFDMsg) message.getPayload();
        final int msgSender = msgPayload.getSender();
        final long msgSequenceNumber = msgPayload.getSequenceNumber();

        final ChenFDElement processElem = this.processesMap.get(msgSender);
        final long currentNanoTime = System.nanoTime();

        if (processElem != null) {
            processElem.processMessage(msgSequenceNumber, currentNanoTime, this.onTimeoutDeltaInNanos);
        } else {
            this.processesMap.put(msgSender, new ChenFDElement(msgSender, msgSequenceNumber, currentNanoTime));
        }
    }

    @Override
    protected void runChild() {
        /*
         * Executor responsible for inferring the elected leader based on vector's content.
         */
        this.leaderElectionExecutor.scheduleAtFixedRate(getOnLeaderElectionRunnable(),
                0, this.onTimeoutDelta, TimeUnit.MILLISECONDS);
    }

    @Override
    protected void shutdownChild() {
        this.leaderElectionExecutor.shutdown();
    }

    /*
     * Private Methods.
     */

    private Runnable getOnLeaderElectionRunnable() {
        // Anonymous class.
        return new Runnable() {

            @Override
            public void run() {
                try {
                    /*
                     * Detect the new leader.
                     */
                    onDetectLeaderEvent(processesMap.values());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        };
    }

    /**
     * Attempt to detect the new leader. Try to elect the trustworthy process
     * with lower identifier.
     */
    private void onDetectLeaderEvent(final Collection<ChenFDElement> processes) {
        this.processSet.clear();
        this.processSet.addAll(processes);

        final long nanoTime = System.nanoTime();

        // Get trustworthy process with lower identifier (use a TreeSet and a
        // comparator to do it).
        for (final ChenFDElement processElem : this.processSet) {

            // Update trustworthy.
            processElem.updateTrustworthy(nanoTime, this.onTimeoutDeltaInNanos);

            if (processElem.isTrustworthy()) {

                if (processElem.getProcessId() != this.getLeader()) {
                    setLeader(processElem.getProcessId());
                }

                break;
            }
        }
    }
}
