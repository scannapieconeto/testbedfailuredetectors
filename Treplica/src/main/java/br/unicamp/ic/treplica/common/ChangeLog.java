/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.Serializable;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;

/**
 * This interface represents a persistent log of changes to an object, with
 * support for checkpointing. Changes can be persistently stored in the log and
 * the object can be reconstructed by replaying these changes. Checkpointing is
 * used to improve the performance of reconstruction, and can also be used to
 * reset the state of the object.
 * <p>
 *
 * A change log can be open or closed. A open change log accepts and executes
 * new operations. A closed change log will refuse new operations. Whenever a
 * open change log finds an error, it will signal the error to the caller and
 * will automatically close itself. It is the responsibility of the caller to
 * monitor the error and try to re-open the change log if appropriate.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface ChangeLog {

    /**
     * Opens and starts the operation of this change log. Before returning, this
     * method will recover the current state of the persistent object by piping
     * the most recent checkpoint and all subsequent changes to a change log
     * client provided as argument. This method guarantees it will only call the
     * <code>processCheckpoint()</code> method of the client once, and after all
     * changes are replayed by calling <code>processChange()</code> on the
     * client, no further calls will be made.
     * <p>
     *
     * Ordinary errors found during this process are ignored and necessary
     * measures are taken to ensure deterministic behavior after new changes or
     * checkpoints are added to the log, without necessarily correcting the
     * error. Unexpected errors will abort the opening of the persistent log.
     * <p>
     *
     * @param client
     *            the client for dependent state recovery.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     * @throws IllegalStateException
     *             if this change log is already open.
     * @see ChangeLogClient
     */
    void open(ChangeLogClient client) throws TreplicaIOException, TreplicaSerializationException;

    /**
     * Closes and stops operation of this change log. A closed change log can be
     * re-opened and will behave as a new change log.
     * <p>
     *
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws IllegalStateException
     *             if this change log is closed.
     */
    void close() throws TreplicaIOException;

    /**
     * Indicates the state of this change log.
     * <p>
     *
     * @return <code>true</code> if this change log is open; <code>false</code>
     *         otherwise.
     */
    boolean isOpen();

    /**
     * Writes a change to this change log. If an I/O error is found, it is
     * indicated to the caller of this method and the change log will be
     * automatically closed.
     * <p>
     *
     * @param change
     *            the change to be written.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws IllegalStateException
     *             if this change log is closed.
     */
    void writeChange(Serializable change) throws TreplicaIOException;

    /**
     * Writes a checkpoint to this change log. If an I/O error is found, it is
     * indicated to the caller of this method and the change log will be
     * automatically closed.
     * <p>
     *
     * @param checkpoint
     *            the checkpoint to be written.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws IllegalStateException
     *             if this change log is closed.
     */
    void writeCheckpoint(Serializable checkpoint) throws TreplicaIOException;

}
