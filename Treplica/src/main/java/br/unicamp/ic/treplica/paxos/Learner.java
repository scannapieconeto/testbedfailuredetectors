/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Random;

import br.unicamp.ic.treplica.common.LRUHashMap;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;

/**
 * This class implements the learner abstraction of Paxos. The learner has the
 * main function of discovering that a decree was passed. In this implementation
 * the learner also manages the delivery of application messages (asking for
 * retransmission of holes, if necessary) and handles local message proposals
 * (retransmitting until it passes), doubling as the proposer.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Learner {

    private Secretary secretary;
    private LinkedList<byte[]> pendingMessages;
    private HashMap<Long, Proposal> pendingProposals;
    private HashMap<Long, Long> pendingTimestamps;
    private int proposalWindowSize;
    private int proposalMaxSize;
    private Random random;
    private HashMap<Long, HashMap<BallotNumber, LearnerBallot>> pendingBallots;
    private LRUHashMap<Proposal, Proposal> knownProposals;
    private int knownProposalsMaxSize;
    private long nextToDeliver;
    private int maxProcesses;
    private int classicMajority;
    private int fastMajority;
    private int timeToPass;
    private long timestamp;
    private PaxosMessage any;

    /**
     * Creates a learner that accesses the ledger, sends and delivers messages
     * through the provided secretary. A learner also needs to know the number
     * of processes that establish a majority, how long it should wait for a
     * decree to pass and the next decree the application expects for delivery.
     * <p>
     *
     * @param secretary
     *            the secretary that handles I/O.
     * @param maxProcesses
     *            the maximum number of processes.
     * @param classicMajority
     *            the number of processes that establish a classic Paxos
     *            majority.
     * @param fastMajority
     *            the number of processes that establish a fast Paxos majority.
     * @param timeToPass
     *            the time to wait for a decree to pass.
     * @param nextToDeliver
     *            the next decree to be delivered to the application when
     *            decided.
     */
    public Learner(final Secretary secretary, final int maxProcesses, final int classicMajority, final int fastMajority,
            final int timeToPass, final long nextToDeliver) {
        this.secretary = secretary;
        pendingMessages = new LinkedList<byte[]>();
        pendingProposals = new HashMap<Long, Proposal>();
        pendingTimestamps = new HashMap<Long, Long>();
        proposalWindowSize = 2;
        // PPG 1 op/cycle
        // proposalMaxSize = 10000;
        proposalMaxSize = 1;
        random = new Random();
        pendingBallots = new HashMap<Long, HashMap<BallotNumber, LearnerBallot>>();
        knownProposalsMaxSize = proposalWindowSize * fastMajority * fastMajority;
        knownProposals = new LRUHashMap<Proposal, Proposal>(knownProposalsMaxSize);
        this.nextToDeliver = nextToDeliver;
        this.maxProcesses = maxProcesses;
        this.classicMajority = classicMajority;
        this.fastMajority = fastMajority;
        this.timeToPass = timeToPass;
        timestamp = System.currentTimeMillis();
        any = null;

    }

    /**
     * Processes a learner message received by the protocol.
     * <p>
     *
     * @param message
     *            the message received.
     */
    public void processMessage(final PaxosMessage message) {
        switch (message.getType()) {
            case PaxosMessage.ANY:
                processAny(message);
                break;
            case PaxosMessage.BEGIN_BALLOT:
                processBeginBallot(message);
                break;
            case PaxosMessage.VOTED:
                processVoted(message);
                break;
            case PaxosMessage.SUCCESS:
                processSuccess(message);
                break;
        }
    }

    /**
     * Processes a <code>PaxosMessage.ANY</code> message received by the
     * learner.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processAny(final PaxosMessage message) {
        Ledger ledger = secretary.getLedger();
        BallotNumber inactiveNextBallot = ledger.getInactiveNextBallot();
        if ((any == null || !(any.getBallotNumber().compareTo(message.getBallotNumber()) > 0))
                && (inactiveNextBallot == null || !(inactiveNextBallot.compareTo(message.getBallotNumber()) > 0))) {
            any = message;
        } else {
            BallotNumber larger = inactiveNextBallot;
            if (larger == null || (any != null && larger.compareTo(any.getBallotNumber()) < 0)) {
                larger = any.getBallotNumber();
            }
            PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.LARGER_BALLOT,
                    Ledger.NULL_COUNTER, larger, null, null);
            secretary.queueMessage(reply);
        }
    }

    /**
     * Processes a <code>PaxosMessage.BEGIN_BALLOT</code> message received by
     * the learner.
     * <p>
     *
     * The learner remembers all proposal presented for vote so the acceptors
     * can vote with only a reference to the proposal.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processBeginBallot(final PaxosMessage message) {
        if (message.getProposal() == null) {
            return;
        }
        knownProposals.put(message.getProposal(), message.getProposal());
    }

    /**
     * Processes a <code>PaxosMessage.VOTED</code> message received by the
     * learner.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processVoted(final PaxosMessage message) {
        Ledger ledger = secretary.getLedger();
        long decreeNumber = message.getDecreeNumber();
        if (!ledger.isDecided(decreeNumber)) {
            Proposal proposal = findProposal(message.getProposal());
            if (message.getProposal() != null && proposal == null) {
                return;
            }
            if (pendingProposals.get(decreeNumber) != null) {
                pendingTimestamps.put(decreeNumber, System.currentTimeMillis());
            }
            HashMap<BallotNumber, LearnerBallot> pendingBallotsForDecree = pendingBallots.get(decreeNumber);
            if (pendingBallotsForDecree == null) {
                pendingBallotsForDecree = new HashMap<BallotNumber, LearnerBallot>();
                pendingBallots.put(decreeNumber, pendingBallotsForDecree);
            }
            BallotNumber ballotNumber = message.getBallotNumber();
            LearnerBallot ballot = pendingBallotsForDecree.get(ballotNumber);
            if (ballot == null) {
                ballot = new LearnerBallot();
                pendingBallotsForDecree.put(ballotNumber, ballot);
            }
            ballot.registerVoted(message.getSender(), proposal);
            if ((ballotNumber.isFast() && ballot.getWinningQuorum() >= fastMajority)
                    || (!ballotNumber.isFast() && ballot.getWinningQuorum() >= classicMajority)) {
                writeDecree(decreeNumber, ballot.getWinningProposal());
            } else if (ballotNumber.isFast()) { /* Collision detection */
                if ((maxProcesses - ballot.receivedVotes() + ballot.getWinningQuorum()) < fastMajority
                        && pendingProposals.containsKey(decreeNumber)) {
                    Proposal pending = pendingProposals.get(decreeNumber);
                    pendingTimestamps.put(decreeNumber, System.currentTimeMillis());
                    PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.PASS, decreeNumber, null,
                            null, pending);
                    secretary.queueMessage(reply);
                }
            }
        }
    }

    /**
     * Discovers if this learner knows about the proposal identified by the
     * provided reference. A proposal reference is a proposal equal to the full
     * proposal, but that has a null message.
     * <p>
     *
     * @param reference
     *            the proposal reference.
     * @return the full proposal if found, <code>null</code> otherwise.
     */
    private Proposal findProposal(final Proposal reference) {
        if (reference == null) {
            return null;
        }
        return knownProposals.get(reference);
    }

    /**
     * Processes a <code>PaxosMessage.SUCCESS</code> message received by the
     * learner.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processSuccess(final PaxosMessage message) {
        Ledger ledger = secretary.getLedger();
        long decreeNumber = message.getDecreeNumber();
        if (!ledger.isDecided(decreeNumber)) {
            writeDecree(decreeNumber, message.getProposal());
        }
    }

    /**
     * Writes a decree in the ledger and delivers messages.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @param proposal
     *            the final proposal.
     */
    private void writeDecree(final long decreeNumber, final Proposal proposal) {
        Ledger ledger = secretary.getLedger();
        ledger.write(decreeNumber, proposal);
        deliver();
        if (decreeNumber > nextToDeliver) {
            removePending(decreeNumber, proposal);
        }
    }

    /**
     * Starts the process of ordering a message. This learner will create a
     * proposal and ask the coordinator to pass the proposal. It will monitor
     * this proposal (resending it, if necessary) until it is passed or the
     * learner crashes.
     * <p>
     *
     * @param message
     *            the message to be ordered.
     */
    public void order(final byte[] message) {
        pendingMessages.addLast(message);
        processMessageWindow();
    }

    /**
     * This method starts as many proposals as the current sending window
     * allows. Each proposal will contain as many messages as the maximum
     * proposal size allows.
     * <p>
     */
    private void processMessageWindow() {
        Ledger ledger = secretary.getLedger();
        long nextDecree = findNextDecree();
        while (nextDecree - ledger.firstUndecidedDecree() < proposalWindowSize && pendingMessages.size() > 0) {
            int messagesInProposal = 0;
            int messagesSize = 0;
            for (Iterator<byte[]> i = pendingMessages.iterator(); messagesSize <= proposalMaxSize && i.hasNext();) {
                messagesSize += i.next().length;
                messagesInProposal++;
            }
            byte[][] messages = new byte[messagesInProposal][];
            for (int i = 0; i < messagesInProposal; i++) {
                messages[i] = pendingMessages.removeFirst();
            }
            Proposal proposal = new Proposal(random.nextInt(), messages);
            addPending(proposal, nextDecree);
            nextDecree = findNextDecree();
        }
    }

    /**
     * This methods selects the next available decree number for passing a
     * proposal, using the information currently available to this learner.
     * <p>
     *
     * @return the next available decree number.
     */
    private long findNextDecree() {
        Ledger ledger = secretary.getLedger();
        long nextDecree = ledger.lastActiveDecree() + 1;
        while (pendingProposals.containsKey(nextDecree)) {
            nextDecree++;
        }
        return nextDecree;
    }

    /**
     * Requests that a new proposal be passed. This methods sends a suitable
     * request to pass this proposal as the provided decree.
     * <p>
     *
     * @param proposal
     *            the proposal.
     * @param nextDecree
     *            the decree.
     */
    private void addPending(final Proposal proposal, final long nextDecree) {
        Ledger ledger = secretary.getLedger();
        pendingProposals.put(nextDecree, proposal);
        pendingTimestamps.put(nextDecree, System.currentTimeMillis());
        PaxosMessage request;
        BallotNumber inactiveNextBallot = ledger.getInactiveNextBallot();
        if (any != null && (any.getDecreeNumber() <= nextDecree)
                && (inactiveNextBallot == null || !(any.getBallotNumber().compareTo(inactiveNextBallot) < 0))) {
            request = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.BEGIN_BALLOT, nextDecree,
                    any.getBallotNumber(), null, proposal);
        } else {
            request = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.PASS, nextDecree, null, null, proposal);
        }
        secretary.queueMessage(request);
    }

    /**
     * Registers that a the proposal has passed as the decree number. If there
     * is a pending proposal for this decree number and the proposals don't
     * match, the pending proposal is reassigned to a new decree number.
     * <p>
     *
     * @param decreeNumber
     *            the decree number
     * @param proposal
     *            the proposal
     */
    private void removePending(final long decreeNumber, final Proposal proposal) {
        Proposal pendingProposal = pendingProposals.remove(decreeNumber);
        pendingTimestamps.remove(decreeNumber);
        if (pendingProposal != null && !pendingProposal.equals(proposal)) {
            addPending(pendingProposal, findNextDecree());
        }
        pendingBallots.remove(nextToDeliver);
        processMessageWindow();
    }

    /**
     * Process a clock tick generated by the protocol.
     * <p>
     */
    public void processTick() {
        if ((System.currentTimeMillis() - timestamp) > timeToPass) {
            deliver();
            findGaps();
            timestamp = System.currentTimeMillis();
        }
        retryProposals();
    }

    /**
     * Delivers all messages associated with decided decrees that have not been
     * delivered, and which delivery does not create gaps.
     * <p>
     */
    private void deliver() {
        Ledger ledger = secretary.getLedger();
        while (ledger.isDecided(nextToDeliver)) {
            Proposal proposal = ledger.read(nextToDeliver);
            if (proposal != null) {
                byte[][] messages = proposal.getMessages();
                for (int i = 0; i < messages.length; i++) {
                    secretary.queueDelivery(new Delivery(nextToDeliver, messages[i]));
                }
            }
            removePending(nextToDeliver, proposal);
            nextToDeliver++;
        }
    }

    /**
     * Finds all gaps in the message sequence and sends a
     * <code>PaxosMessage.PASS</code> message for each gap.
     * <p>
     */
    private void findGaps() {
        Ledger ledger = secretary.getLedger();
        int sent = 0;
        for (long i = nextToDeliver; i < ledger.lastDecidedDecree() && sent < 100; i++) {
            if (!ledger.isDecided(i) && !pendingProposals.containsKey(i)) {
                PaxosMessage message = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.PASS, i, null, null, null);
                secretary.queueMessage(message);
                sent++;
            }
        }
    }

    /**
     * Sends a <code>PaxosMessage.PASS</code> message for all pending proposals.
     * <p>
     */
    private void retryProposals() {
        long now = System.currentTimeMillis();
        for (Entry<Long, Proposal> entry : pendingProposals.entrySet()) {
            long decreeNumber = entry.getKey().longValue();
            Proposal proposal = entry.getValue();
            long proposalTimestamp = pendingTimestamps.get(decreeNumber);
            if ((now - proposalTimestamp) > timeToPass) {
                PaxosMessage message = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.PASS, decreeNumber, null,
                        null, proposal);
                secretary.queueMessage(message);
            }
        }
    }

}
