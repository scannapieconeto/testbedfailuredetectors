/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.io.Serializable;

import br.unicamp.ic.treplica.paxos.ledger.LoggingLedger;

/**
 * This class holds all data required to restore a Paxos persistent queue
 * instance.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class PaxosCheckpoint implements Serializable {
    private static final long serialVersionUID = 1670117558796398344L;

    private LoggingLedger ledger;
    private Serializable application;
    private long nextDecree;

    /**
     * Creates a checkpoint.
     * <p>
     *
     * @param ledger
     *            the ledger of this checkpoint.
     * @param application
     *            the application of this checkpoint.
     * @param nextDecree
     *            the next decree the application would receive.
     */
    public PaxosCheckpoint(final LoggingLedger ledger, final Serializable application, final long nextDecree) {
        this.ledger = ledger;
        this.application = application;
        this.nextDecree = nextDecree;
    }

    /**
     * Returns the ledger of this checkpoint.
     * <p>
     *
     * @return the ledger of this checkpoint.
     */
    public LoggingLedger getLedger() {
        return ledger;
    }

    /**
     * Returns the application of this checkpoint.
     * <p>
     *
     * @return the application of this checkpoint.
     */
    public Serializable getApplication() {
        return application;
    }

    /**
     * Returns the next decree the application would receive.
     * <p>
     *
     * @return the next decree the application would receive.
     */
    public long getNextDecree() {
        return nextDecree;
    }

}
