package br.unicamp.ic.treplica.transport.opponent;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OpponentCommand {

    // Constants.
    public static final long NO_DELAY = 0;

    public static final OpponentCommand NO_FAILURE = OpponentCommand.builder()
            .delay(OpponentCommand.NO_DELAY)
            .drop(false)
            .build();

    public static final OpponentCommand DROP = OpponentCommand.builder()
            .delay(OpponentCommand.NO_DELAY)
            .drop(true)
            .build();

    // In milliseconds.
    private long delay;
    private boolean drop;
}
