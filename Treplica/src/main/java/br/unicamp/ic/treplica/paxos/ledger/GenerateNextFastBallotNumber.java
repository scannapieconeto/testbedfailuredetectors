/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos.ledger;

/**
 * This class implements the change performed by the
 * <code>generateNextFastBallotNumber()</code> method to the ledger.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 * @see Ledger#generateNextFastBallotNumber()
 */
public class GenerateNextFastBallotNumber implements LedgerChange {
    private static final long serialVersionUID = -4914411187541633581L;

    /*
     * (non-Javadoc)
     *
     * @see LedgerChange#executeOnLedger(Ledger)
     */
    @Override
    public Object executeOnLedger(final Ledger ledger) {
        return ledger.generateNextFastBallotNumber();
    }
}
