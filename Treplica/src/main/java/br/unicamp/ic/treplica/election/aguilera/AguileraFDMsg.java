package br.unicamp.ic.treplica.election.aguilera;

import java.io.Serializable;

class AguileraFDMsg implements Serializable {

    private static final long serialVersionUID = 5798096687478286815L;
    private final int sender;

    AguileraFDMsg(final int sender) {
        this.sender = sender;
    }

    int getSender() {
        return this.sender;
    }
}
