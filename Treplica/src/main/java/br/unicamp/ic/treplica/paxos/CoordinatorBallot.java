/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.HashMap;
import java.util.HashSet;

/**
 * This class holds the volatile data about a ballot, from the point of view of
 * a coordinator. Specifically, it counts the LAST_VOTE messages received and
 * computes the winning proposal.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class CoordinatorBallot {

    public static final int WAITING_LAST_VOTE = 0;
    public static final int PENDING_SUCCESS = 1;

    private int state;
    private long timestamp;
    private HashSet<Integer> lastVoteQuorum;
    private BallotNumber winningBallot;
    private int winningBallotMostVoted;
    private HashMap<Proposal, HashSet<Integer>> votes;
    private Proposal winningProposal;

    /**
     * Creates the volatile data about a ballot in a provided state and sets the
     * winning proposal. Possible states are
     * <code>Ballot.WAITING_LAST_VOTE</code> and
     * <code>Ballot.PENDING_SUCCESS</code>. A <code>null</code> proposal is
     * considered to be an empty message.
     * <p>
     *
     * When created in the state <code>Ballot.WAITING_LAST_VOTE</code>, a
     * winning proposal set in this constructor will remain if all members of a
     * majority sent LAST_VOTE messages with <code>null</code> votes. When
     * created in the state <code>Ballot.PENDING_SUCCESS</code>, the winning
     * proposal is considered to be consistent and ready to vote.
     *
     * @param state
     *            the state.
     * @param winningProposal
     *            the winning proposal.
     */
    public CoordinatorBallot(final int state, final Proposal winningProposal) {
        this.state = state;
        lastVoteQuorum = new HashSet<Integer>();
        winningBallot = null;
        winningBallotMostVoted = 0;
        votes = null;
        this.winningProposal = winningProposal;
        setTimestamp();
    }

    /**
     * Creates the volatile data about a ballot in a provided state and with
     * <code>null</code> as proposal. Possible states are
     * <code>Ballot.WAITING_LAST_VOTE</code> and
     * <code>Ballot.PENDING_SUCCESS</code>.
     * <p>
     *
     * @param state
     *            the state.
     */
    public CoordinatorBallot(final int state) {
        this(state, null);
    }

    /**
     * Sets the timestamp to the current time.
     * <p>
     */
    private void setTimestamp() {
        timestamp = System.currentTimeMillis();
    }

    /**
     * Returns the timestamp of the last message related change to this ballot.
     * <p>
     *
     * @return the timestamp of the last message related change to this ballot.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the state of this ballot. Possible values are
     * <code>Ballot.WAITING_LAST_VOTE</code> and
     * <code>Ballot.PENDING_SUCCESS</code>.
     * <p>
     *
     * @return the state of this ballot.
     */
    public int getState() {
        return state;
    }

    /**
     * Records the state of this ballot. Possible values are
     * <code>Ballot.WAITING_LAST_VOTE</code> and
     * <code>Ballot.PENDING_SUCCESS</code>.
     * <p>
     *
     * @param state
     *            the state of this ballot.
     */
    public void setState(final int state) {
        this.state = state;
    }

    /**
     * Registers a last vote message. If the process has already sent a last
     * vote message for this ballot, this method does nothing.
     * <p>
     *
     * @param process
     *            the process who cast the last vote.
     * @param vote
     *            the last vote.
     */
    public void registerLastVote(final int process, final Vote vote) {
        if (lastVoteQuorum.add(process)) {
            if (vote != null && (winningBallot == null || winningBallot.compareTo(vote.getBallotNumber()) < 0)) {
                winningBallot = vote.getBallotNumber();
                votes = new HashMap<Proposal, HashSet<Integer>>();
                winningBallotMostVoted = 0;
            }
            if (vote != null && vote.getBallotNumber().equals(winningBallot)) {
                HashSet<Integer> quorum = votes.get(vote.getProposal());
                if (quorum == null) {
                    quorum = new HashSet<Integer>();
                    votes.put(vote.getProposal(), quorum);
                }
                quorum.add(process);
                if (quorum.size() > winningBallotMostVoted) {
                    winningBallotMostVoted = quorum.size();
                    winningProposal = vote.getProposal();
                }
            }
            setTimestamp();
        }
    }

    /**
     * Returns the number of last votes registered for this ballot.
     * <p>
     *
     * @return the number of last votes registered for this ballot.
     */
    public int receivedLastVote() {
        return lastVoteQuorum.size();
    }

    /**
     * Returns the winning proposal. The winning proposal is the one present in
     * the vote with the largest ballot number for a classic Paxos ballot
     * number, the proposal more frequent for the largest ballot number for a
     * fast Paxos ballot number, or the proposal set in the constructor if all
     * last votes received were <code>null</code>.
     * <p>
     *
     * @return the winning proposal.
     */
    public Proposal getWinningProposal() {
        return winningProposal;
    }

}
