package br.unicamp.ic.treplica.transport;

/**
 * MessageSender Decorator.
 */
public abstract class MessageSenderDecorator implements IMessageSender {

    protected IMessageSender decoratedMessageSender;

    /**
     * Constructor.
     *
     * @param decoratedMessageSender
     *            decorated message sender.
     */
    public MessageSenderDecorator(final IMessageSender decoratedMessageSender) {
        this.decoratedMessageSender = decoratedMessageSender;
    }
}
