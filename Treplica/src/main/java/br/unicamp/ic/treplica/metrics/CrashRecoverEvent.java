package br.unicamp.ic.treplica.metrics;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CrashRecoverEvent {
    private String eventDate;
    private boolean isProcessRunning;
    private int processId;
    private long eventTime;
}
