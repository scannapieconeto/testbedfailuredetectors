/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

/**
 * This class implements a Treplica I/O exception. This exception is thrown when
 * an I/O error occurs.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TreplicaIOException extends TreplicaException {
    private static final long serialVersionUID = -7424122581745631953L;

    /**
     * Constructs a new <code>TreplicaIOException</code> object with
     * <code>null</code> as its detail message.
     * <p>
     */
    public TreplicaIOException() {
        super();
    }

    /**
     * Constructs a new <code>TreplicaIOException</code> object with the
     * specified detail message.
     * <p>
     *
     * @param message
     *            the detail message.
     */
    public TreplicaIOException(final String message) {
        super(message);
    }

    /**
     * Constructs a new <code>TreplicaIOException</code> object with the
     * specified detail message and cause.
     * <p>
     *
     * @param message
     *            the detail message.
     * @param cause
     *            the cause.
     */
    public TreplicaIOException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new <code>TreplicaIOException</code> object with the
     * specified cause and a detail message of <code>cause.toString()</code>.
     * <p>
     *
     * @param cause
     *            the cause.
     */
    public TreplicaIOException(final Throwable cause) {
        super(cause);
    }
}
