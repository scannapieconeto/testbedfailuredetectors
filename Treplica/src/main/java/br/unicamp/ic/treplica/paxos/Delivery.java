/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.Arrays;

/**
 * This class holds a message to be delivered to the application.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Delivery {

    private long decree;
    private byte[] message;

    /**
     * Creates a new message delivery.
     * <p>
     *
     * @param decree
     *            the decree which generated this delivery.
     * @param message
     *            the message.
     */
    public Delivery(final long decree, final byte[] message) {
        this.decree = decree;
        this.message = message;
    }

    /**
     * Returns the decree which generated this delivery.
     * <p>
     *
     * @return the decree which generated this delivery.
     */
    public long getDecree() {
        return decree;
    }

    /**
     * Returns the message of this delivery.
     * <p>
     *
     * @return the message of this delivery.
     */
    public byte[] getMessage() {
        return message;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Delivery) {
            Delivery delivery = (Delivery) o;
            return decree == delivery.decree && Arrays.equals(message, delivery.message);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (int) decree << 16 + Arrays.hashCode(message);
    }

}
