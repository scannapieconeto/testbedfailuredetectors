package br.unicamp.ic.treplica.election.chen;

import java.util.Comparator;

class ChenFDElement {

    /*
     * Constant used to help handling fail-recover scenarios. We're relying on
     * more than one message here (from 1 to 50) because UDP is the underlying
     * transport protocol and messages can be lost.
     */
    private static final long SEQUENCE_NUM_WARM_UP = 50;
    private final int processId;

    private volatile long localSequenceNumber;
    private volatile long currentLowerBoundInNanos;
    private volatile boolean isTrustworthy;

    ChenFDElement(final int processId, final long sequenceNumber, final long currentNanoTime) {
        this.processId = processId;

        initiateFDElement(sequenceNumber, currentNanoTime);
    }

    int getProcessId() {
        return this.processId;
    }

    boolean isTrustworthy() {
        return this.isTrustworthy;
    }

    void processMessage(final Long msgSequenceNumber, final long currentNanoTime, final long deltaInNanos) {
        if (msgSequenceNumber > this.localSequenceNumber) {
            // Compute lower bound.
            this.currentLowerBoundInNanos += (msgSequenceNumber - this.localSequenceNumber) * deltaInNanos;

            // Update local sequence number.
            this.localSequenceNumber = msgSequenceNumber;

            // Update trustworthy.
            updateTrustworthy(currentNanoTime, deltaInNanos);
        } else if (msgSequenceNumber <= SEQUENCE_NUM_WARM_UP && this.localSequenceNumber != 0) {
            // Condition used to handle fail-recover scenarios.
            // Has to be 1, so that the lower bound never gets higher that the
            // currentNanoTime.
            initiateFDElement(1, currentNanoTime);
        }
    }

    void updateTrustworthy(final long currentNanoTime, final long deltaInNanos) {
        // Check if message reception is inside the correct window.
        if (currentNanoTime - this.currentLowerBoundInNanos > deltaInNanos) {
            this.isTrustworthy = false;
        } else {
            this.isTrustworthy = true;
        }
    }

    private void initiateFDElement(final long sequenceNumber, final long currentNanoTime) {
        this.localSequenceNumber = sequenceNumber;
        this.currentLowerBoundInNanos = currentNanoTime;
        this.isTrustworthy = true;
    }

    /**
     * Inner class. IdComparator.
     */
    static class IdComparator implements Comparator<ChenFDElement> {

        @Override
        public int compare(final ChenFDElement element1, final ChenFDElement element2) {
            // Applying the ascending order, so that the process with lower
            // identifier is the first one.
            return Long.compare(element1.getProcessId(), element2.getProcessId());
        }
    }

}
