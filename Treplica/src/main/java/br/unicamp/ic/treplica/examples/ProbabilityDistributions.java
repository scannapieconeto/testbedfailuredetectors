package br.unicamp.ic.treplica.examples;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

public class ProbabilityDistributions {

	// Entry point
	public static void main(final String... args) {

		final int[] sampleSpace = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		final EnumeratedIntegerDistribution enumeratedDistribution = new EnumeratedIntegerDistribution(sampleSpace);

		final double mean = 136;
		final double sd = 20;
		final NormalDistribution normalDistribution = new NormalDistribution(mean, sd);

		while (true) {
			System.out.println(enumeratedDistribution.sample());
			System.out.println(normalDistribution.sample());
		}
	}
}
