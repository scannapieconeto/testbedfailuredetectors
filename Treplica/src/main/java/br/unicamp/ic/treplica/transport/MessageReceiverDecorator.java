package br.unicamp.ic.treplica.transport;

/**
 * MessageReceiver Decorator.
 */
public abstract class MessageReceiverDecorator implements IMessageReceiver {

    protected IMessageReceiver decoratedMessageReceiver;

    /**
     * Constructor.
     *
     * @param decoratedMessageReceiver
     *            decorated message receiver.
     */
    public MessageReceiverDecorator(final IMessageReceiver decoratedMessageReceiver) {
        this.decoratedMessageReceiver = decoratedMessageReceiver;
    }
}
