/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

import java.io.Serializable;

/**
 * This class implements a message of the state machine.
 * <p>
 */
public class SMMessage implements Serializable {
    private static final long serialVersionUID = -1713433435326657753L;

    private int id;
    private Action action;

    /**
     * Creates a new state machine message.
     * <p>
     *
     * @param id
     *            the id of this message.
     * @param action
     *            the action carried by this message.
     */
    public SMMessage(final int id, final Action action) {
        this.id = id;
        this.action = action;
    }

    /**
     * Returns the id of this message.
     * <p>
     *
     * @return the id of this message.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the action carried by this message.
     * <p>
     *
     * @return the action carried by this message.
     */
    public Action getAction() {
        return action;
    }
}
