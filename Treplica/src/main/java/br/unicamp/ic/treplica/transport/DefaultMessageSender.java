package br.unicamp.ic.treplica.transport;

import static br.unicamp.ic.treplica.transport.UDPTransport.HEADER_SIZE;
import static br.unicamp.ic.treplica.transport.UDPTransport.MAX_DATA_SIZE;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Random;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.common.Marshall;

public class DefaultMessageSender implements IMessageSender {

    /**
     * Method responsible for handling message send events.
     *
     * @param mIP
     * @param mPort
     * @param uSocket
     * @param message
     * @param destination
     * @throws TreplicaSerializationException
     * @throws TreplicaIOException
     */
    @Override
    public void sendMessage(final String mIP, final int mPort, final DatagramSocket uSocket, final Serializable message,
            final ITransportId destination) throws TreplicaSerializationException, TreplicaIOException {
        try {
            InetAddress address = InetAddress.getByName(mIP);
            int port = mPort;

            if (destination != null) {
                UDPTransportId destId = (UDPTransportId) destination;
                address = InetAddress.getByAddress(destId.getAddress());
                port = destId.getPort();
            }

            final byte[][] fragments = fragment(Marshall.marshall(message));
            for (int i = 0; i < fragments.length; ++i) {
                final DatagramPacket packet = new DatagramPacket(fragments[i], fragments[i].length, address, port);
                uSocket.send(packet);
            }
        } catch (IOException e) {
            throw new TreplicaIOException(e);
        }
    }

    @Override
    public void shutdown() {
    }

    /*
     * Private Methods.
     */

    /**
     * Fragments a byte array in chunks that fit an UDP packet, including enough
     * information for later reassembly.
     * <p>
     *
     * @param source
     *            the byte array to be fragmented.
     * @return an array of byte arrays, each containing a fragment.
     */
    private byte[][] fragment(final byte[] source) {
        byte fragNumber = (byte) ((source.length / (MAX_DATA_SIZE - HEADER_SIZE)) + 1);
        byte[][] fragments = new byte[fragNumber][];
        int offset = 0;
        int messageId = new Random().nextInt();
        for (byte fragment = 0; fragment < fragNumber; ++fragment) {
            final byte[] buffer = new byte[fragment < fragNumber - 1 ? MAX_DATA_SIZE
                    : (source.length % (MAX_DATA_SIZE - HEADER_SIZE)) + HEADER_SIZE];

            ByteBuffer.wrap(buffer).putInt(messageId).put(fragment).put(fragNumber);
            for (int i = HEADER_SIZE; i < buffer.length; ++i, ++offset) {
                buffer[i] = source[offset];
            }
            fragments[fragment] = buffer;
        }

        return fragments;
    }
}
