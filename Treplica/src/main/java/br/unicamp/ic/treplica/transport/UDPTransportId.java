/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.transport;

import java.util.Arrays;

/**
 * This class implements a transport id for the UDP transport.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class UDPTransportId implements ITransportId {
    private static final long serialVersionUID = -4450891110647090858L;

    private byte[] address;
    private int port;

    /**
     * Creates a new transport id with the provided ip address and port.
     * <p>
     *
     * @param address
     *            the raw Internet protocol address of the transport in network
     *            byte order (highest order byte at index 0).
     * @param port
     *            the port the transport listens to.
     */
    public UDPTransportId(final byte[] address, final int port) {
        this.address = address;
        this.port = port;
    }

    /**
     * Returns the raw Internet protocol address of the transport.
     * <p>
     *
     * @return the raw Internet protocol address of the transport in network
     *         byte order (highest order byte at index 0).
     */
    public byte[] getAddress() {
        return address;
    }

    /**
     * Returns the port the transport listens to.
     * <p>
     *
     * @return the port the transport listens to.
     */
    public int getPort() {
        return port;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof UDPTransportId) {
            UDPTransportId id = (UDPTransportId) o;
            return Arrays.equals(address, id.address) && port == id.port;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (Arrays.hashCode(address) << 16) + port;
    }

    /*
     * (non-Javadoc)
     *
     * @see Comparable#compareTo(Object)
     */
    @Override
    public int compareTo(final ITransportId o) {
        byte[] us = address;
        byte[] them = ((UDPTransportId) o).address;
        for (int i = 0; i < us.length; i++) {
            if (us[i] > them[i]) {
                return 1;
            } else if (us[i] < them[i]) {
                return -1;
            }
        }
        if (port > ((UDPTransportId) o).port) {
            return 1;
        } else if (port < ((UDPTransportId) o).port) {
            return -1;
        }
        return 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < address.length; i++) {
            int unsigned = address[i] & 0xFF;
            builder.append(unsigned);
            builder.append(".");
        }
        builder.setLength(builder.length() - 1);
        builder.append(":").append(port);
        return builder.toString();
    }

}
