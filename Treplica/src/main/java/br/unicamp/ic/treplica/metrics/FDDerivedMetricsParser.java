package br.unicamp.ic.treplica.metrics;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import br.unicamp.ic.treplica.utils.FileHandler;

/**
 * Arguments:
 *
 * args[0]: input directory. "{{ experiments_results_dir }}/{{ item }}/"
 * args[1]: output directory. "{{ experiments_results_dir }}/"
 * args[2]: failure detector algorithm. "{{ failure_detector_algorithm }}"
 * args[3]: iteration duration. "{{ iteration_duration }}"
 */
public class FDDerivedMetricsParser {

    // Config class.
    // protected static final OpponentConfig OPPONENT_CONFIG =
    // JarConfig.INST.getOpponent();

    // Constants.
    // private static final int DELAY_TO_STABILIZE_ELECTION =
    // OPPONENT_CONFIG.getDelayToStabilizeElection() * 1_000;
    private static final String FD_LOG_EXTENSION = ".fdLog";
    private static final String CRASH_RECOVER_EVENT_EXTENSION = ".crashRecoverEvent";
    private static final String TOKEN_SEPARATOR = " ";
    private static final int EXPECTED_LEADER = 14;
    private static final int EXPECTED_MIN_QUORUM = 3;
    private static final String T_TRANSITION = "T";
    private static final String S_TRANSITION = "S";

    private static String fdAvgMistakeRateFile;
    private static String fdTGSumFile;
    private static String fdTGFile;
    private static String fdTGBinsFile;
    // private static long initialExperimentTimeThreshold = 0;
    private static long finalExperimentTimeThreshold = 0;

    public static void main(final String... args) {
        // Get arguments.
        final String inputDirectory = args[0];
        final String outputDirectory = args[1];
        final String failureDetectorAlgorithm = args[2];
        final int iterationDuration = Integer.parseInt(args[3]);

        generateOutputfileName(failureDetectorAlgorithm);

        try {
            new FDDerivedMetricsParser().run(inputDirectory, outputDirectory, iterationDuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void run(final String inputDirectory, final String outputDirectory, final int iterationDuration)
            throws IOException {
        // If it's not a failure-free run skip this parser.
        if (!isFailureFreeRun(inputDirectory)) {
            return;
        }

        // Parse input.
        parseMetrics(inputDirectory, outputDirectory, iterationDuration);

        /*
        // PARSE LOCAL
        for (int i = 1; i <= 4; ++i) {
            final String id = inputDirectory + i;

            // If it's not a failure-free run skip this parser.
            if (!isFailureFreeRun(id)) {
                return;
            }

            // Parse input.
            parseMetrics(id, outputDirectory, iterationDuration);
        }
        */
    }

    private boolean isFailureFreeRun(final String inputDirectory) throws IOException {
        // Read files inside the directory.
        try (final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(inputDirectory))) {
            for (final Path path : directoryStream) {
                final String fileName = path.getFileName().toString();

                // If it's not a failure-free run skip this parser.
                if (fileName.endsWith(CRASH_RECOVER_EVENT_EXTENSION)) {
                    return false;
                }
            }
        }

        return true;
    }

    private void parseMetrics(final String inputDirectory, final String outputDirectory, final int iterationDuration)
            throws IOException {
        final Map<Long, FdLog> mergeSortEventsMap = new TreeMap<>();

        // Read files inside the directory.
        try (final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(inputDirectory))) {
            for (final Path path : directoryStream) {
                final String fileName = path.getFileName().toString();

                if (!fileName.endsWith(FD_LOG_EXTENSION)) {
                    continue;
                }

                final List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
                if (lines.isEmpty()) {
                    continue;
                }

                parseFdLog(lines, mergeSortEventsMap, iterationDuration);
            }
        }

        final List<String> transitions = fetchFDTransitions(mergeSortEventsMap);

        final double avgMistakeRate = calculateAvgMistakeRate(transitions, iterationDuration);
        FileHandler.writeFile(outputDirectory + "/" + FDDerivedMetricsParser.fdAvgMistakeRateFile, true,
                avgMistakeRate + " ");

        final long tgSum = calculateTGSum(transitions);
        FileHandler.writeFile(outputDirectory + "/" + FDDerivedMetricsParser.fdTGSumFile, true, tgSum + " ");

        calculateTG(transitions, outputDirectory);
        calculateTGBins(transitions, outputDirectory);
    }

    private double calculateAvgMistakeRate(final List<String> transitions, final int iterationDuration) {
        int sumSTransactions = 0;

        for (final String transition : transitions) {
            final String[] tokens = transition.split(TOKEN_SEPARATOR);
            if (tokens[tokens.length - 1].equals(S_TRANSITION)) {
                ++sumSTransactions;
            }
        }

        return sumSTransactions / (double) iterationDuration;
    }

    private long calculateTGSum(final List<String> transitions) {
        long tgSum = 0;
        long formerEventTimeMillis = 0;

        for (final String transition : transitions) {
            final String[] tokens = transition.split(TOKEN_SEPARATOR);
            final long eventTimeMillis = Long.parseLong(tokens[tokens.length - 2]);
            final String transitionType = tokens[tokens.length - 1];

            if (transitionType.equals(T_TRANSITION)) {
                formerEventTimeMillis = eventTimeMillis;
            } else if (transitionType.equals(S_TRANSITION)) {
                tgSum += (eventTimeMillis - formerEventTimeMillis);
                formerEventTimeMillis = 0;
            }
        }

        if (formerEventTimeMillis != 0) {
            tgSum += (finalExperimentTimeThreshold - formerEventTimeMillis);
        }

        return tgSum;
    }

    private void calculateTG(final List<String> transitions, final String outputDirectory) {
        final List<Long> tgs = new ArrayList<>();
        long formerEventTimeMillis = 0;

        for (final String transition : transitions) {
            final String[] tokens = transition.split(TOKEN_SEPARATOR);
            final long eventTimeMillis = Long.parseLong(tokens[tokens.length - 2]);
            final String transitionType = tokens[tokens.length - 1];

            if (transitionType.equals(T_TRANSITION)) {
                formerEventTimeMillis = eventTimeMillis;
            } else if (transitionType.equals(S_TRANSITION)) {
                if (formerEventTimeMillis != 0) {
                    tgs.add(eventTimeMillis - formerEventTimeMillis);
                }

                formerEventTimeMillis = 0;
            }
        }

        if (formerEventTimeMillis != 0) {
            tgs.add(finalExperimentTimeThreshold - formerEventTimeMillis);
        }

        for (final Long tg : tgs) {
            FileHandler.writeFile(outputDirectory + "/" + FDDerivedMetricsParser.fdTGFile, true, tg + "\n");
        }
    }

    private void calculateTGBins(final List<String> transitions, final String outputDirectory) {
        final List<String> tgBins = new ArrayList<>();
        long formerEventTimeMillis = 0;

        for (final String transition : transitions) {
            final String[] tokens = transition.split(TOKEN_SEPARATOR);
            final long eventTimeMillis = Long.parseLong(tokens[tokens.length - 2]);
            final String transitionType = tokens[tokens.length - 1];

            if (transitionType.equals(T_TRANSITION)) {
                formerEventTimeMillis = eventTimeMillis;
            } else if (transitionType.equals(S_TRANSITION)) {
                if (formerEventTimeMillis != 0) {
                    final long diff = eventTimeMillis - formerEventTimeMillis;
                    tgBins.add(getHistogramBin(diff));
                }

                formerEventTimeMillis = 0;
            }
        }

        if (formerEventTimeMillis != 0) {
            final long diff = finalExperimentTimeThreshold - formerEventTimeMillis;
            tgBins.add(getHistogramBin(diff));
        }

        for (final String bin : tgBins) {
            FileHandler.writeFile(outputDirectory + "/" + FDDerivedMetricsParser.fdTGBinsFile, true, bin + "\n");
        }
    }

    private String getHistogramBin(final long tg) {
        if (tg <= 75)
            return "75";
        if (tg <= 150)
            return "150";
        if (tg <= 225)
            return "225";
        if (tg <= 300)
            return "300";

        if (tg <= 375)
            return "375";
        if (tg <= 450)
            return "450";
        if (tg <= 525)
            return "525";
        if (tg <= 600)
            return "600";

        if (tg <= 675)
            return "675";
        if (tg <= 750)
            return "750";
        if (tg <= 825)
            return "825";
        if (tg <= 900)
            return "900";

        if (tg <= 975)
            return "975";
        if (tg <= 1050)
            return "1050";
        if (tg <= 1125)
            return "1125";
        if (tg <= 1200)
            return "1200";

        if (tg <= 1275)
            return "1275";
        if (tg <= 1350)
            return "1350";
        if (tg <= 1425)
            return "1425";
        if (tg <= 1500)
            return "1500";

        if (tg <= 1575)
            return "1575";
        if (tg <= 1650)
            return "1650";
        if (tg <= 1725)
            return "1725";
        if (tg <= 1800)
            return "1800";

        if (tg <= 1875)
            return "1875";
        if (tg <= 1950)
            return "1950";
        if (tg <= 2025)
            return "2025";
        if (tg <= 2100)
            return "2100";

        if (tg <= 2175)
            return "2175";
        if (tg <= 2250)
            return "2250";
        if (tg <= 2325)
            return "2325";
        if (tg <= 2400)
            return "2400";

        if (tg <= 2475)
            return "2475";
        if (tg <= 2550)
            return "2550";
        if (tg <= 2625)
            return "2625";
        if (tg <= 2700)
            return "2700";

        if (tg <= 2775)
            return "2775";
        if (tg <= 2850)
            return "2850";
        if (tg <= 2925)
            return "2925";
        if (tg <= 3000)
            return "3000";

        if (tg <= 3075)
            return "3075";
        if (tg <= 3150)
            return "3150";
        if (tg <= 3225)
            return "3225";
        if (tg <= 3300)
            return "3300";

        if (tg <= 3375)
            return "3375";
        if (tg <= 3450)
            return "3450";
        if (tg <= 3525)
            return "3525";
        if (tg <= 3600)
            return "3600";

        if (tg <= 3675)
            return "3675";
        if (tg <= 3750)
            return "3750";
        if (tg <= 3825)
            return "3825";
        if (tg <= 3900)
            return "3900";

        if (tg <= 3975)
            return "3975";
        if (tg <= 4050)
            return "4050";
        if (tg <= 4125)
            return "4125";
        if (tg <= 4200)
            return "4200";

        if (tg <= 4275)
            return "4275";
        if (tg <= 4350)
            return "4350";
        if (tg <= 4425)
            return "4425";
        if (tg <= 4500)
            return "4500";

        if (tg <= 4575)
            return "4575";
        if (tg <= 4650)
            return "4650";
        if (tg <= 4725)
            return "4725";
        if (tg <= 4800)
            return "4800";

        if (tg <= 4875)
            return "4875";
        if (tg <= 4950)
            return "4950";
        if (tg <= 5025)
            return "5025";
        if (tg <= 5100)
            return "5100";

        if (tg <= 5175)
            return "5175";
        if (tg <= 5250)
            return "5250";
        if (tg <= 5325)
            return "5325";
        if (tg <= 5400)
            return "5400";

        if (tg <= 5475)
            return "5475";
        if (tg <= 5550)
            return "5550";
        if (tg <= 5625)
            return "5625";
        if (tg <= 5700)
            return "5700";

        if (tg <= 5775)
            return "5775";
        if (tg <= 5850)
            return "5850";
        if (tg <= 5925)
            return "5925";
        if (tg <= 6000)
            return "6000";

        if (tg <= 6075)
            return "6075";
        if (tg <= 6150)
            return "6150";
        if (tg <= 6225)
            return "6225";
        if (tg <= 6300)
            return "6300";

        if (tg <= 6375)
            return "6375";
        if (tg <= 6450)
            return "6450";
        if (tg <= 6525)
            return "6525";
        if (tg <= 6600)
            return "6600";

        if (tg <= 6675)
            return "6675";
        if (tg <= 6750)
            return "6750";
        if (tg <= 6825)
            return "6825";
        if (tg <= 6900)
            return "6900";

        if (tg <= 6975)
            return "6975";
        if (tg <= 7050)
            return "7050";
        if (tg <= 7125)
            return "7125";
        if (tg <= 7200)
            return "7200";

        if (tg <= 7275)
            return "7275";
        if (tg <= 7350)
            return "7350";
        if (tg <= 7425)
            return "7425";
        if (tg <= 7500)
            return "7500";

        if (tg <= 7575)
            return "7575";
        if (tg <= 7650)
            return "7650";
        if (tg <= 7725)
            return "7725";
        if (tg <= 7800)
            return "7800";

        if (tg <= 7875)
            return "7875";
        if (tg <= 7950)
            return "7950";
        if (tg <= 8025)
            return "8025";
        if (tg <= 8100)
            return "8100";

        if (tg <= 8175)
            return "8175";
        if (tg <= 8250)
            return "8250";
        if (tg <= 8325)
            return "8325";
        if (tg <= 8400)
            return "8400";

        if (tg <= 8475)
            return "8475";
        if (tg <= 8550)
            return "8550";
        if (tg <= 8625)
            return "8625";
        if (tg <= 8700)
            return "8700";

        if (tg <= 8775)
            return "8775";
        if (tg <= 8850)
            return "8850";
        if (tg <= 8925)
            return "8925";
        if (tg <= 9000)
            return "9000";

        if (tg <= 9075)
            return "9075";
        if (tg <= 9150)
            return "9150";
        if (tg <= 9225)
            return "9225";
        if (tg <= 9300)
            return "9300";

        if (tg <= 9375)
            return "9375";
        if (tg <= 9450)
            return "9450";
        if (tg <= 9525)
            return "9525";
        if (tg <= 9600)
            return "9600";

        if (tg <= 9675)
            return "9675";
        if (tg <= 9750)
            return "9750";
        if (tg <= 9825)
            return "9825";
        if (tg <= 9900)
            return "9900";
        if (tg <= 9975)
            return "9975";

        if (tg <= 10150)
            return "10150";

        if (tg <= 100000)
            return "100000";

        if (tg <= 200000)
            return "200000";

        if (tg <= 300000)
            return "300000";

        if (tg <= 400000)
            return "400000";

        if (tg <= 500000)
            return "500000";

        if (tg <= 600000)
            return "600000";

        return "infinity";
    }

    /**
     * Expected file content: Fri Jul 24 02:39:49 BRT 2020 1595569189915 14 14
     * 15.
     */
    private void parseFdLog(final List<String> lines, final Map<Long, FdLog> mergeSortEventsMap,
            final int iterationDuration) {
        boolean isFirstLine = true;

        for (final String line : lines) {
            final String[] tokens = line.split(TOKEN_SEPARATOR);

            final long eventTimeMillis = Long.parseLong(tokens[6]);

            if (isFirstLine) {
                isFirstLine = false;
                // initialExperimentTimeThreshold = eventTimeMillis +
                // DELAY_TO_STABILIZE_ELECTION;
                finalExperimentTimeThreshold = eventTimeMillis + (iterationDuration * 1_000);
            }

            if (eventTimeMillis > finalExperimentTimeThreshold) {
                continue;
            }

            mergeSortEventsMap.put(eventTimeMillis, FdLog.builder()
                    .eventDate(tokens[0] + TOKEN_SEPARATOR + tokens[1] + TOKEN_SEPARATOR + tokens[2] + TOKEN_SEPARATOR
                            + tokens[3] + TOKEN_SEPARATOR + tokens[4] + TOKEN_SEPARATOR + tokens[5])
                    .eventTimeMillis(eventTimeMillis).processId(Integer.parseInt(tokens[7]))
                    .formerLeaderId(Integer.parseInt(tokens[8])).currentLeaderId(Integer.parseInt(tokens[9])).build());
        }
    }

    private List<String> fetchFDTransitions(final Map<Long, FdLog> mergeSortEventsMap) {
        final Set<Integer> quorumSet = new HashSet<>();
        final List<String> transitions = new ArrayList<>();

        for (final Entry<Long, FdLog> entry : mergeSortEventsMap.entrySet()) {
            final String eventDate = entry.getValue().getEventDate();
            final long eventTimeMillis = entry.getValue().getEventTimeMillis();
            final int processId = entry.getValue().getProcessId();
            final int currentLeader = entry.getValue().getCurrentLeaderId();

            // Update quorum.
            final int formerQuorumSize = quorumSet.size();
            if (currentLeader == EXPECTED_LEADER) {
                quorumSet.add(processId);
            } else {
                quorumSet.remove(processId);
            }
            final int currentQuorumSize = quorumSet.size();

            // T-transition (from 2 to 3 processes)
            if (formerQuorumSize == (EXPECTED_MIN_QUORUM - 1) && currentQuorumSize == EXPECTED_MIN_QUORUM) {
                transitions.add(String.format("%s %s %s", eventDate, eventTimeMillis, T_TRANSITION));
            }

            // S-transition (from 3 to 2 processes)
            if (formerQuorumSize == EXPECTED_MIN_QUORUM && currentQuorumSize == (EXPECTED_MIN_QUORUM - 1)) {
                transitions.add(String.format("%s %s %s", eventDate, eventTimeMillis, S_TRANSITION));
            }
        }

        return transitions;
    }

    private static void generateOutputfileName(final String failureDetectorAlgorithm) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String currDate = dateFormat.format(Calendar.getInstance().getTime());

        final String resultFilePrefix = currDate + "_" + failureDetectorAlgorithm;
        FDDerivedMetricsParser.fdAvgMistakeRateFile = resultFilePrefix + "_FD_AVG_MISTAKE_RATE.result";
        FDDerivedMetricsParser.fdTGSumFile = resultFilePrefix + "_FD_TG_SUM.result";
        FDDerivedMetricsParser.fdTGFile = resultFilePrefix + "_FD_TG.result";
        FDDerivedMetricsParser.fdTGBinsFile = resultFilePrefix + "_FD_TG_BINS.result";
    }
}
