package br.unicamp.ic.treplica.metrics;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.unicamp.ic.treplica.utils.FileHandler;

/**
 * Arguments:
 *
 * args[0]: input directory. "{{ experiments_results_dir }}/{{ item }}/"
 * args[1]: output directory. "{{ experiments_results_dir }}/"
 * args[2]: failure detector algorithm. "{{ failure_detector_algorithm }}"
 */
public class MapToCSV {

    private static final String LG_LOG_EXTENSION = ".treplica.output.content.log";
    private static final String TOKEN_SEPARATOR = " ";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
    private static String lgContentResultFile;

    public static void main(final String... args) {
        // Get arguments.
        final String inputDirectory = args[0];
        final String outputDirectory = args[1];
        final String failureDetectorAlgorithm = args[2];

        generateOutputfileName(failureDetectorAlgorithm);

        try {
            new MapToCSV().run(inputDirectory, outputDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void run(final String inputDirectory, final String outputDirectory) throws IOException {
        // Parse input.
        parseMetrics(inputDirectory, outputDirectory);
    }

    private void parseMetrics(final String inputDirectory, final String outputDirectory) throws IOException {
        // Read files inside the directory.
        try (final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(inputDirectory))) {
            for (final Path path : directoryStream) {
                final String fileName = path.getFileName().toString();

                if (!fileName.endsWith(LG_LOG_EXTENSION)) {
                    continue;
                }

                final List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
                if (lines.isEmpty()) {
                    continue;
                }

                String csvLine = "key,value";
                FileHandler.writeFile(outputDirectory + "/" + MapToCSV.lgContentResultFile, true, csvLine + "\n");

                for (final String line : lines) {
                    final String[] tokens = line.split(TOKEN_SEPARATOR);
                    csvLine = tokens[0] + ",";

                    final long millis = Long.parseLong(tokens[1]);
                    csvLine += DATE_FORMAT.format(new Date(millis));

                    FileHandler.writeFile(outputDirectory + "/" + MapToCSV.lgContentResultFile, true, csvLine + "\n");
                }

                // parse the content of only one replica.
                break;
            }
        }
    }

    private static void generateOutputfileName(final String failureDetectorAlgorithm) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String currDate = dateFormat.format(Calendar.getInstance().getTime());

        final String resultFilePrefix = currDate + "_" + failureDetectorAlgorithm;
        MapToCSV.lgContentResultFile = resultFilePrefix + "_LG_MAP_CONTENT.csv";
    }
}
