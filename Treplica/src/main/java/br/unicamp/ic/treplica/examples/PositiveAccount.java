/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.examples;

import java.io.Serializable;

import br.unicamp.ic.treplica.Action;

public class PositiveAccount implements Serializable {
    private static final long serialVersionUID = 1855888504826200733L;

    private int balance;

    public PositiveAccount() {
        balance = 0;
    }

    public int getBalance() {
        return balance;
    }

    public void deposit(final int amount) throws NoYouDontException {
        if (amount >= 0) {
            balance += amount;
            System.out.println("Deposited: " + amount + " Balance: " + balance);
            synchronized (this) {
                notifyAll();
            }
        } else {
            throw new NoYouDontException("Failed deposit: " + amount);
        }
    }

    public void withdraw(final int amount) throws NoYouDontException {
        if (amount >= 0 && balance - amount >= 0) {
            balance -= amount;
            System.out.println("Withdrawn: " + amount + " Balance: " + balance);
        } else {
            throw new NoYouDontException("Failed withdraw: " + amount);
        }
    }

    public static class NoYouDontException extends Exception {
        private static final long serialVersionUID = -7981519790371392230L;

        public NoYouDontException(final String description) {
            super(description);
        }
    }

    protected abstract static class AccountAction implements Action, Serializable {
        private static final long serialVersionUID = -2236519396850933918L;

        protected int amount;

        public AccountAction(final int amount) {
            this.amount = amount;
        }

        @Override
        public Object executeOn(final Object root) throws NoYouDontException {
            return executeOnAccount((PositiveAccount) root);
        }

        protected abstract Object executeOnAccount(PositiveAccount pacc) throws NoYouDontException;
    }

    public static class DepositAction extends AccountAction {
        private static final long serialVersionUID = -1043125873647840011L;

        public DepositAction(final int amount) {
            super(amount);
        }

        @Override
        protected Object executeOnAccount(final PositiveAccount pacc) throws NoYouDontException {
            pacc.deposit(amount);
            return null;
        }
    }

    public static class WithdrawAction extends AccountAction {
        private static final long serialVersionUID = -1457861497463047072L;

        public WithdrawAction(final int amount) {
            super(amount);
        }

        @Override
        protected Object executeOnAccount(final PositiveAccount pacc) throws NoYouDontException {
            pacc.withdraw(amount);
            return null;
        }
    }
}
