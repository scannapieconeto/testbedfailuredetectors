/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import br.unicamp.ic.treplica.transport.ITransportId;

/**
 * This class implements the acceptor abstraction of Paxos. The acceptor takes
 * part of ballots, casting votes according to its ledger.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Acceptor {

    private Secretary secretary;

    /**
     * Creates an acceptor that accesses the ledger and sends messages through
     * the provided secretary.
     * <p>
     *
     * @param secretary
     *            the secretary that handles I/O.
     */
    public Acceptor(final Secretary secretary) {
        this.secretary = secretary;
    }

    /**
     * Processes an acceptor message received by the protocol.
     * <p>
     *
     * @param message
     *            the message received.
     * @param sender
     *            the message sender.
     */
    public void processMessage(final PaxosMessage message, final ITransportId sender) {
        switch (message.getType()) {
            case PaxosMessage.NEXT_BALLOT:
                processNextBallot(message, sender);
                break;
            case PaxosMessage.NEXT_BALLOT_INACTIVE:
                processNextBallotInactive(message, sender);
                break;
            case PaxosMessage.BEGIN_BALLOT:
                processBeginBallot(message, sender);
                break;
        }
    }

    /**
     * Processes a <code>PaxosMessage.NEXT_BALLOT</code> message received by the
     * acceptor. Messages referring to a smaller ballot number than the current
     * next ballot number are ignored if created by the same process, if not, a
     * <code>PaxosMessage.LARGER_BALLOT</code> is created.
     * <p>
     *
     * @param message
     *            the message received.
     * @param sender
     *            the message sender.
     */
    private void processNextBallot(final PaxosMessage message, final ITransportId sender) {
        Ledger ledger = secretary.getLedger();
        long decree = message.getDecreeNumber();
        BallotNumber ballot = message.getBallotNumber();
        PaxosMessage reply = null;
        BallotNumber nextBallot = ledger.getNextBallot(decree);
        if (ledger.isDecided(decree)) {
            reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.SUCCESS, decree, null, null,
                    ledger.read(decree));
        } else if (nextBallot == null || nextBallot.compareTo(ballot) < 0) {
            ledger.setNextBallot(decree, ballot);
            reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.LAST_VOTE, decree, ballot,
                    ledger.getPrevVote(decree), null);
        } else if (nextBallot.getPaxosId() != ballot.getPaxosId()) {
            reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.LARGER_BALLOT, decree, nextBallot, null,
                    null);
        }
        if (reply != null) {
            secretary.queueMessage(reply, sender);
        }
    }

    /**
     * Processes a <code>PaxosMessage.NEXT_BALLOT_INACTIVE</code> message
     * received by the acceptor. Messages referring to a smaller ballot number
     * than the current inactive next ballot number are ignored if created by
     * the same process, if not, a <code>PaxosMessage.LARGER_BALLOT</code> is
     * sent.
     * <p>
     *
     * @param message
     *            the message received.
     * @param sender
     *            the message sender.
     */
    private void processNextBallotInactive(final PaxosMessage message, final ITransportId sender) {
        Ledger ledger = secretary.getLedger();
        BallotNumber nextBallotInactive = ledger.getInactiveNextBallot();
        if (nextBallotInactive == null || nextBallotInactive.compareTo(message.getBallotNumber()) < 0) {
            ledger.setInactiveNextBallot(message.getBallotNumber());
            PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.LAST_VOTE_INACTIVE,
                    ledger.lastActiveDecree() + 1, message.getBallotNumber(), null, null);
            secretary.queueMessage(reply, sender);
        } else if (nextBallotInactive.getPaxosId() != message.getBallotNumber().getPaxosId()) {
            PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.LARGER_BALLOT,
                    Ledger.NULL_COUNTER, nextBallotInactive, null, null);
            secretary.queueMessage(reply, sender);
        }
    }

    /**
     * Processes a <code>PaxosMessage.BEGIN_BALLOT</code> message received by
     * the acceptor. Messages referring to the wrong ballot number are ignored.
     * <p>
     *
     * @param message
     *            the message received.
     * @param sender
     *            the message sender.
     */
    private void processBeginBallot(final PaxosMessage message, final ITransportId sender) {
        Ledger ledger = secretary.getLedger();
        long decreeNumber = message.getDecreeNumber();
        BallotNumber ballot = message.getBallotNumber();
        BallotNumber nextBallot = ledger.getNextBallot(decreeNumber);
        Vote prevVote = ledger.getPrevVote(decreeNumber);
        if ((nextBallot == null || !(nextBallot.compareTo(ballot) > 0))
                && (prevVote == null || prevVote.getBallotNumber().compareTo(ballot) < 0)) {
            Vote vote = new Vote(ballot, message.getProposal());
            if (nextBallot == null || nextBallot.compareTo(ballot) < 0) {
                ledger.setNextBallot(decreeNumber, ballot);
            }
            ledger.setPrevVote(decreeNumber, vote);
            Proposal proposalReference = message.getProposal();
            if (proposalReference != null) {
                proposalReference = new Proposal(proposalReference.getId(), null);
            }
            PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.VOTED, decreeNumber, ballot,
                    null, proposalReference);
            secretary.queueMessage(reply);
        } else if (nextBallot != null && nextBallot.compareTo(ballot) > 0 && !ballot.isFast()) {
            PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.LARGER_BALLOT, decreeNumber,
                    nextBallot, null, null);
            secretary.queueMessage(reply, sender);
        }
    }

}
