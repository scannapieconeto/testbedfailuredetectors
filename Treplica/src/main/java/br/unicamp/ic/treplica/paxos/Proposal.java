/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.io.Serializable;

/**
 * This class represents a proposal for some decree.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Proposal implements Serializable {
    private static final long serialVersionUID = -5327313710584384472L;

    private int id;
    private byte[][] messages;

    /**
     * Creates a new proposal. A proposal is uniquely identified by an integer.
     * A proposal carries a list of application level messages that were locally
     * ordered in the order they appear in the list.
     * <p>
     *
     * @param id
     *            the id of the proposal.
     * @param messages
     *            the messages of this proposal.
     */
    public Proposal(final int id, final byte[][] messages) {
        this.id = id;
        this.messages = messages;
    }

    /**
     * Returns the id of the proposal.
     * <p>
     *
     * @return the id of the proposal.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the messages of this proposal.
     * <p>
     *
     * @return the messages of this proposal.
     */
    public byte[][] getMessages() {
        return messages;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Proposal) {
            Proposal proposal = (Proposal) o;
            return id == proposal.id;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return id;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return Integer.toHexString(hashCode());
    }

}
