/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class specializes <code>LinkedHashMap</code> in such way that it has a
 * fixed maximum capacity. This maximum is enforced by removing the least
 * accessed elements as new elements are added.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 *
 * @param <K>
 *            the type of keys maintained by this map
 * @param <V>
 *            the type of mapped values
 */
public class LRUHashMap<K, V> extends LinkedHashMap<K, V> {
    private static final long serialVersionUID = -6080118320861716818L;

    private int maxSize;

    public LRUHashMap(final int maxSize) {
        super(maxSize * 2, (float) 0.75, true);
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return size() > maxSize;
    }
}
