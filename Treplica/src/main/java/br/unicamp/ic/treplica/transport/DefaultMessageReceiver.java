package br.unicamp.ic.treplica.transport;

import java.util.LinkedList;

public class DefaultMessageReceiver implements IMessageReceiver {

    /**
     * Method responsible for handling message receive events.
     *
     * @param messageQueue
     * @param message
     */
    @Override
    public void receiveMessage(final LinkedList<UDPMessage> messageQueue, final UDPMessage message) {
        synchronized (messageQueue) {
            messageQueue.addLast(message);
            messageQueue.notify();
        }
    }

    @Override
    public void shutdown() {
    }
}
