package br.unicamp.ic.treplica.transport.opponent.strategy;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;

import br.unicamp.ic.treplica.transport.opponent.OpponentCommand;
import br.unicamp.ic.treplica.utils.Replica;
import lombok.extern.slf4j.Slf4j;

/*
 * Implementation of the normal (gaussian) distribution.
 * mean - Mean for this distribution.
 * sd - Standard deviation for this distribution.
 */
@Slf4j
public class NormalDistributionNetworkStrategy extends OpponentStrategy {

	// Config.
    private static final boolean ONLY_LEADER_FAILS = OPPONENT_CONFIG.isNetworkFailureLeaderOnly();
    private static final int MEAN = OPPONENT_CONFIG.getNetworkStrategyMean();
    private static final int STANDARD_DEVIATION = OPPONENT_CONFIG.getNetworkStrategyStandardDeviation();
    private static final int[] SAMPLE_SPACE = OPPONENT_CONFIG.getNetworkStrategySampleSpace();

    private final NormalDistribution normalDistribution;
    private final EnumeratedIntegerDistribution lossEnumeratedIntegerDistribution;

    private volatile boolean wasStableLeader = false;
    private volatile boolean warmupDone = false;

    public NormalDistributionNetworkStrategy() {
    	this.normalDistribution = new NormalDistribution(MEAN, STANDARD_DEVIATION);

        this.lossEnumeratedIntegerDistribution = new EnumeratedIntegerDistribution(SAMPLE_SPACE);
        log.debug("Built NormalDistributionNetworkStrategy");
    }

    @Override
    public void setupStrategy() {
        super.setupStrategy();
        log.debug("Setup NormalDistributionNetworkStrategy");
    }

    @Override
    public OpponentCommand getOpponentCommand() {
        if (!warmupDone || (ONLY_LEADER_FAILS && !wasStableLeader)) {
            return OpponentCommand.NO_FAILURE;
        }

        // Negative discrete values means drop.
        if (this.lossEnumeratedIntegerDistribution.sample() < 0) {
            return OpponentCommand.DROP;
        }

        final int sample = (int) this.normalDistribution.sample();
        return OpponentCommand.builder().delay(sample).drop(false).build();
    }

    @Override
    public void runOpponentStrategy() throws InterruptedException {
        warmupDone = true;
        wasStableLeader = Replica.getInstance().isLeader();
    }

}
