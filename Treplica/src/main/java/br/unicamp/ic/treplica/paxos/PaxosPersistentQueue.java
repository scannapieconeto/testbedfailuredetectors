/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright �� 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

import org.slf4j.Logger;

import br.unicamp.ic.treplica.PersistentQueue;
import br.unicamp.ic.treplica.StateManager;
import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.common.ChangeLog;
import br.unicamp.ic.treplica.common.Marshall;
import br.unicamp.ic.treplica.election.Election;
import br.unicamp.ic.treplica.election.Election.FDAlgorithm;
import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;
import br.unicamp.ic.treplica.transport.ITransportId;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.Replica;

/**
 * This class implements a persistent queue using the Paxos protocol.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class PaxosPersistentQueue implements PersistentQueue, ElectionClient {

    private static final JarConfig.FailureDetectorConfig FD_CONFIG = JarConfig.INST.getFailureDetector();

    protected Logger logger;
    protected int roundtrip;
    private int maxProcesses;
    private boolean fast;
    protected ITransport transport;
    private StateManager applicationState;
    private LinkedList<Delivery> messageQueue;
    protected Secretary secretary;
    protected Election election;
    protected Coordinator coordinator;
    protected Acceptor acceptor;
    private long nextDecree;
    protected Learner learner;
    private Thread mainLoop;
    private int replicaId;

    /**
     * Creates a new Paxos persistent queue.
     * <p>
     *
     * @param roundtrip
     *            the expected time for a response to a message to be received,
     *            including processing, in milliseconds.
     * @param maxProcesses
     *            the maximum number of processes bound to this queue.
     * @param fast
     *            if this queue will use fast Paxos ballots.
     * @param transport
     *            the transport this queue will use to send messages.
     * @param log
     *            the change log this queue will use for persistence.
     */
    public PaxosPersistentQueue(final int roundtrip, final int maxProcesses, final boolean fast,
            final ITransport transport, final ChangeLog log) {
        this.roundtrip = roundtrip;
        this.maxProcesses = maxProcesses;
        this.fast = fast;
        this.transport = transport;
        this.applicationState = null;
        this.messageQueue = new LinkedList<Delivery>();
        this.secretary = new Secretary(log, transport, messageQueue);
        this.election = null;
        this.coordinator = null;
        this.acceptor = null;
        this.nextDecree = Ledger.FIRST_COUNTER;
        this.learner = null;
        this.mainLoop = null;
        this.replicaId = Replica.getInstance().getMyId();
    }

    /*
     * ElectionClient implementation.
     */

    @Override
    public int getPaxosId() {
        return this.replicaId;
    }

    @Override
    public void leaderChange(final int newLeader) {
        if (this.getPaxosId() == newLeader) {
            this.coordinator.activate();
        } else {
            this.coordinator.deactivate();
        }
    }

    /*
     * PersistentQueue implementation.
     */

    @Override
    public void bind(final StateManager manager) throws TreplicaIOException, TreplicaSerializationException {
        synchronized (messageQueue) {
            synchronized (secretary) {
                applicationState = manager;
                nextDecree = secretary.bind(new Random().nextInt(), applicationState);
                messageQueue.clear();
                int classicMajority = (maxProcesses / 2) + 1;
                int fastMajority = maxProcesses - (maxProcesses / 4);

                coordinator = new Coordinator(secretary, classicMajority, fastMajority, 2 * roundtrip, fast);

                acceptor = new Acceptor(secretary);

                learner = new Learner(secretary, maxProcesses, classicMajority, fastMajority, 2 * roundtrip,
                        nextDecree);

                this.election = getElection();

                if (mainLoop == null) {
                    mainLoop = new Thread(new MainLoop(), "Paxos Main Thread");
                    mainLoop.setDaemon(true);
                    mainLoop.start();
                }
            }
        }
    }

    @Override
    public void enqueue(final Serializable message) throws TreplicaIOException, TreplicaSerializationException {
        byte[] sMessage = Marshall.marshall(message);

        synchronized (secretary) {
            learner.order(sMessage);
            secretary.flush();
        }
    }

    @Override
    public Serializable dequeue() throws TreplicaIOException, TreplicaSerializationException {
        Delivery delivery;
        synchronized (messageQueue) {
            while (messageQueue.size() == 0) {
                try {
                    messageQueue.wait();
                } catch (InterruptedException e) {
                }
            }

            delivery = messageQueue.removeFirst();
        }
        nextDecree = delivery.getDecree() + 1;

        return Marshall.unmarshall(delivery.getMessage());
    }

    @Override
    public void checkpoint() {
        synchronized (messageQueue) {
            synchronized (secretary) {
                secretary.queueCheckpoint(applicationState.getState(), nextDecree);
                secretary.synchronousFlush();
            }
        }
    }

    @Override
    public void shutdown() {
        if (this.election != null) {
            // Notifies election of the shutdown process.
            this.election.shutdown();
        }

        if (this.transport != null) {
            this.transport.shutdown();
        }
    }

    /*
     * Private Methods.
     */

    /**
     * This class implements the main loop for this Paxos persistent queue. It
     * listens to messages, receives them, and forwards them to correct
     * component, while keeping track of time and sending clock ticks to some
     * components at a constant rate.
     * <p>
     */
    private class MainLoop implements Runnable {

        private int step;
        private long deadline;

        protected MainLoop() {
            step = roundtrip;
            deadline = System.currentTimeMillis() + step;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    long now = System.currentTimeMillis();
                    if (now < deadline) {
                        final IMessage message = transport.receiveMessage((int) (deadline - now));

                        if (message != null) {
                            processMessage(message);
                        }
                    } else {
                        processTick();
                        deadline += step;
                    }
                } catch (TreplicaException e) {
                    throw new RuntimeException("Exception in PaxosPersistentQueue main thread. " + "Thread stopped!",
                            e);
                }
            }
        }

        private void processMessage(final IMessage message) throws TreplicaIOException, TreplicaSerializationException {
            ITransportId sender = message.getSender();
            PaxosMessage payload = (PaxosMessage) message.getPayload();

            synchronized (secretary) {
                switch (payload.getType()) {
                    // case PaxosMessage.LEADER:
                    // election.processMessage(payload);
                    // break;
                    case PaxosMessage.PASS: // PPG BEGIN of consensus cycle
                    case PaxosMessage.LAST_VOTE:
                    case PaxosMessage.LAST_VOTE_INACTIVE:
                    case PaxosMessage.LARGER_BALLOT:
                        coordinator.processMessage(payload, sender);
                        break;
                    case PaxosMessage.NEXT_BALLOT:
                    case PaxosMessage.NEXT_BALLOT_INACTIVE:
                        acceptor.processMessage(payload, sender);
                        break;
                    case PaxosMessage.BEGIN_BALLOT:
                        learner.processMessage(payload);
                        acceptor.processMessage(payload, sender);
                        break;
                    case PaxosMessage.ANY:
                    case PaxosMessage.VOTED:
                    case PaxosMessage.SUCCESS: // PPG END of consensus cycle
                        learner.processMessage(payload);
                        break;
                }
                secretary.flush();
            }
        }

        private void processTick() throws TreplicaIOException, TreplicaSerializationException {
            synchronized (secretary) {
                coordinator.processTick();
                learner.processTick();
                secretary.flush();
            }
        }
    }

    private Election getElection() {
        final FDAlgorithm fdAlgorithm = FDAlgorithm.valueOf(FD_CONFIG.getAlgorithm());
        return new Election(fdAlgorithm, FD_CONFIG.getDelta(), this, this.maxProcesses);
    }
}
