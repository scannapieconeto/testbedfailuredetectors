/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.HashMap;
import java.util.HashSet;

/**
 * This class holds the volatile data about a ballot, from the point of view of
 * a learner. Specifically, it counts the VOTED messages received.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class LearnerBallot {

    private HashMap<Proposal, HashSet<Integer>> votes;
    private int voters;
    private Proposal winningProposal;
    private int winningQuorum;

    /**
     * Creates the volatile data about a ballot, from the point of view of a
     * learner.
     */
    public LearnerBallot() {
        votes = new HashMap<Proposal, HashSet<Integer>>();
        voters = 0;
        winningProposal = null;
        winningQuorum = 0;
    }

    /**
     * Registers a voted message. If the process has already sent a voted
     * message for this ballot, this method does nothing.
     * <p>
     *
     * @param process
     *            the process who voted.
     * @param proposal
     *            the proposal it voted for.
     */
    public void registerVoted(final int process, final Proposal proposal) {
        HashSet<Integer> quorum = votes.get(proposal);
        if (quorum == null) {
            quorum = new HashSet<Integer>();
            votes.put(proposal, quorum);
        }
        if (quorum.add(process)) {
            voters++;
        }
        if (quorum.size() > winningQuorum) {
            winningQuorum = quorum.size();
            winningProposal = proposal;
        }
    }

    /**
     * Returns the total number of votes registered for this ballot.
     * <p>
     *
     * @return the total number of votes registered for this ballot.
     */
    public int receivedVotes() {
        return voters;
    }

    /**
     * Returns the winning proposal. The winning proposal is the proposal that
     * received the larger number of votes. If two or more proposals received
     * the same number of votes this method may return deterministically any of
     * the proposals.
     * <p>
     *
     * @return the winning proposal.
     */
    public Proposal getWinningProposal() {
        return winningProposal;
    }

    /**
     * Returns the number of votes registered for the winning proposal.
     * <p>
     *
     * @return the number of votes registered for the winning proposal.
     */
    public int getWinningQuorum() {
        return winningQuorum;
    }

}
