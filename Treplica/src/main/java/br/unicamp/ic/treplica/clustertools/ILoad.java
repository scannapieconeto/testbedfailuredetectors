package br.unicamp.ic.treplica.clustertools;

import java.util.List;

public interface ILoad extends Runnable {

    /**
     * Get Log entries.
     * @return List of log entries.
     */
    List<String> getLogEntries();

    /**
     * Get Data structure content.
     * @return List of content.
     */
    List<String> getDataStructureContent();

    /**
     * Graceful shutdown.
     */
    void shutdown();
}
