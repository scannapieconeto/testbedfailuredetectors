package br.unicamp.ic.treplica.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.UnknownHostException;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;
import br.unicamp.ic.treplica.transport.TransportFactory;
import br.unicamp.ic.treplica.transport.TransportFactory.TransportFactoryCaller;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.Replica;

public final class OpponentTesting {

    private static final JarConfig.FailureDetectorConfig FD_CONFIG = JarConfig.INST.getFailureDetector();

    private static ITransport transport1;
    private static ITransport transport2;

    // Private constructor.
    private OpponentTesting() {
    }

    // Entry point
    public static void main(final String... args)
            throws UnknownHostException, TreplicaIOException, TreplicaSerializationException, InterruptedException {

        if (args.length != 1) {
            System.out.println("Usage: OpponentTesting <isLeader>");
            System.exit(0);
        }

        if (Boolean.parseBoolean(args[0])) {
            Replica.getInstance().setIsLeader();
            System.out.println("Is leader!");
        } else {
            Replica.getInstance().setIsNotLeader();
            System.out.println("Is not leader!");
        }

        transport1 = TransportFactory.getTransport(FD_CONFIG.getIp(), FD_CONFIG.getPort(),
                TransportFactoryCaller.FAILURE_DETECTOR);
        transport2 = TransportFactory.getTransport(TransportFactoryCaller.TREPLICA);

        // Reads terminal commands.
        terminalReader();
    }

    private static void terminalReader() throws TreplicaIOException, TreplicaSerializationException {
        final BufferedReader terminal = new BufferedReader(new InputStreamReader(System.in));
        long counter = 0;

        while (true) {
            System.out.print("> ");

            String command;
            try {
                command = terminal.readLine();
            } catch (IOException e) {
                continue;
            }

            if (command == null || command.length() == 0) {
                continue;
            }

            if ("t1s".equals(command)) {
                final String messageId = "1-" + counter++;
                transport1.sendMessage(new MyMessage(messageId));
                System.out.println("Message sent " + messageId);
            } else if ("t2s".equals(command)) {
                final String messageId = "2-" + counter++;
                transport2.sendMessage(new MyMessage(messageId));
                System.out.println("Message sent " + messageId);
            } else if ("t1r".equals(command)) {
                final IMessage message = transport1.receiveMessage();
                if (message != null) {
                    System.out.println((MyMessage) message.getPayload());
                } else {
                    System.out.println("Null Message!");
                }
            } else if ("t2r".equals(command)) {
                final IMessage message = transport2.receiveMessage();
                if (message != null) {
                    System.out.println((MyMessage) message.getPayload());
                } else {
                    System.out.println("Null Message!");
                }
            } else if ("q".equals(command)) {
                System.exit(0);
            } else {
                System.out.println("Type 'q' to exit!");
            }
        }
    }

    private static class MyMessage implements Serializable {

        private static final long serialVersionUID = -4975364741643456808L;
        private final String id;

        MyMessage(final String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "MyMessage [id=" + id + "]";
        }
    }
}
