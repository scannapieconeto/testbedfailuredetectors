package br.unicamp.ic.treplica.transport;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.transport.opponent.OpponentMessageReceiverDecorator;
import br.unicamp.ic.treplica.transport.opponent.OpponentMessageSenderDecorator;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.OpponentConfig;
import br.unicamp.ic.treplica.utils.JarConfig.TreplicaConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class TransportFactory {

    // Config.
    private static final OpponentConfig OPPONENT_CONFIG = JarConfig.INST.getOpponent();

    private static final TreplicaConfig TREPLICA_CONFIG = JarConfig.INST.getTreplica();
    private static final String DEFAULT_MULT_IP = TREPLICA_CONFIG.getIp();
    private static final int DEFAULT_MULT_PORT = TREPLICA_CONFIG.getPort();

    /*
     * Failure Detector Algorithms.
     */
    public enum TransportFactoryCaller {
        TREPLICA, FAILURE_DETECTOR
    }

    // Private constructor.
    private TransportFactory() {
    }

    /**
     * Build and retrieve a transport instance.
     *
     * @return
     * @throws TreplicaIOException
     */
    public static ITransport getTransport(final TransportFactoryCaller caller) throws TreplicaIOException {
        return getTransport(DEFAULT_MULT_IP, DEFAULT_MULT_PORT, caller);
    }

    /**
     * Build and retrieve a transport instance.
     *
     * @param address
     *            IP address.
     * @param port
     *            port.
     * @return
     * @throws TreplicaIOException
     */
    public static ITransport getTransport(final String address, final int port, final TransportFactoryCaller caller)
            throws TreplicaIOException {
        log.debug("Treplica opponent is enabled = " + OPPONENT_CONFIG.isTreplicaOpponentEnabled());
        log.debug("Failure Detector opponent is enabled = " + OPPONENT_CONFIG.isFdOpponentEnabled());

        final IMessageReceiver defaultMessageReceiver = new DefaultMessageReceiver();
        final IMessageSender defaultMessageSender = new DefaultMessageSender();

        if (caller.equals(TransportFactoryCaller.TREPLICA)) {
            return OPPONENT_CONFIG.isTreplicaOpponentEnabled()
                    ? new UDPTransport(address, port, new OpponentMessageReceiverDecorator(defaultMessageReceiver),
                            new OpponentMessageSenderDecorator(defaultMessageSender))
                    : new UDPTransport(address, port, defaultMessageReceiver, defaultMessageSender);
        } else {
            return OPPONENT_CONFIG.isFdOpponentEnabled()
                    ? new UDPTransport(address, port, new OpponentMessageReceiverDecorator(defaultMessageReceiver),
                            new OpponentMessageSenderDecorator(defaultMessageSender))
                    : new UDPTransport(address, port, defaultMessageReceiver, defaultMessageSender);
        }
    }
}
