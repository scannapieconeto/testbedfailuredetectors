/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.unicamp.ic.treplica.TreplicaIOException;

/**
 *
 * MemoryChangeLog implements the client side of the main-memory change log.
 * Changes are stored in the remote log server and can be used to recover the
 * state of the object upon recovery. Checkpointing is used to improve the
 * performance of reconstruction, and can also be used to reset the state of the
 * application.
 *
 * @author Luiz Eduardo Buzato
 */
public class MemoryChangeLog implements ChangeLog {

    private Logger logger;

    private static final long UNKNOWN_CHECKPOINT = -1;
    private static final long UNKNOWN_CHANGE_BLOCK = -1;
    private static final Serializable NULL_CHECKPOINT = "null checkpoint";

    private long lastCheckpoint;
    private long nextCheckpoint;
    private long nextChangeBlock;

    private int processId;

    private Socket theServer;
    private String serverIP;
    private int serverPort;

    private ObjectOutputStream toServer;
    private ObjectInputStream fromServer;

    /**
     *
     * @param stableMedia
     *            <ip>:<port> of the MemoryChangeLogServer
     */
    public MemoryChangeLog(final String stableMedia) {
        logger = LoggerFactory.getLogger(this.getClass());
        // Parse the IP and port of the server.
        try {
            StringTokenizer serverAddress = new StringTokenizer(stableMedia, ":");
            serverIP = new String(serverAddress.nextToken());
            serverPort = (Integer.parseInt(serverAddress.nextToken()));
        } catch (Exception e) {
            logger.error("Parameter persistor <ip><port> malformed.");
            logger.error("Message returned by exception: ", e);
        }
    }

    /**
     * openChangeLog
     *
     * @param client
     *            the ChangeLog client (Scribe).
     *
     */

    @Override
    public void open(final ChangeLogClient client) throws TreplicaIOException {

        // Recovery (discovery) must be handled here in the future.
        // Is the server there?
        try { /* create a client socket */
            this.theServer = new Socket(InetAddress.getByName(serverIP), serverPort);
            logger.debug("Connect to server at: {}:{}", serverIP, serverPort);
        } catch (IOException e) {
            logger.error("Socket connection failed.");
            logger.error("Message returned by exception: ", e);
        }

        /*
         * Establishes communication with the MainMemoryChangeLogServer located
         * at serverIP, serverPort
         */
        try {
            toServer = new ObjectOutputStream(theServer.getOutputStream());
            toServer.flush();
            fromServer = new ObjectInputStream(theServer.getInputStream());

            // begin: identification, action
            // begin: INIT
            toServer.writeInt(processId);
            toServer.flush();
            toServer.writeObject(ServerAction.INIT);
            toServer.flush();
        } catch (IOException e) {
            logger.error("serverOutStream or serverInStream setup failed.");
            logger.error("IOException returned: ", e);
        }

        int ackProcessId;

        try {
            ackProcessId = fromServer.readInt();

            if (processId != ackProcessId) {
                logger.error("Inconsistent ProcessId returned by server.");
                close();
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("readObject isInputShutdown: ", e0);
                close();
            } else {
                logger.error("readObject EOFException: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("readObject IOException: ", e2);
            close();
        }
        // end: identification, action
        // end: INIT

        nextCheckpoint = UNKNOWN_CHECKPOINT;
        nextChangeBlock = UNKNOWN_CHANGE_BLOCK;

        loadLastCheckpoint(client);
        logger.debug("[after INIT] nextCheckpoint= {}", nextCheckpoint);

        loadChangeBlocks(client);
        logger.debug("[after INIT] nextChangeBlock= {}", nextChangeBlock);

    }

    @Override
    public void close() {
        try {
            toServer.close();
            fromServer.close();
            theServer.close();
        } catch (IOException e) {
            logger.error("closeChangeLog close: ", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see ChangeLog#isOpen()
     */
    @Override
    public boolean isOpen() {
        return theServer != null && !theServer.isClosed();
    }

    private void loadLastCheckpoint(final ChangeLogClient client) {

        Serializable checkpoint = null;

        int ackProcessId;

        // begin identification, action
        try {
            toServer.writeInt(processId);
            toServer.flush();
            toServer.writeObject(ServerAction.LOAD_LAST_CHECKPOINT);
            toServer.flush();
        } catch (IOException e) {
            logger.error("IOException returned: ", e);
        }

        try {
            ackProcessId = fromServer.readInt();

            if (processId != ackProcessId) {
                logger.error("Inconsistent ProcessId returned by server.");
                close();
            }

        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }
        // end: identification, action

        // begin: LOAD_LAST_CHECKPOINT
        try {
            lastCheckpoint = fromServer.readLong();

        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }

        nextCheckpoint = lastCheckpoint + 1;
        nextChangeBlock = 0;

        try {
            checkpoint = (Serializable) fromServer.readObject();
            if (!checkpoint.equals(NULL_CHECKPOINT)) {
                client.processCheckpoint(checkpoint);
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (ClassNotFoundException e1) {
            logger.error("ClassNotFoundException returned: ", e1);
            close();
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }

        // end: LOAD_LAST_CHECKPOINT
    }

    private void loadChangeBlocks(final ChangeLogClient client) {

        long numberOfChangeBlocks = -1;
        int ackProcessId;
        Serializable change = null;

        // begin: identification, action
        try {
            toServer.writeInt(processId);
            toServer.flush();
            toServer.writeObject(ServerAction.LOAD_CHANGE_BLOCKS);
            toServer.flush();
        } catch (IOException e) {
            logger.error("IOException returned: ", e);
        }

        try {
            ackProcessId = fromServer.readInt();

            if (processId != ackProcessId) {
                logger.error("Inconsistent ProcessId returned by server.");
                close();
            }

        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }
        // end: identification, action

        // begin: LOAD_CHANGE_BLOCKS

        try {
            toServer.writeLong(nextCheckpoint);
            toServer.flush();
        } catch (IOException e) {
            logger.error("writeLong IOException ", e);
            close();
        }

        try {
            numberOfChangeBlocks = fromServer.readLong();
            if (numberOfChangeBlocks < 0) {
                logger.error("Server returned inconsistent numberOfChangeBlocks.");
                close();
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }

        logger.debug("LOAD_CHANGE_BLOCKS: readLong: numberOfChangeBlocks: {}", numberOfChangeBlocks);

        long numberOfReceivedBlocks = 0;
        while (numberOfReceivedBlocks < numberOfChangeBlocks) {

            try {
                change = (Serializable) fromServer.readObject();
            } catch (EOFException e0) {
                if (theServer.isInputShutdown()) {
                    logger.error("Server closed its output stream.");
                } else {
                    logger.error("EOFException returned: ", e0);
                }
                close();
                break;
            } catch (ClassNotFoundException e1) {
                logger.error("ClassNotFoundException returned: ", e1);
                close();
                break;
            } catch (IOException e2) {
                logger.error("IOException returned: ", e2);
                close();
                break;
            }

            if (change != null) {
                client.processChange(change);
            }

            numberOfReceivedBlocks++;
        } // end of while

        // end: LOAD_CHANGE_BLOCKS
    }

    @Override
    public void writeChange(final Serializable change) throws TreplicaIOException {
        int ackProcessId;
        long ackNextChangeBlock = -1;

        // begin: identification, action
        try {
            toServer.writeInt(processId);
            toServer.flush();
            toServer.writeObject(ServerAction.WRITE_CHANGE);
            toServer.flush();
        } catch (IOException e) {
            logger.error("writeObject IOException", e);
            close();
        }

        try {
            ackProcessId = fromServer.readInt();

            if (processId != ackProcessId) {
                logger.error("Inconsistent ProcessId returned by server.");
                close();
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }
        // end: identification, action

        // begin: WRITE_CHANGE

        try {
            toServer.writeLong(nextCheckpoint);
            toServer.flush();
            toServer.writeLong(nextChangeBlock);
            toServer.flush();
            toServer.writeObject(change);
            toServer.flush();
        } catch (IOException e) {
            logger.error("writeObject IOException", e);
            close();
        }

        try {
            ackNextChangeBlock = fromServer.readLong();
            if (ackNextChangeBlock != nextChangeBlock) {
                logger.error("Server returned inconsistent numberOfChangeBlocks.");
                close();
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }
        // end: WRITE_CHANGE
    }

    @Override
    public void writeCheckpoint(final Serializable checkpoint) throws TreplicaIOException {

        long ackNextCheckpoint = -1;
        int ackProcessId;

        // begin: identification, action
        try {
            toServer.writeInt(processId);
            toServer.flush();
            toServer.writeObject(ServerAction.WRITE_CHECKPOINT);
            toServer.flush();
        } catch (IOException e) {
            logger.error("writeObject IOException", e);
            close();
        }

        try {
            ackProcessId = fromServer.readInt();

            if (processId != ackProcessId) {
                logger.error("Inconsistent ProcessId returned by server.");
                close();
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }
        // end: identification, action

        // begin: WRITE_CHECKPOINT

        try {
            toServer.writeLong(nextCheckpoint);
            toServer.flush();
            toServer.writeObject(checkpoint);
            toServer.flush();
        } catch (IOException e) {
            logger.error("writeObject IOException", e);
            close();
        }

        try {
            ackNextCheckpoint = fromServer.readLong();
            if (ackNextCheckpoint != nextCheckpoint) {
                logger.error("Server returned inconsistent nextCheckpoint.");
                close();
            }
        } catch (EOFException e0) {
            if (theServer.isInputShutdown()) {
                logger.error("Server closed its output stream.");
                close();
            } else {
                logger.error("EOFException returned: ", e0);
                close();
            }
        } catch (IOException e2) {
            logger.error("IOException returned: ", e2);
            close();
        }

        nextCheckpoint++;
        nextChangeBlock = 0;

        // end: WRITE_CHECKPOINT

    }
}
