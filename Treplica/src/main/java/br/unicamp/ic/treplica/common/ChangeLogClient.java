/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.Serializable;

/**
 * This interface represents a client of a change log. The client is invoked
 * only on change log open, to allow object reconstruction.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface ChangeLogClient {

    /**
     * This method processes a checkpoint provided by the change log.
     * <p>
     *
     * @param checkpoint
     *            the checkpoint to be processed.
     */
    void processCheckpoint(Serializable checkpoint);

    /**
     * This method processes a change provided by the change log.
     * <p>
     *
     * @param change
     *            the change to be processed.
     */
    void processChange(Serializable change);
}
