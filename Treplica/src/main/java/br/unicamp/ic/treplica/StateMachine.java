/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright �� 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

import br.unicamp.ic.treplica.common.ChangeLog;
import br.unicamp.ic.treplica.common.DiskChangeLog;
import br.unicamp.ic.treplica.common.MemoryChangeLog;
import br.unicamp.ic.treplica.paxos.PaxosPersistentQueue;
import br.unicamp.ic.treplica.transport.TransportFactory;
import br.unicamp.ic.treplica.transport.TransportFactory.TransportFactoryCaller;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.TreplicaConfig;

/**
 * This class implements a state machine that is transparently replicated using
 * a totally ordered and persistent message queue primitive. This state machine
 * doesn't care about explicit transitions, it just executes actions on the
 * stored state. It is the responsibility of the client to implement, with the
 * appropriate set of actions, meaningful states and transitions.
 * <p>
 *
 * FIXME: The state management is probably thread unsafe, but we depend on the
 * behavior of the persistent queue with respect to the state manager to sort it
 * out.
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class StateMachine implements Runnable, StateManager {

    private static final TreplicaConfig TREPLICA_CONFIG = JarConfig.INST.getTreplica();

    private PersistentQueue queue;
    private Serializable state;
    private Random random;
    private HashMap<Integer, ExecutionMonitor> waiting;

    /**
     * Creates a new state machine. If there isn't any previous state available
     * in the provided queue, the provided state will be used. Otherwise, a
     * state retrieved from the queue will be used instead. So, it is important
     * to always access the state through the <code>getState()</code> method.
     *
     * @param state
     *            the initial state.
     * @param queue
     *            the persistent queue.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     * @see StateMachine#getState()
     */
    public StateMachine(final Serializable state, final PersistentQueue queue)
            throws TreplicaIOException, TreplicaSerializationException {
        this.queue = queue;
        this.state = state;
        this.queue.bind(this);
        random = new Random();
        waiting = new HashMap<Integer, ExecutionMonitor>();
        Thread receiver = new Thread(this, "StateMachine Receiver Thread");
        receiver.setDaemon(true);
        receiver.start();
    }

    /*
     * (non-Javadoc)
     *
     * @see StateManager#getState()
     */
    @Override
    public Serializable getState() {
        return state;
    }

    /*
     * (non-Javadoc)
     *
     * @see StateManager#setState(Serializable)
     */
    @Override
    public void setState(final Serializable state) {
        this.state = state;
    }

    @Override
    public void shutdown() {
        if (this.queue != null) {
            this.queue.shutdown();
        }
    }

    /**
     * Executes an action in the state machine.
     * <p>
     *
     * @param action
     *            the action to be executed.
     * @return the result of the execution of this action.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     * @throws Exception
     *             if an exception occurs in this action.
     */
    public Object execute(final Action action) throws Exception {
        int id = random.nextInt();
        ExecutionMonitor monitor = createMonitor(id);
        SMMessage message = new SMMessage(id, action);
        synchronized (monitor) {
            queue.enqueue(message);
            try {
                // Message sends are asynchronous, but commands are
                // synchronous. This thread will sleep now to be waken up
                // when the message is asynchronously delivered.
                monitor.wait();
            } catch (InterruptedException e) {
            }
        }
        if (monitor.exception != null) {
            throw monitor.exception;
        }
        return monitor.returnValue;
    }

    /**
     * Saves a checkpoint of the state to stable storage for faster recovery.
     * <p>
     *
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     */
    public void checkpoint() throws TreplicaIOException, TreplicaSerializationException {
        queue.checkpoint();
    }

    /**
     * This class needs a thread executing this method to listen for messages of
     * the broadcast primitive. This thread is created by the constructor, this
     * method should not be called from clients of this class.
     * <p>
     */
    @Override
    public void run() {
        try {
            while (true) {
                SMMessage message = (SMMessage) queue.dequeue();
                executeOrdered(message.getAction(), message.getId());
            }
        } catch (TreplicaException e) {
            throw new RuntimeException("Exception in message receiver thread. " + "Thread stopped!", e);
        }
    }

    /**
     * Executes an already ordered action in the local state.
     * <p>
     *
     * @param action
     *            the action to be executed.
     * @param messageId
     *            the id of the message carrying the action.
     */
    private void executeOrdered(final Action action, final int messageId) {
        Object returnValue = null;
        Exception exception = null;
        try {
            synchronized (state) {
                returnValue = action.executeOn(state);
            }
        } catch (Exception e) {
            exception = e;
        }
        ExecutionMonitor monitor = destroyMonitor(messageId);
        if (monitor != null) {
            synchronized (monitor) {
                monitor.returnValue = returnValue;
                monitor.exception = exception;
                monitor.notify();
            }
        }
    }

    /**
     * This class is used as a monitor to synchronize a thread while it waits
     * for the operation it started to execute, and also holds a possible return
     * value or exception generated by the execution.
     * <p>
     */
    protected static class ExecutionMonitor {
        private Object returnValue;
        private Exception exception;
    }

    /**
     * Creates and registers a monitor to aid the synchronization of the sending
     * and receiving threads of a message.
     * <p>
     *
     * @param messageId
     *            the id of the message.
     * @return the created monitor.
     */
    private ExecutionMonitor createMonitor(final int messageId) {
        ExecutionMonitor monitor = new ExecutionMonitor();
        synchronized (waiting) {
            waiting.put(messageId, monitor);
        }
        return monitor;
    }

    /**
     * Finds and de-registers a monitor to aid the synchronization of the
     * sending and receiving threads of a message.
     * <p>
     *
     * @param messageId
     *            the id of the message.
     * @return the previously registered monitor.
     */
    private ExecutionMonitor destroyMonitor(final int messageId) {
        ExecutionMonitor monitor = null;
        synchronized (waiting) {
            monitor = waiting.remove(messageId);
        }
        return monitor;
    }

    public static StateMachine createPaxosSM(final Serializable stateMachine, final int maxProcesses,
            final String stableMedia) throws TreplicaIOException, TreplicaSerializationException {

        final int roundtrip = TREPLICA_CONFIG.getRoundTrip();
        final boolean isFastPaxos = TREPLICA_CONFIG.getPaxosAlgorithm().equals("FAST_PAXOS");

        final ChangeLog log = (stableMedia.contains(":")) ? new MemoryChangeLog(stableMedia)
                : new DiskChangeLog(stableMedia);

        return new StateMachine(stateMachine, new PaxosPersistentQueue(roundtrip, maxProcesses, isFastPaxos,
                TransportFactory.getTransport(TransportFactoryCaller.TREPLICA), log));
    }
}
