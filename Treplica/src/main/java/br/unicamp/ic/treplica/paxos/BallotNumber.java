/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.io.Serializable;

/**
 * This class implements a Paxos ballot number.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class BallotNumber implements Serializable, Comparable<BallotNumber> {
    private static final long serialVersionUID = 538217829319657113L;

    private int paxosId;
    private long counter;
    private boolean fast;

    /**
     * Creates a new ballot number.
     * <p>
     *
     * @param paxosId
     *            the id of this Paxos instance.
     * @param counter
     *            the local counter.
     * @param fast
     *            <code>true</code> if it is a fast ballot number;
     *            <code>false</code> if it is a classic ballot number.
     */
    public BallotNumber(final int paxosId, final long counter, final boolean fast) {
        this.paxosId = paxosId;
        this.counter = counter;
        this.fast = fast;
    }

    /**
     * Creates a new classic ballot number.
     * <p>
     *
     * @param paxosId
     *            the id of this Paxos instance.
     * @param counter
     *            the local counter.
     */
    public BallotNumber(final int paxosId, final long counter) {
        this(paxosId, counter, false);
    }

    /**
     * Returns the Paxos id of the owner of this ballot number.
     * <p>
     *
     * @return the Paxos id of the owner of this ballot number.
     */
    public int getPaxosId() {
        return paxosId;
    }

    /**
     * Returns the local counter of this ballot number.
     * <p>
     *
     * @return the local counter of this ballot number.
     */
    public long getCounter() {
        return counter;
    }

    /**
     * Indicates if this ballot number is fast.
     * <p>
     *
     * @return <code>true</code> if it is a fast ballot number;
     *         <code>false</code> if it is a classic ballot number.
     */
    public boolean isFast() {
        return fast;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof BallotNumber) {
            BallotNumber ballot = (BallotNumber) o;
            return paxosId == ballot.paxosId && counter == ballot.counter && fast == ballot.fast;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (((paxosId << 16) + (int) counter) << 1) + (fast ? 1 : 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see Comparable#compareTo(Object)
     */
    @Override
    public int compareTo(final BallotNumber ballot) {
        if (counter > ballot.counter) {
            return 1;
        } else if (counter < ballot.counter) {
            return -1;
        }
        if (paxosId > ballot.paxosId) {
            return 1;
        } else if (paxosId < ballot.paxosId) {
            return -1;
        }
        if (!(fast ^ ballot.fast)) {
            return 0;
        }
        if (fast) {
            return -1;
        }
        return 1;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return Integer.toHexString(paxosId) + "|" + counter + "(" + (fast ? "f" : "c") + ")";
    }
}
