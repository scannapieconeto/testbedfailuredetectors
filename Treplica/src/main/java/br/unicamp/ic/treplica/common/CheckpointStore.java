/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 *
 * CheckpointStore is a very simple storage for checkpoints. It is a hash whose
 * key is the checkpoint identifier; an integer.
 *
 * @author Luiz Eduardo Buzato
 */
public class CheckpointStore {

    private HashMap<Long, Serializable> checkpoints = new HashMap<Long, Serializable>();

    public void clear() {
        checkpoints.clear();
    }

    public boolean isEmpty() {
        return checkpoints.isEmpty();
    }

    public long numberOfCheckpoints() {
        return checkpoints.size();
    }

    public boolean containCheckpoint(final long checkpointId) {
        return checkpoints.containsKey(checkpointId);
    }

    public Serializable readCheckpoint(final long checkpointId) {
        Serializable checkpoint = null;

        if (checkpoints.containsKey(checkpointId)) {
            checkpoint = checkpoints.get(checkpointId);
        }

        return checkpoint;
    }

    public void writeCheckpoint(final long checkpointId, final Serializable checkpoint) {
        checkpoints.put(checkpointId, checkpoint);
    }

    /*
     * A checkpointId can be associated to a null checkpoint and this case it is
     * considered as not present and an exception is raised.
     *
     */

    public Serializable deleteCheckpoint(final long checkpointId) throws NoSuchElementException {
        Serializable previousCheckpoint = null;

        previousCheckpoint = checkpoints.remove(checkpointId);

        if (previousCheckpoint == null) {
            throw new NoSuchElementException();
        }

        return previousCheckpoint;

    }

    /*
     * readMostRecentCheckpoint returns null if the checkpoint store is empty.
     *
     * readMostRecentCheckpoint goes through the hash every time it is invoked
     * to make sure that recent store changes are taken into account. It is not
     * atomic, not synchronized.
     *
     */

    public Serializable readMostRecentCheckpoint() {

        Serializable previousCheckpoint = null;

        Set<Long> checkpointIds = checkpoints.keySet();
        Iterator<Long> i = checkpointIds.iterator();

        long sentinelId = -1;

        while (i.hasNext()) {
            long checkpointId = i.next();
            if (checkpointId > sentinelId) {
                sentinelId = checkpointId;
            }
        }

        previousCheckpoint = checkpoints.get(sentinelId);

        return previousCheckpoint;

    }

    /*
     * returns -1 if the checkpoint store is empty.
     */

    public long mostRecentCheckpoint() {

        Set<Long> checkpointIds = checkpoints.keySet();
        Iterator<Long> i = checkpointIds.iterator();

        long sentinelId = -1;

        while (i.hasNext()) {
            long checkpointId = i.next();
            if (checkpointId > sentinelId) {
                sentinelId = checkpointId;
            }
        }

        return sentinelId;

    }

}
