package br.unicamp.ic.treplica.metrics;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import br.unicamp.ic.treplica.utils.FileHandler;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.OpponentConfig;

/**
 * Arguments:
 *
 * args[0]: input directory. "{{ experiments_results_dir }}/{{ item }}/"
 * args[1]: output directory. "{{ experiments_results_dir }}/"
 * args[2]: failoverleader. "15"
 * args[3]: failure detector algorithm. "{{ failure_detector_algorithm }}"
 * args[4]: iteration duration. "{{ iteration_duration }}"
 */
public class MetricsParser {

    // Config class.
    protected static final OpponentConfig OPPONENT_CONFIG = JarConfig.INST.getOpponent();

    // Constants.
    private static final String FD_LOG_EXTENSION = ".fdLog";
    private static final String CRASH_RECOVER_EVENT_EXTENSION = ".crashRecoverEvent";
    private static final String TOKEN_SEPARATOR = " ";
    private static final String TDM_LOG = "lsd%s QoS (TDm) Time: %s\n";
    private static final String TDM_AVG_LOG = "Averg QoS (TDm) Time: %s\n\n";
    private static final String TEM_LOG = "lsd%s QoS (TEm) Time: %s\n";
    private static final String TEM_AVG_LOG = "Averg QoS (TEm) Time: %s\n\n";
    private static final String DISCREPANCY_LOG = "Discrepancy removed: %s\n";
    private static final String LEADER_FORECAST = "Leader Forecast. Adding zero...\n";
    private static final long EXPERIMENT_TIME_THRESHOLD = OPPONENT_CONFIG.getDelayToStabilizeElection() * 1_000;
    private static final long METRIC_DISCREPANCY_THRESHOLD = 1000;

    private static String tdmResultFile;
    private static String temResultFile;

    private final Map<Integer, LinkedList<FdLog>> fdLogMap = new TreeMap<>();
    private final LinkedList<CrashRecoverEvent> crashRecoverEventList = new LinkedList<>();
    private long initialExperimentTimeThreshold;
    private long finalExperimentTimeThreshold;
    private long processFailureFrequency;

    public static void main(final String... args) {

        // Get arguments.
        final String inputDirectory = args[0];
        final String outputDirectory = args[1];
        final int failoverLeader = Integer.parseInt(args[2]);
        final String failureDetectorAlgorithm = args[3];
        final int iterationDuration = Integer.parseInt(args[4]);

        generateOutputfileNames(failureDetectorAlgorithm);

        try {
            new MetricsParser().run(inputDirectory, outputDirectory, failoverLeader, iterationDuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void generateOutputfileNames(final String failureDetectorAlgorithm) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String currDate = dateFormat.format(Calendar.getInstance().getTime());

        final String resultFilePrefix = currDate + "_" + failureDetectorAlgorithm;

        MetricsParser.tdmResultFile = resultFilePrefix + "_TDm.result";
        MetricsParser.temResultFile = resultFilePrefix + "_TEm.result";
    }

    private void run(final String inputDirectory, final String outputDirectory, final int failoverLeader,
            final int iterationDuration) throws IOException {
        // If it's a failure-free run skip this parser.
        if (isFailureFreeRun(inputDirectory)) {
            return;
        }

        // Parse input.
        if (!parseMetrics(inputDirectory, outputDirectory) || this.crashRecoverEventList.isEmpty()) {
            return;
        }

        // Extract experiment initial and final thresholds.
        extractExperimentThresholds(outputDirectory, iterationDuration);

        // Get rid of leader FdLog. This log is not used to extract TDm and TEm.
        removeLeaderFdLog(outputDirectory);

        // Extract processFailureFrequency.
        extractProcessFailureFrequency(outputDirectory);

        // Extract TDm and TEm metrics.
        extractTDmAndTEm(this.crashRecoverEventList, this.fdLogMap, outputDirectory, failoverLeader);
    }

    private boolean isFailureFreeRun(final String inputDirectory) throws IOException {
        // Read files inside the directory.
        try (final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(inputDirectory))) {
            for (final Path path : directoryStream) {
                final String fileName = path.getFileName().toString();

                // If it's not a failure-free run skip this parser.
                if (fileName.endsWith(CRASH_RECOVER_EVENT_EXTENSION)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean parseMetrics(final String inputDirectory, final String outputDirectory) throws IOException {
        int numberOfCrashRecoverEvent = 0;

        // Read files inside the directory.
        try (final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(inputDirectory))) {
            for (final Path path : directoryStream) {
                final String fileName = path.getFileName().toString();

                if (!fileName.endsWith(FD_LOG_EXTENSION) && !fileName.endsWith(CRASH_RECOVER_EVENT_EXTENSION)) {
                    continue;
                }

                if (fileName.endsWith(CRASH_RECOVER_EVENT_EXTENSION)) {
                    if (++numberOfCrashRecoverEvent > 1) {
                        return false;
                    }
                }

                final List<String> lines = Files.readAllLines(path, Charset.defaultCharset());

                if (fileName.endsWith(FD_LOG_EXTENSION)) {
                    this.fdLogMap.put(extractProcessId(fileName), parseFdLog(lines));
                } else if (fileName.endsWith(CRASH_RECOVER_EVENT_EXTENSION)) {
                    this.crashRecoverEventList.addAll(parseCrashRecoverEvent(lines));
                }
            }
        }

        return true;
    }

    private int extractProcessId(final String fileName) {
        return Integer.parseInt(fileName.substring(3, 5));
    }

    /**
     * Expected file content: Fri Jul 24 02:39:49 BRT 2020 1595569189915 14 14
     * 15.
     */
    private LinkedList<FdLog> parseFdLog(final List<String> lines) throws IOException {
        final LinkedList<FdLog> fds = new LinkedList<>();

        for (final String line : lines) {
            final String[] tokens = line.split(TOKEN_SEPARATOR);

            fds.add(FdLog.builder()
                    .eventDate(tokens[0] + TOKEN_SEPARATOR + tokens[1] + TOKEN_SEPARATOR + tokens[2] + TOKEN_SEPARATOR
                            + tokens[3] + TOKEN_SEPARATOR + tokens[4] + TOKEN_SEPARATOR + tokens[5] + TOKEN_SEPARATOR)
                    .eventTimeMillis(Long.parseLong(tokens[6])).processId(Integer.parseInt(tokens[7]))
                    .formerLeaderId(Integer.parseInt(tokens[8])).currentLeaderId(Integer.parseInt(tokens[9])).build());
        }

        return fds;
    }

    /**
     * Expected file content: Fri Jul 24 02:39:49 BRT 2020 1518523137036 14 OFF.
     */
    private List<CrashRecoverEvent> parseCrashRecoverEvent(final List<String> lines) throws IOException {
        final List<CrashRecoverEvent> cres = new LinkedList<>();

        for (final String line : lines) {
            final String[] tokens = line.split(TOKEN_SEPARATOR);

            cres.add(CrashRecoverEvent.builder()
                    .eventDate(tokens[0] + TOKEN_SEPARATOR + tokens[1] + TOKEN_SEPARATOR + tokens[2] + TOKEN_SEPARATOR
                            + tokens[3] + TOKEN_SEPARATOR + tokens[4] + TOKEN_SEPARATOR + tokens[5] + TOKEN_SEPARATOR)
                    .isProcessRunning(tokens[8].equals("ON"))
                    .processId(Integer.parseInt(tokens[7])).eventTime(Long.parseLong(tokens[6])).build());
        }

        return cres;
    }

    private void extractExperimentThresholds(final String outputDirectory, final int iterationDuration) {
        final int leaderProcessId = this.crashRecoverEventList.getFirst().getProcessId();
        final long experimentStartTime = this.fdLogMap.get(leaderProcessId).getFirst().getEventTimeMillis();

        initialExperimentTimeThreshold = experimentStartTime + EXPERIMENT_TIME_THRESHOLD;
        finalExperimentTimeThreshold = experimentStartTime + (iterationDuration * 1_000 - EXPERIMENT_TIME_THRESHOLD);
    }

    private void removeLeaderFdLog(final String outputDirectory) {
        final int leaderProcessId = this.crashRecoverEventList.getFirst().getProcessId();

        this.fdLogMap.remove(leaderProcessId);
    }

    private void extractProcessFailureFrequency(final String outputDirectory) {
        Long first = null;
        Long second = null;

        for (final CrashRecoverEvent crashRecoverEvent : this.crashRecoverEventList) {
            if (first == null) {
                first = crashRecoverEvent.getEventTime();
            } else if (second == null) {
                second = crashRecoverEvent.getEventTime();
            } else {
                break;
            }
        }

        this.processFailureFrequency = second - first;
    }

    /*
     * TDm: "Tempo de detecao de maioria: Momento apos a falha do lider no qual
     * uma maioria de processos corretos deteta permanentemente que o processo
     * lider falhou."
     *
     * TEm: "Tempo de estabilizacao de maioria: Momento apos a falha do lider em
     * que uma maioria de processos corretos aponta permanentemente o novo lider
     * como lider."
     */
    private void extractTDmAndTEm(final LinkedList<CrashRecoverEvent> ftList,
            final Map<Integer, LinkedList<FdLog>> fdMap, final String outputDirectory, final int failoverLeader) {
        Long previousFailureTime = null;

        for (final CrashRecoverEvent crashRecoverEvent : ftList) {
            // Evaluate thresholds.
            if (crashRecoverEvent.getEventTime() < initialExperimentTimeThreshold
                    || crashRecoverEvent.getEventTime() > finalExperimentTimeThreshold) {
                continue;
            }

            if (!crashRecoverEvent.isProcessRunning()) { // Failure.
                extractTDm(crashRecoverEvent, fdMap, outputDirectory);
                previousFailureTime = crashRecoverEvent.getEventTime();
            } else if (previousFailureTime != null) { // Recover.
                extractTEm(crashRecoverEvent, fdMap, outputDirectory, failoverLeader, previousFailureTime);
            }
        }
    }

    /*
     * Find first event after failureTime for each node.
     */
    private void extractTDm(final CrashRecoverEvent failureEvent, final Map<Integer, LinkedList<FdLog>> fdMap,
            final String outputDirectory) {
        final List<FdLog> fdLogs = new LinkedList<>();
        final long failureTime = failureEvent.getEventTime();

        // Iterate through each node.
        for (final Map.Entry<Integer, LinkedList<FdLog>> entry : fdMap.entrySet()) {

            // Find first event with time greater than the failure time.
            for (final FdLog fdLog : entry.getValue()) {
                if (fdLog.getEventTimeMillis() > failureTime
                        && fdLog.getFormerLeaderId() == failureEvent.getProcessId()) {
                    fdLogs.add(fdLog);
                    break;
                }
            }
        }

        writeMetric(fdLogs, failureTime, outputDirectory, TDM_LOG, TDM_AVG_LOG, MetricsParser.tdmResultFile);
    }

    private void extractTEm(final CrashRecoverEvent recoverEvent, final Map<Integer, LinkedList<FdLog>> fdMap,
            final String outputDirectory, final int failoverLeader, final Long previousFailureTime) {
        final List<FdLog> fdLogs = new LinkedList<>();

        final long recoveryTime = recoverEvent.getEventTime();
        FdLog previousEvent = null;

        // Iterate through each node.
        for (final Map.Entry<Integer, LinkedList<FdLog>> entry : fdMap.entrySet()) {
            // Find last event right before the recover time.
            for (final FdLog fdLog : entry.getValue()) {
                if (fdLog.getEventTimeMillis() > recoveryTime) {
                    fdLogs.add(previousEvent);
                    break;
                } else if (fdLog.getCurrentLeaderId() == failoverLeader) {
                    previousEvent = fdLog;
                }
            }
        }

        writeMetric(fdLogs, previousFailureTime, outputDirectory, TEM_LOG, TEM_AVG_LOG, MetricsParser.temResultFile);
    }

    private void writeMetric(final List<FdLog> fdLogs, final long failureTime, final String outputDirectory,
            final String metricLog, final String metricAvgLog, final String fileName) {
        final List<Long> metrics = new ArrayList<Long>();
        final StringBuilder sb = new StringBuilder("\n");

        // Get metrics.
        for (final FdLog fdLog : fdLogs) {
            final long metric = fdLog.getEventTimeMillis() - failureTime;

            sb.append(String.format(metricLog, fdLog.getProcessId(), metric));

            metrics.add(metric);
        }

        removeDiscrepancies(metrics, sb);

        if (metrics.isEmpty()) {
            return;
        }

        long sum = 0;

        for (final Long metric : metrics) {
            sum += metric;
        }

        long avg = sum / metrics.size();

        sb.append(String.format(metricAvgLog, avg));

        FileHandler.writeFile(outputDirectory + "/" + fileName, true, avg + " ");
    }

    private void removeDiscrepancies(final List<Long> metrics, final StringBuilder sb) {
        // Keep track of the discrepancies.
        final List<Long> discrepancies = new LinkedList<>();

        // Track number of leader forecasts.
        int zeroedMetric = 0;

        for (final long currMetric : metrics) {

            // If leader forecast.
            if (currMetric < 0 || currMetric > this.processFailureFrequency) {
                removeDiscrepant(discrepancies, currMetric, sb);
                ++zeroedMetric;
                continue;
            }

            if (currMetric > METRIC_DISCREPANCY_THRESHOLD) {
                removeDiscrepant(discrepancies, currMetric, sb);
            }
        }

        metrics.removeAll(discrepancies);

        for (int i = 0; i < zeroedMetric; ++i) {
            metrics.add(0L);
            sb.append(LEADER_FORECAST);
        }
    }

    private void removeDiscrepant(final List<Long> discrepancies, final long metric, final StringBuilder sb) {
        sb.append(String.format(DISCREPANCY_LOG, metric));
        discrepancies.add(metric);
    }
}
