package br.unicamp.ic.treplica.election;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;
import br.unicamp.ic.treplica.utils.FileHandler;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.Replica;
import lombok.extern.slf4j.Slf4j;

/**
 * Failure detector abstract class.
 */
@Slf4j
public abstract class IFailureDetector implements Runnable {

    // Constants.
    private static final String LOG_CONTENT = "%s %s %s %s %s\n";
    private static final JarConfig.FailureDetectorConfig FD_CONFIG = JarConfig.INST.getFailureDetector();
    private static final String FD_LOG_FILE = FD_CONFIG.getOutputDir() + "fdLog";

    private final ScheduledExecutorService timeoutExecutor = Executors.newScheduledThreadPool(1);
    private Thread onMessageReceiverThread;

    private final List<String> fdLogEntries;
    private final ElectionClient client;

    // This variable can be accessed by more than one thread at the same time.
    private volatile int leader;

    protected final int myId;
    protected final ITransport transport;
    protected final long onTimeoutDelta; // In milliseconds.
    protected final long onTimeoutDeltaInNanos; // In nanoseconds.

    static {
        // Create directory and sub-directories.
        new File(FD_CONFIG.getOutputDir()).mkdirs();

        // Make sure the file will be clean.
        FileHandler.deleteFile(FD_LOG_FILE);
    }

    /**
     * Constructor.
     *
     * @param onTimeoutDelta
     *            OnTimeoutDelta in milliseconds.
     * @param client
     *            Election client.
     * @param transport
     *            Transport.
     */
    protected IFailureDetector(final long onTimeoutDelta, final ElectionClient client, final ITransport transport) {
        this.myId = client.getPaxosId();
        this.client = client;
        this.transport = transport;
        this.onTimeoutDelta = onTimeoutDelta;
        this.onTimeoutDeltaInNanos = onTimeoutDelta * 1_000_000;
        this.fdLogEntries = new ArrayList<>();

        // Log FD params.
        logFDParams();

        // Considers itself as the leader.
        setLeader(this.myId);
    }

    public void shutdown() {
        /*
         * Call subclass shutdown.
         */
        shutdownChild();

        this.timeoutExecutor.shutdown();

        if (this.onMessageReceiverThread != null) {
            this.onMessageReceiverThread.interrupt();
        }

        // Transfer FD logs from memory to a file.
        flushLogEntriesToFile();

        log.debug("Shutdown");
    }

    /**
     * Method that can be overridden by subclasses in order to add custom logic
     * to the main shutdown() method.
     */
    protected void shutdownChild() {
    }

    /*
     * Getters and Setters.
     */

    protected synchronized int getLeader() {
        return this.leader;
    }

    protected synchronized void setLeader(final int leader) {
        final int formerLeader = this.leader;
        this.leader = leader;

        // Handle replica leadership.
        handleReplicaLeadership(this.leader);

        // Notifies ElectionClient the leader change.
        this.client.leaderChange(this.leader);

        // Add leader change info to log.
        appendToMemoryLog(this.leader, formerLeader);

        log.debug("My former leader is " + formerLeader);
        log.debug("My leader is " + this.leader);
    }

    /*
     * Abstract Methods.
     */

    /**
     * Process on timeout events.
     */
    protected abstract void onTimeoutEvent() throws TreplicaIOException, TreplicaSerializationException;

    /**
     * Process on received message.
     *
     * @param message
     *            received message.
     */
    protected abstract void onMessageEvent(final IMessage message)
            throws TreplicaIOException, TreplicaSerializationException;

    /*
     * Run.
     */

    @Override
    public void run() {
        /*
         * Call subclass run.
         */
        runChild();

        /*
         * The leader will multicast a heartbeat message every 'delta'
         * milliseconds.
         */
        this.timeoutExecutor.scheduleAtFixedRate(getOnTimeoutRunnable(), 0, this.onTimeoutDelta, TimeUnit.MILLISECONDS);

        /*
         * Starts the message receiver thread.
         */
        this.onMessageReceiverThread = new Thread(getOnMessageRunnable(), "OnMessageReceiverThread");
        this.onMessageReceiverThread.setDaemon(true);
        this.onMessageReceiverThread.start();
    }

    /**
     * Method that can be overridden by subclasses in order to add custom logic
     * to the main run() method.
     */
    protected void runChild() {
    }

    /**
     * Retrieves an instance of a class responsible for handling on timeout
     * events.
     */
    private Runnable getOnTimeoutRunnable() {
        // Anonymous class.
        return new Runnable() {

            @Override
            public void run() {
                try {
                    // Abstract method.
                    onTimeoutEvent();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        };
    }

    /**
     * Retrieves an instance of a class responsible for on message events.
     */
    private Runnable getOnMessageRunnable() {
        // Anonymous class.
        return new Runnable() {

            @Override
            public void run() {
                while (!onMessageReceiverThread.isInterrupted()) {
                    try {
                        // Waits for the next message.
                        final IMessage message = transport.receiveMessage((int) onTimeoutDelta);

                        if (message != null) {
                            // Abstract method.
                            onMessageEvent(message);
                        }
                    } catch (TreplicaIOException | TreplicaSerializationException e) {
                        e.printStackTrace();
                    }
                }
            }

        };
    }

    /*
     * Manage log entries.
     */

    private void addLogEntry(final String content) {
        synchronized (this.fdLogEntries) {
            this.fdLogEntries.add(content);
        }
    }

    private void flushLogEntriesToFile() {
        synchronized (this.fdLogEntries) {
            for (final String content : this.fdLogEntries) {
                FileHandler.writeFile(FD_LOG_FILE, true, content);
            }
        }
    }

    /*
     * Manage FD log.
     */

    /**
     * Appends information to the failure detector log.
     *
     * @param leaderId
     *            Leader identifier.
     */
    private void appendToMemoryLog(final int leaderId, final int formerLeaderId) {
        final long currentTimeMillis = System.currentTimeMillis();

        addLogEntry(String.format(LOG_CONTENT,
                new Date(currentTimeMillis),
                currentTimeMillis,
                this.myId,
                formerLeaderId,
                leaderId));
    }

    /**
     * Handle replica leadership.
     *
     * @param leaderId
     *            Leader identifier.
     */
    private void handleReplicaLeadership(final int leaderId) {
        // Handle leader file.
        if (this.myId == leaderId) {
            Replica.getInstance().setIsLeader();
        } else {
            Replica.getInstance().setIsNotLeader();
        }
    }

    /**
     * Log info regarding current failure detector in a file.
     */
    private void logFDParams() {
        final StringBuilder content = new StringBuilder("Failure Detector:\n")
                .append("\t algorithm: ").append(FD_CONFIG.getAlgorithm()).append("\n")
                .append("\t delta: ").append(FD_CONFIG.getDelta()).append("\n")
                .append("\t ip: ").append(FD_CONFIG.getIp()).append("\n")
                .append("\t port: ").append(FD_CONFIG.getPort()).append("\n")
                .append("\t outputDir: ").append(FD_CONFIG.getOutputDir());

        log.debug(content.toString());
    }
}
