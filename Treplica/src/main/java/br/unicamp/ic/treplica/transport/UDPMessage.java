/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.transport;

import java.io.Serializable;

/**
 * This class implements a UDP message.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class UDPMessage implements IMessage {

    private Serializable payload;
    private UDPTransportId sender;
    private UDPTransportId receiver;

    /**
     * Creates a new UDP message.
     * <p>
     *
     * @param payload
     *            the payload of this message.
     * @param sender
     *            the id of the socket sending this message.
     * @param receiver
     *            the id of the socket receiving this message.
     */
    public UDPMessage(final Serializable payload, final UDPTransportId sender, final UDPTransportId receiver) {
        this.payload = payload;
        this.sender = sender;
        this.receiver = receiver;
    }

    /*
     * (non-Javadoc)
     *
     * @see Message#getPayload()
     */
    @Override
    public Serializable getPayload() {
        return payload;
    }

    /*
     * (non-Javadoc)
     *
     * @see Message#getSender()
     */
    @Override
    public ITransportId getSender() {
        return sender;
    }

    /*
     * (non-Javadoc)
     *
     * @see Message#getReceiver()
     */
    @Override
    public ITransportId getReceiver() {
        return receiver;
    }

}
