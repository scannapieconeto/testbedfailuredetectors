/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

import java.io.Serializable;

/**
 * This interface represents a generic asynchronous persistent queue. A
 * persistent queue is an abstraction for a sequence of messages that are
 * exchanged by all clients sharing the same queue. It defines an order for the
 * messages and guarantees that all processes will see messages in this order.
 * Moreover, it assumes the clients are deterministic objects that only change
 * state whenever a message is received and only delivers a message in the same
 * state it was delivered in all other clients. As a consequence of this
 * behavior, it can be used to make the client state persistent.
 * <p>
 */
public interface PersistentQueue {

    /**
     * Binds a client to this persistent queue. The client must provide a state
     * manager and must guarantee that:
     * <ul>
     * <li>The state manager is able to take a meaningful snapshot before bind
     * returns (through a call to <code>StateManager.getState()</code>), or</li>
     * <li>It is able to replace the state with a provided snapshot before bind
     * returns (through a call to <code>StateManager.setState()</code>).</li>
     * </ul>
     * After these steps, the local state of the client will be consistent with
     * the next message received from the queue.
     * <p>
     *
     * The consistency guarantee provided by this method is true even for
     * multiple calls. For each call, the state is (possibly) updated to reflect
     * any consistent point in the history of messages and the queue is reset to
     * deliver the next logical message. Multiple calls however are not
     * guaranteed to return to the <emph>same</emph> logical point in the
     * message sequence.
     * <p>
     *
     * @param stateManager
     *            the keeper of the deterministic state to be made persistent.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     * @see StateManager#getState()
     * @see StateManager#setState(Serializable)
     */
    void bind(final StateManager stateManager) throws TreplicaIOException, TreplicaSerializationException;

    /**
     * Enqueues a message in the asynchronous persistent queue.
     * <p>
     *
     * @param message
     *            the message to be queued.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     */
    void enqueue(final Serializable message) throws TreplicaIOException, TreplicaSerializationException;

    /**
     * Dequeues a message from the asynchronous persistent queue.
     * <p>
     *
     * @return the next message from the queue.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     */
    Serializable dequeue() throws TreplicaIOException, TreplicaSerializationException;

    /**
     * Saves a current checkpoint of the underlying state managed by this queue,
     * potentially speeding recovery. This method is provided so the client of
     * the persistent queue can influence the checkpointing process, but the
     * queue implementation is free to implement its own checkpointing policy.
     * <p>
     *
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     */
    void checkpoint() throws TreplicaIOException, TreplicaSerializationException;

    /**
     * Graceful shutdown.
     */
    void shutdown();
}
