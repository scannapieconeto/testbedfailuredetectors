/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos.ledger;

import java.io.Serializable;

import br.unicamp.ic.treplica.paxos.BallotNumber;
import br.unicamp.ic.treplica.paxos.Proposal;
import br.unicamp.ic.treplica.paxos.Vote;

/**
 * This interface defines the Paxos ledger.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface Ledger extends Serializable {

    long NULL_COUNTER = -1;
    long FIRST_COUNTER = 0;

    /**
     * Returns the unique Paxos id of this ledger.
     * <p>
     *
     * @return the unique Paxos id of this ledger.
     */
    int getPaxosId();

    /**
     * Returns the last ballot the owner of this ledger has initiated for a
     * specified decree. If the decree isn't active, this method should return
     * the same ballot as the <code>getInactiveLastTried()</code> method.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return the last ballot number the owner of this ledger has initiated.
     * @see Ledger#getInactiveLastTried()
     */
    BallotNumber getLastTried(long decreeNumber);

    /**
     * Registers the last ballot the owner of this ledger has initiated for a
     * specified decree. If the decree isn't active, it is activated.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @param ballot
     *            the ballot number.
     */
    void setLastTried(long decreeNumber, BallotNumber ballot);

    /**
     * Returns the last ballot the owner of this ledger has initiated for all
     * inactive decrees.
     * <p>
     *
     * @return the last ballot number the owner of this ledger has initiated.
     */
    BallotNumber getInactiveLastTried();

    /**
     * Registers the last ballot the owner of this ledger has initiated for all
     * inactive decrees.
     * <p>
     *
     * @param ballot
     *            the ballot number.
     */
    void setInactiveLastTried(BallotNumber ballot);

    /**
     * Returns the current ballot the owner of this ledger has promised to vote,
     * for a specified decree. If the decree isn't active, this method should
     * return the same ballot as the <code>getInactiveNextBallot()</code>
     * method.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return the ballot number the owner of this ledger has promised to vote.
     * @see Ledger#getInactiveNextBallot()
     */
    BallotNumber getNextBallot(long decreeNumber);

    /**
     * Registers the current ballot the owner of this ledger has promised to
     * vote, for a specified decree. If the decree isn't active, it is
     * activated.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @param ballot
     *            the ballot number.
     */
    void setNextBallot(long decreeNumber, BallotNumber ballot);

    /**
     * Returns the current ballot the owner of this ledger has promised to vote,
     * for all inactive decrees.
     * <p>
     *
     * @return the current ballot number the owner of this ledger has promised
     *         to vote.
     */
    BallotNumber getInactiveNextBallot();

    /**
     * Registers the current ballot the owner of this ledger has promised to
     * vote, for all inactive decrees.
     * <p>
     *
     * @param ballot
     *            the ballot number.
     */
    void setInactiveNextBallot(BallotNumber ballot);

    /**
     * Returns the last vote the owner of this ledger has cast for a specified
     * decree.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return the last vote the owner of this ledger has cast;
     *         <code>null</code> if no vote was cast or if the decree is
     *         inactive.
     */
    Vote getPrevVote(long decreeNumber);

    /**
     * Registers the last vote the owner of this ledger has cast for a specified
     * decree. If the decree isn't active, it is activated.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @param vote
     *            the vote.
     */
    void setPrevVote(long decreeNumber, Vote vote);

    /**
     * Returns the winning proposal for a decree. This method returns
     * <code>null</code> when the decree is undecided, however it also returns
     * <code>null</code> when the <code>null</code> proposal is decided. To
     * discover if a decree is decided use the <code>isDecided(long)</code>
     * method.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return the winning proposal for a decree; <code>null</code> if no vote
     *         has won, if the decree is inactive or if the <code>null</code>
     *         proposal was decided.
     * @see Ledger#isDecided(long)
     */
    Proposal read(long decreeNumber);

    /**
     * Registers the winning vote for a decree. If the decree isn't active, it
     * is activated.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @param proposal
     *            the winning proposal.
     */
    void write(long decreeNumber, Proposal proposal);

    /**
     * Generates a unique classic ballot number higher than all ballots created
     * by previous invocations of this method.
     * <p>
     *
     * @return a unique classic ballot number.
     */
    BallotNumber generateNextClassicBallotNumber();

    /**
     * Generates a unique fast ballot number higher than all ballots created by
     * previous invocations of this method.
     * <p>
     *
     * @return a unique fast ballot number.
     */
    BallotNumber generateNextFastBallotNumber();

    /**
     * Ensures that the next ballot number generated will be higher than all
     * ballots created and higher than the provided ballot.
     * <p>
     *
     * @param ballot
     *            the reference ballot.
     */
    void ensureHigherBallotNumber(BallotNumber ballot);

    /**
     * Returns the number of the active decree with the highest number.
     * <p>
     *
     * @return the number of the active decree with the highest number.
     */
    long lastActiveDecree();

    /**
     * Returns the number of the undecided decree with the lowest number.
     * <p>
     *
     * @return the number of the undecided decree with the lowest number.
     */
    long firstUndecidedDecree();

    /**
     * Returns the number of the decided decree with the highest number.
     * <p>
     *
     * @return the number of the decided decree with the highest number.
     */
    long lastDecidedDecree();

    /**
     * Indicates if a decree is active.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return <code>true</code> if the decree is active; <code>false</code>
     *         otherwise.
     */
    boolean isActive(long decreeNumber);

    /**
     * Indicates if a decree is decided.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @return <code>true</code> if the decree is decided; <code>false</code>
     *         otherwise.
     */
    boolean isDecided(long decreeNumber);
}
