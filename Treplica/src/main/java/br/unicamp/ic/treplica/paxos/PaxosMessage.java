/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.io.Serializable;

/**
 * This class implements all messages sent by the Paxos protocol.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class PaxosMessage implements Serializable {
    private static final long serialVersionUID = -7879617906791748788L;

    public static final int LEADER = 0;
    public static final int PASS = 1;
    public static final int NEXT_BALLOT = 2;
    public static final int NEXT_BALLOT_INACTIVE = 3;
    public static final int LAST_VOTE = 4;
    public static final int LAST_VOTE_INACTIVE = 5;
    public static final int BEGIN_BALLOT = 6;
    public static final int VOTED = 7;
    public static final int SUCCESS = 8;
    public static final int LARGER_BALLOT = 9;
    public static final int ANY = 10;

    private int sender;
    private int type;
    private long decreeNumber;
    private BallotNumber ballotNumber;
    private Vote vote;
    private Proposal proposal;

    /**
     * Creates a Paxos message.
     * <p>
     *
     * @param sender
     *            the Paxos id of this message sender.
     * @param type
     *            the type of this message.
     * @param decreeNumber
     *            the decree number this message refers to.
     * @param ballotNumber
     *            the ballot number, if applicable.
     * @param vote
     *            the vote, if applicable.
     * @param proposal
     *            the proposal, if applicable.
     */
    public PaxosMessage(final int sender, final int type, final long decreeNumber, final BallotNumber ballotNumber,
            final Vote vote, final Proposal proposal) {
        this.sender = sender;
        this.type = type;
        this.decreeNumber = decreeNumber;
        this.ballotNumber = ballotNumber;
        this.vote = vote;
        this.proposal = proposal;
    }

    /**
     * Returns the Paxos id of this message sender.
     * <p>
     *
     * @return the Paxos id of this message sender.
     */
    public int getSender() {
        return sender;
    }

    /**
     * Returns the type of this message.
     * <p>
     *
     * @return the type of this message.
     */
    public int getType() {
        return type;
    }

    /**
     * Returns the decree number this message refers to.
     * <p>
     *
     * @return the decree number this message refers to.
     */
    public long getDecreeNumber() {
        return decreeNumber;
    }

    /**
     * Returns the ballot number, if applicable.
     * <p>
     *
     * @return the ballot number; <code>null</code> if this type of message
     *         doesn't hold a ballot number.
     */
    public BallotNumber getBallotNumber() {
        return ballotNumber;
    }

    /**
     * Returns the vote, if applicable.
     * <p>
     *
     * @return the vote; <code>null</code> if this type of message doesn't hold
     *         a vote.
     */
    public Vote getVote() {
        return vote;
    }

    /**
     * Returns the proposal, if applicable.
     * <p>
     *
     * @return the proposal; <code>null</code> if this type of message doesn't
     *         hold a proposal.
     */
    public Proposal getProposal() {
        return proposal;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return Integer.toHexString(sender) + ":" + type + ":" + decreeNumber + ":" + ballotNumber + ":" + vote + ":"
                + proposal;
    }

}
