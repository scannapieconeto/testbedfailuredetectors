package br.unicamp.ic.treplica.transport.opponent.strategy;

import br.unicamp.ic.treplica.transport.opponent.OpponentCommand;
import lombok.extern.slf4j.Slf4j;

/**
 * This class does not change change the network strategy.
 */
@Slf4j
public final class NoNetworkStrategy extends OpponentStrategy {

    public NoNetworkStrategy() {
        log.debug("Built NoNetworkStrategy");
    }

    @Override
    public void setupStrategy() {
        log.debug("Setup NoNetworkStrategy");
    }

    @Override
    public OpponentCommand getOpponentCommand() {
        return OpponentCommand.NO_FAILURE;
    }

    @Override
    public void runOpponentStrategy() throws InterruptedException {
    }
}
