package br.unicamp.ic.treplica.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public final class FileHandler {

    // Util class doesn't require a Constructor.
    private FileHandler() {
    }

    /**
     * Write content to a file.
     *
     * @param fileName
     *            Name of the file.
     * @param content
     *            Content to be written.
     */
    public static void writeFile(final String fileName, final String content) {
        writeFile(fileName, false, content);
    }

    /**
     * Write content to a file.
     *
     * @param fileName
     *            Name of the file.
     * @param append
     *            Indicates whether the content will be appended or not.
     * @param content
     *            Content to be written.
     */
    public static void writeFile(final String fileName, final boolean append, final String content) {
        try (final BufferedWriter fdOutLog = new BufferedWriter(new FileWriter(fileName, append))) {
            fdOutLog.append(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete a file.
     *
     * @param fileName
     *            Name of the file.
     */
    public static void deleteFile(final String fileName) {
        final File f = new File(fileName);

        if (f.exists()) {
            f.delete();

            // Make sure the file was deleted before leaving this method.
            while (f.exists()) {
            }
        }
    }
}
