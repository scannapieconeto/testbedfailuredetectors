package br.unicamp.ic.treplica.metrics;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import br.unicamp.ic.treplica.clustertools.ReplicatedMapLG;
import br.unicamp.ic.treplica.utils.FileHandler;

/**
 * Arguments:
 *
 * args[0]: input directory. "{{ experiments_results_dir }}/{{ item }}/"
 * args[1]: output directory. "{{ experiments_results_dir }}/"
 * args[2]: failure detector algorithm. "{{ failure_detector_algorithm }}"
 * args[3]: iteration duration. "{{ iteration_duration }}"
 */
public class LGMetricsParser {

    // Constants.
    private static final String LG_LOG_EXTENSION = ".treplica.output.log";
    private static final String TOKEN_SEPARATOR = " ";
    private static final String LOG_PREFIX = ReplicatedMapLG.LOG_PREFIX;

    private final List<Integer> lgMapSizeList = new ArrayList<>();
    private static String lgResultFile;

    public static void main(final String... args) {

        // Get arguments.
        final String inputDirectory = args[0];
        final String outputDirectory = args[1];
        final String failureDetectorAlgorithm = args[2];
        final int iterationDuration = Integer.parseInt(args[3]);

        generateOutputfileName(failureDetectorAlgorithm);

        try {
            new LGMetricsParser().run(inputDirectory, outputDirectory, iterationDuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void run(final String inputDirectory, final String outputDirectory, final int iterationDuration)
            throws IOException {
        // Parse input.
        parseMetrics(inputDirectory, outputDirectory);

        // Store median in the result file.
        final int median = getMedian(this.lgMapSizeList);
        final double reqPerSec = median / (double) iterationDuration;

        final DecimalFormat df = new DecimalFormat("0.00");
        final String reqPerSecStr = df.format(reqPerSec);
        FileHandler.writeFile(outputDirectory + "/" + LGMetricsParser.lgResultFile, true, reqPerSecStr + " ");
    }

    private void parseMetrics(final String inputDirectory, final String outputDirectory) throws IOException {
        // Read files inside the directory.
        try (final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(inputDirectory))) {
            for (final Path path : directoryStream) {
                final String fileName = path.getFileName().toString();

                if (!fileName.endsWith(LG_LOG_EXTENSION)) {
                    continue;
                }

                final List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
                final int finalMapSize = fetchLgMapSize(lines);

                if (finalMapSize < 0) {
                    continue;
                }

                this.lgMapSizeList.add(finalMapSize);
            }
        }
    }

    /**
     * Expected file content: LAST_MAP_SIZE: Tue Jul 21 02:31:07 BRT 2020
     * 1595309467105 14 61 287
     */
    private int fetchLgMapSize(final List<String> lines) throws IOException {
        for (final String line : lines) {
            if (!line.startsWith(LOG_PREFIX)) {
                continue;
            }

            final String[] tokens = line.split(TOKEN_SEPARATOR);
            return Integer.parseInt(tokens[tokens.length - 1]);
        }

        return -1;
    }

    private int getMedian(final List<Integer> list) {
        Collections.sort(list);

        final int size = list.size();
        final int middle = size / 2;
        if (size % 2 == 0) {
            return (list.get(middle) + list.get(middle - 1)) / 2;
        } else {
            return list.get(middle);
        }
    }

    private static void generateOutputfileName(final String failureDetectorAlgorithm) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String currDate = dateFormat.format(Calendar.getInstance().getTime());

        final String resultFilePrefix = currDate + "_" + failureDetectorAlgorithm;
        LGMetricsParser.lgResultFile = resultFilePrefix + "_LG_CONSENSUS_RATE.result";
    }
}
