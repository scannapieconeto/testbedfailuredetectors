package br.unicamp.ic.treplica.transport.opponent.strategy;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import br.unicamp.ic.treplica.transport.opponent.OpponentCommand;
import br.unicamp.ic.treplica.utils.Replica;
import lombok.extern.slf4j.Slf4j;

/*
 * Create a discrete integer-valued distribution from the input data. Values are assigned
 * mass based on their frequency.
 * For example, [0,1,1,2] as input creates a distribution with values 0, 1 and 2
 * having probability masses 0.25, 0.5 and 0.25 respectively.
 */
@Slf4j
public class EnumeratedDistributionNetworkStrategy extends OpponentStrategy {

    // Config.
    private static final boolean ONLY_LEADER_FAILS = OPPONENT_CONFIG.isNetworkFailureLeaderOnly();
    private static final int[] SAMPLE_SPACE = OPPONENT_CONFIG.getNetworkStrategySampleSpace();

    private final EnumeratedIntegerDistribution enumeratedIntegerDistribution;
    private volatile boolean wasStableLeader = false;
    private volatile boolean warmupDone = false;

    public EnumeratedDistributionNetworkStrategy() {
        this.enumeratedIntegerDistribution = new EnumeratedIntegerDistribution(SAMPLE_SPACE);
        log.debug("Built EnumeratedDistributionNetworkStrategy");
    }

    @Override
    public void setupStrategy() {
        super.setupStrategy();
        log.debug("Setup EnumeratedDistributionNetworkStrategy");
    }

    @Override
    public OpponentCommand getOpponentCommand() {
        if (!warmupDone || (ONLY_LEADER_FAILS && !wasStableLeader)) {
            return OpponentCommand.NO_FAILURE;
        }

        final int sample = this.enumeratedIntegerDistribution.sample();

        // Negative discrete values means drop.
        if (sample < 0) {
            return OpponentCommand.DROP;
        }

        // 0 means no delay whatsoever.
        if (sample == 0) {
            return OpponentCommand.NO_FAILURE;
        }

        return OpponentCommand.builder().delay(sample).drop(false).build();
    }

    @Override
    public void runOpponentStrategy() throws InterruptedException {
        warmupDone = true;
        wasStableLeader = Replica.getInstance().isLeader();
    }
}
