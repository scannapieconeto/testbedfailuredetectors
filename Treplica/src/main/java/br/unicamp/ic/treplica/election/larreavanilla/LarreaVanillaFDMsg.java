package br.unicamp.ic.treplica.election.larreavanilla;

import java.io.Serializable;

class LarreaVanillaFDMsg implements Serializable {

    private static final long serialVersionUID = -4975364741643456808L;
    private final int sender;

    LarreaVanillaFDMsg(final int sender) {
        this.sender = sender;
    }

    int getSender() {
        return this.sender;
    }
}
