package br.unicamp.ic.treplica.transport.opponent;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import br.unicamp.ic.treplica.transport.IMessageReceiver;
import br.unicamp.ic.treplica.transport.MessageReceiverDecorator;
import br.unicamp.ic.treplica.transport.UDPMessage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class OpponentMessageReceiverDecorator extends MessageReceiverDecorator {

    // Creates thread pool.
    private final ExecutorService threadPool = Executors.newCachedThreadPool();

    private final Opponent opponent;

    /**
     * Constructor.
     *
     * @param decoratedMessageReceiver
     *            decorated message receiver.
     */
    public OpponentMessageReceiverDecorator(final IMessageReceiver decoratedMessageReceiver) {
        super(decoratedMessageReceiver);
        opponent = Opponent.getInstance();

        log.debug("Built OpponentMessageReceiverDecorator");
    }

    /**
     * Method responsible for handling message receive events.
     *
     * @param messageQueue
     * @param message
     */
    @Override
    public void receiveMessage(final LinkedList<UDPMessage> messageQueue, final UDPMessage message) {
        final OpponentCommand command = this.opponent.handleRequest();

        if (command.isDrop()) {
            log.trace("dropping receiveMessage(...)");
            // Never adds to message queue.
            return;
        } else if (command.getDelay() > 0) {
            log.trace("delaying receiveMessage(...)");

            // Delays to add to message queue. Asynchronous processing.
            threadPool.execute(new DelayedReceiver(messageQueue, message, command.getDelay()));
            return;
        }

        // Adds to message queue normally.
        this.decoratedMessageReceiver.receiveMessage(messageQueue, message);
    }

    @Override
    public void shutdown() {
        this.threadPool.shutdown();
    }

    /*
     * Private Members.
     */

    /**
     * Inner class responsible for delaying message receive.
     */
    private class DelayedReceiver implements Runnable {

        private final LinkedList<UDPMessage> messageQueue;
        private final UDPMessage message;
        private final long delay;

        DelayedReceiver(final LinkedList<UDPMessage> messageQueue, final UDPMessage message, final long delay) {
            this.messageQueue = messageQueue;
            this.message = message;
            this.delay = delay;
        }

        @Override
        public void run() {
            // Delay message.
            sleep(this.delay);

            // Receive message.
            decoratedMessageReceiver.receiveMessage(messageQueue, message);
        }
    }

    private void sleep(final long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
        }
    }
}
