/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos.ledger;

import br.unicamp.ic.treplica.paxos.BallotNumber;

/**
 * This class implements the change performed by the
 * <code>setNextBallot(long, BallotNumber)</code> method to the ledger.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 * @see Ledger#setNextBallot(long, BallotNumber)
 */
public class SetNextBallot implements LedgerChange {
    private static final long serialVersionUID = -2983601961282094675L;

    private long decreeNumber;
    private BallotNumber ballot;

    /**
     * Creates the change with the method parameters.
     * <p>
     *
     * @param decreeNumber
     *            the decree number.
     * @param ballot
     *            the ballot number.
     */
    public SetNextBallot(final long decreeNumber, final BallotNumber ballot) {
        this.decreeNumber = decreeNumber;
        this.ballot = ballot;
    }

    /*
     * (non-Javadoc)
     *
     * @see LedgerChange#executeOnLedger(Ledger)
     */
    @Override
    public Object executeOnLedger(final Ledger ledger) {
        ledger.setNextBallot(decreeNumber, ballot);
        return null;
    }
}
