package br.unicamp.ic.treplica.transport.opponent.strategy;

import br.unicamp.ic.treplica.transport.opponent.OpponentCommand;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.OpponentConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class OpponentStrategy {

    // Constant.
    protected static final OpponentConfig OPPONENT_CONFIG = JarConfig.INST.getOpponent();
    private static final int DELAY_TO_STABILIZE_ELECTION = OPPONENT_CONFIG.getDelayToStabilizeElection();

    private Thread strategyThread;

    /**
     * Get opponent command.
     *
     * @return opponent command.
     */
    public abstract OpponentCommand getOpponentCommand();

    /**
     * Setup opponent strategy.
     */
    public void setupStrategy() {
        this.strategyThread = new Thread(getOponnentStrategyRunnable(), "OponnentStrategyThread");
        this.strategyThread.setDaemon(true);
        this.strategyThread.start();
    }

    /**
     * Run opponent specific strategy.
     */
    protected abstract void runOpponentStrategy() throws InterruptedException;

    /*
     * Private Methods.
     */

    private Runnable getOponnentStrategyRunnable() {
        // Anonymous class.
        return new Runnable() {

            @Override
            public void run() {
                try {
                    log.debug("DELAY_TO_STABILIZE_ELECTION: sleeping for " + DELAY_TO_STABILIZE_ELECTION);

                    // Wait until stabilize the election.
                    Thread.sleep(DELAY_TO_STABILIZE_ELECTION * 1_000);

                    log.debug("Stable leader!");

                    // Run opponent specific strategy.
                    runOpponentStrategy();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

        };
    }
}
