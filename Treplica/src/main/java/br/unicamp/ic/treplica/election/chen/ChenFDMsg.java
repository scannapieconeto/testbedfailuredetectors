package br.unicamp.ic.treplica.election.chen;

import java.io.Serializable;

class ChenFDMsg implements Serializable {

    private static final long serialVersionUID = -7146633960434300207L;
    private final int sender;
    private final long sequenceNumber;

    ChenFDMsg(final int sender, final long sequenceNumber) {
        this.sender = sender;
        this.sequenceNumber = sequenceNumber;
    }

    int getSender() {
        return this.sender;
    }

    long getSequenceNumber() {
        return this.sequenceNumber;
    }
}
