package br.unicamp.ic.treplica.transport.opponent;

import br.unicamp.ic.treplica.transport.opponent.strategy.EnumeratedDistributionNetworkStrategy;
import br.unicamp.ic.treplica.transport.opponent.strategy.NoNetworkStrategy;
import br.unicamp.ic.treplica.transport.opponent.strategy.NormalDistributionNetworkStrategy;
import br.unicamp.ic.treplica.transport.opponent.strategy.OpponentStrategy;
import br.unicamp.ic.treplica.transport.opponent.strategy.ProcessFailureStrategy;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.OpponentConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class Opponent {

    // Singleton.
    private static Opponent instance;

    private final OpponentStrategy processStrategy;
    private final OpponentStrategy networkStrategy;

    // Private constructor.
    private Opponent() {
        this.processStrategy = new ProcessFailureStrategy();
        this.processStrategy.setupStrategy();

        this.networkStrategy = getNetworkOpponentStrategy();
    }

    // Singleton logic.
    public static synchronized Opponent getInstance() {
        if (instance == null) {
            log.debug("Creating Opponent singleton...");
            instance = new Opponent();
        }

        return instance;
    }

    public OpponentCommand handleRequest() {
        final OpponentCommand processOpponentCommand = this.processStrategy.getOpponentCommand();

        if (processOpponentCommand.isDrop()) {
            return processOpponentCommand;
        }

        return this.networkStrategy.getOpponentCommand();
    }

    /*
     * Private Method.
     */

    private OpponentStrategy getNetworkOpponentStrategy() {
        final OpponentConfig opponentConfig = JarConfig.INST.getOpponent();
        final String ns = opponentConfig.getNetworkStrategy();

        OpponentStrategy os;

        switch (ns) {
            case "ENUMERATED_DISTRIBUTION_NETWORK_STRATEGY":
                os = new EnumeratedDistributionNetworkStrategy();
                break;
            case "NORMAL_DISTRIBUTION_NETWORK_STRATEGY":
                os = new NormalDistributionNetworkStrategy();
                break;
            case "NO_NETWORK_STRATEGY":
            default:
                os = new NoNetworkStrategy();
                break;
        }

        // Setup strategy.
        os.setupStrategy();

        return os;
    }
}
