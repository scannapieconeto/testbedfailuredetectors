package br.unicamp.ic.treplica.transport;

import java.util.LinkedList;

public interface IMessageReceiver {

    /**
     * Method responsible for handling message receive events.
     *
     * @param messageQueue
     * @param message
     */
    void receiveMessage(final LinkedList<UDPMessage> messageQueue, final UDPMessage message);

    /**
     * Shutdown.
     */
    void shutdown();
}
