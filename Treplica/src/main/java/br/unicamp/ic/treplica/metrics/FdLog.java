package br.unicamp.ic.treplica.metrics;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FdLog {
    private String eventDate;
    private long eventTimeMillis;
    private int processId;
    private int formerLeaderId;
    private int currentLeaderId;
}
