/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright �� 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.transport;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Random;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.common.LRUHashMap;
import br.unicamp.ic.treplica.common.Marshall;
import lombok.extern.slf4j.Slf4j;

/**
 * This class implements a UDP multicast transport. It allows to send and
 * receive multicast and unicast messages. Unicast addressing is done using the
 * transport id object returned by <code>getId()</code>.
 * <p>
 *
 * This class needs a functional UDP multicast route to work. Multicast routing
 * is no different than unicast routing: packets are routed accordingly to the
 * routes netmasks and the multicast IP. As it is very uncommon to have explicit
 * multicast routes, multicast packets will be usually routed to the default
 * gateway subnet. For hosts with only one interface, it is necessary that this
 * subnet delivers multicast packets correctly (not firewalled). If the host has
 * two or more interfaces and the default gateway subnet isn't suitable for
 * multicast, an explicit route for multicast traffic must be created pointing
 * to a suitable subnet. For example, in Linux a route could be set with this
 * command:
 * <p>
 * <code>   route add -net 224.0.0.0/4 eth0</code>
 * <p>
 *
 * FIXME: Receive queue is unbounded. Establish a maximum size for the receive
 * queue.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
@Slf4j
public class UDPTransport implements ITransport {

    public static final int MAX_DATA_SIZE = 65507; // 20 bytes IP, 8 bytes UDP
    public static final int HEADER_SIZE = 6;

    private final String mIP;
    private final int mPort;
    private final IMessageReceiver messageReceiver;
    private final IMessageSender messageSender;
    private final DatagramSocket uSocket;
    private final MulticastSocket mSocket;
    private final UDPTransportId id;
    private final LinkedList<UDPMessage> messageQueue;

    /**
     * Creates a new UDP transport with provided multicast IP and port.
     * <p>
     *
     * @param address
     *            the multicast ip address.
     * @param port
     *            the multicast port.
     * @param messageReceiver
     *            the messageReceiver.
     * @param messageSender
     *            the messageSender.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     */
    public UDPTransport(final String address, final int port, final IMessageReceiver messageReceiver,
            final IMessageSender messageSender) throws TreplicaIOException {
        try {
            this.mIP = address;
            this.mPort = port;
            this.messageReceiver = messageReceiver;
            this.messageSender = messageSender;

            this.uSocket = new DatagramSocket();
            this.mSocket = new MulticastSocket(this.mPort);
            this.mSocket.joinGroup(InetAddress.getByName(this.mIP));
            this.id = discoverTransportId();
            this.messageQueue = new LinkedList<UDPMessage>();

            Thread t = new Thread(new Receiver(this.uSocket, this.id), "UDPTransport Unicast Receiver Thread");
            t.setDaemon(true);
            t.start();

            t = new Thread(new Receiver(this.mSocket, null), "UDPTransport Multicast Receiver Thread");
            t.setDaemon(true);
            t.start();

            log.debug("Built UDPTransport: IP = " + this.mIP + " Port = " + this.mPort);
        } catch (IOException e) {
            throw new TreplicaIOException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see Transport#getId()
     */
    @Override
    public ITransportId getId() {
        return this.id;
    }

    /*
     * Receive Message.
     */

    /*
     * (non-Javadoc)
     *
     * @see Transport#receiveMessage()
     */
    @Override
    public IMessage receiveMessage() {
        return receiveMessage(0);
    }

    /*
     * (non-Javadoc)
     *
     * @see Transport#receiveMessage(int)
     */
    @Override
    public IMessage receiveMessage(final int timeout) {
        synchronized (this.messageQueue) {
            if (this.messageQueue.size() == 0) {
                try {
                    this.messageQueue.wait(timeout);
                } catch (InterruptedException e) {
                }
            }

            if (this.messageQueue.size() == 0) {
                return null; /* Timeout */
            }

            return this.messageQueue.removeFirst();
        }
    }

    /*
     * Send Message.
     */

    /*
     * (non-Javadoc)
     *
     * @see Transport#sendMessage(Serializable)
     */
    @Override
    public void sendMessage(final Serializable message) throws TreplicaIOException, TreplicaSerializationException {
        sendMessage(message, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see Transport#sendMessage(Serializable, TransportId)
     */
    @Override
    public void sendMessage(final Serializable message, final ITransportId destination)
            throws TreplicaIOException, TreplicaSerializationException {
        messageSender.sendMessage(mIP, mPort, uSocket, message, destination);
    }

    @Override
    public void shutdown() {
        if (this.messageReceiver != null) {
            this.messageReceiver.shutdown();
        }

        if (this.messageSender != null) {
            this.messageSender.shutdown();
        }
    }

    /*
     * Private Class and Methods.
     */

    /**
     * This class implements a message receiver for a UDP socket. It listens to
     * messages, receives them, unmarshalls the payload, and put then in the
     * receive queue.
     * <p>
     */
    private class Receiver implements Runnable {

        private static final int CACHE_SIZE = 30;

        private DatagramSocket socket;
        private UDPTransportId receiver;
        private LRUHashMap<Integer, Fragments> fragmentCache;

        protected Receiver(final DatagramSocket socket, final UDPTransportId receiver) {
            this.socket = socket;
            this.receiver = receiver;
            fragmentCache = new LRUHashMap<Integer, Fragments>(CACHE_SIZE);
        }

        @Override
        public void run() {
            byte[] receiveBuffer = new byte[MAX_DATA_SIZE];
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            while (true) {
                try {
                    socket.receive(receivePacket);
                    byte[] payload = reassemble(receivePacket.getData(), receivePacket.getLength());
                    if (payload != null) {
                        final UDPMessage message = new UDPMessage(Marshall.unmarshall(payload),
                                new UDPTransportId(receivePacket.getAddress().getAddress(), receivePacket.getPort()),
                                receiver);

                        messageReceiver.receiveMessage(messageQueue, message);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(
                            "Exception in UDPTransport message receiver thread. " + "Thread stopped!", e);
                } catch (TreplicaSerializationException e) {
                    /* Corrupted message: discard. */
                }
            }
        }

        /**
         * Tries to reassemble a message using the provided fragment and other
         * fragments stored in the fragment cache. If it isn't possible to
         * reassemble the packet the fragment is cached.
         * <p>
         *
         * @param source
         *            the fragment to be reassembled.
         * @param length
         *            the length of the fragment.
         * @return the reassembled byte array if possible to reassemble;
         *         <code>null</code> otherwise.
         */
        private byte[] reassemble(final byte[] source, final int length) {
            final ByteBuffer buffer = ByteBuffer.wrap(source, 0, length);
            final int messageId = buffer.getInt();
            final byte fragment = buffer.get();
            final byte fragNumber = buffer.get();

            if (fragNumber > 1) {
                Fragments fragments = fragmentCache.get(messageId);
                if (fragments == null) {
                    fragments = new Fragments(fragNumber);
                    fragmentCache.put(messageId, fragments);
                }
                return fragments.register(fragment, copyRange(source, 0, length));
            }

            return copyRange(source, HEADER_SIZE, length);
        }

        /**
         * Creates a copy of a byte array.
         * <p>
         *
         * @param original
         *            the original byte array.
         * @param from
         *            the starting position of the copy.
         * @param to
         *            the ending position of the copy.
         * @return a new byte array containing the elements between from
         *         (inclusive) and to (exclusive).
         */
        private byte[] copyRange(final byte[] original, final int from, final int to) {
            final byte[] copy = new byte[to - from];
            for (int i = 0; i < copy.length; ++i) {
                copy[i] = original[i + from];
            }

            return copy;
        }

    }

    /**
     * This class sends UDP messages to a determined IP and port, through a
     * provided socket until it is interrupted.
     * <p>
     */
    private class UDPSpammer implements Runnable {

        private DatagramSocket socket;
        private InetAddress destination;
        private int port;

        protected UDPSpammer(final DatagramSocket socket, final InetAddress destination, final int port) {
            this.socket = socket;
            this.destination = destination;
            this.port = port;
        }

        @Override
        public void run() {
            byte[] buffer = new byte[1];
            final DatagramPacket packet = new DatagramPacket(buffer, buffer.length, destination, port);

            while (true) {
                try {
                    socket.send(packet);
                    Thread.sleep(1000);
                } catch (IOException e) {
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    /**
     * This class stores the cached fragments of a message.
     * <p>
     */
    private static class Fragments {
        private byte number;
        private int size;
        private byte[][] fragments;

        Fragments(final byte total) {
            number = 0;
            size = 0;
            fragments = new byte[total][];
            for (int i = 0; i < fragments.length; ++i) {
                fragments[i] = null;
            }
        }

        /**
         * Registers a fragment.
         * <p>
         *
         * @param place
         *            the place of the fragment in the sequence.
         * @param fragment
         *            the fragment.
         * @return the reassembled message if it is complete; <code>null</code>
         *         otherwise.
         */
        public byte[] register(final byte place, final byte[] fragment) {
            if (fragments[place] == null) {
                fragments[place] = fragment;
                number++;
                size += fragment.length - HEADER_SIZE;
            }

            if (number >= fragments.length) {
                byte[] buffer = new byte[size];
                int position = 0;
                for (int i = 0; i < fragments.length; i++) {
                    for (int j = HEADER_SIZE; j < fragments[i].length; j++) {
                        buffer[position++] = fragments[i][j];
                    }
                }
                return buffer;
            }

            return null;
        }
    }

    /**
     * This method is a dirty hack to discover the default multicast interface
     * of this host. It is necessary for multihomed machines to make sure our
     * local id is the same as the sending address of our multicast messages.
     * <p>
     *
     * @return the transport ID that will appear in sent multicast messages.
     * @throws IOException
     *             if an I/O error occurs.
     */
    private UDPTransportId discoverTransportId() throws IOException {
        int port = mPort;

        while (port == mPort) {
            port = new Random().nextInt(28000) + 2000;
        }

        final MulticastSocket dummy = new MulticastSocket(port);
        dummy.joinGroup(InetAddress.getByName(mIP));

        final Thread t = new Thread(new UDPSpammer(uSocket, InetAddress.getByName(mIP), port));
        t.start();

        byte[] dummyBuffer = new byte[16];

        final DatagramPacket dummyPacket = new DatagramPacket(dummyBuffer, dummyBuffer.length);
        dummy.receive(dummyPacket);

        while (!isLocalAddress(dummyPacket.getAddress())) {
            dummy.receive(dummyPacket);
        }

        t.interrupt();
        dummy.close();

        return new UDPTransportId(dummyPacket.getAddress().getAddress(), dummyPacket.getPort());
    }

    /**
     * Checks if an internet protocol address belongs to some interface in this
     * host.
     * <p>
     *
     * @param address
     *            the address to check.
     * @return <code>true</code> if this address is local; <code>false</code>
     *         otherwise.
     * @throws IOException
     *             if an I/O error occurs.
     */
    private boolean isLocalAddress(final InetAddress address) throws IOException {
        final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

        while (interfaces.hasMoreElements()) {
            NetworkInterface i = interfaces.nextElement();
            Enumeration<InetAddress> addresses = i.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress a = addresses.nextElement();
                if (address.equals(a)) {
                    return true;
                }
            }
        }

        return false;
    }

}
