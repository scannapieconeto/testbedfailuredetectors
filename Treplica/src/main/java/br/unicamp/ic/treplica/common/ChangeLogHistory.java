/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * @author Luiz Eduardo Buzato
 */
public class ChangeLogHistory {

    private final CheckpointStore checkpoints = new CheckpointStore();
    private final HashMap<Long, ChangesStore> changesStore = new HashMap<Long, ChangesStore>();

    ChangeLogHistory() {
        checkpoints.clear();
        changesStore.clear();
    }

    public void clear() {
        checkpoints.clear();
        changesStore.clear();
    }

    public boolean isEmpty() {
        return (checkpoints.isEmpty() && changesStore.isEmpty());
    }

    /*
     * Methods dealing with checkpoints
     *
     *
     */

    /**
     *
     * readCheckpoint
     *
     * @param checkpointId
     *
     * @return checkpoint (Serializable); null if a checkpoint identified by
     *         checkpointId has not been found in the checkpoint store.
     *
     */
    public Serializable readCheckpoint(final long checkpointId) {
        Serializable aCheckpoint = null;

        aCheckpoint = checkpoints.readCheckpoint(checkpointId);

        return aCheckpoint;
    }

    public void writeCheckpoint(final long checkpointId, final Serializable checkpoint) {

        checkpoints.writeCheckpoint(checkpointId, checkpoint);

    }

    public Serializable deleteCheckpoint(final long checkpointId) throws NoSuchElementException {
        return checkpoints.deleteCheckpoint(checkpointId);
    }

    /**
     * readMostRecentCheckpoint
     *
     * @return checkpoint most recently stored into the checkpoint store; null
     *         if the checkpoint store is empty. checkpoint must honor
     *         Serializable.
     *
     */
    public Serializable readMostRecentCheckpoint() {
        return checkpoints.readMostRecentCheckpoint();
    }

    public long numberOfCheckpoints() {
        return checkpoints.numberOfCheckpoints();
    }

    public long mostRecentCheckpoint() {
        return checkpoints.mostRecentCheckpoint();
    }

    /*
     * Methods dealing with change blocks and changes associated with a
     * checkpoint
     */

    public void writeChange(final long checkpointId, final long changeBlockId, final Serializable change) {
        ChangesStore targetChangesStore = null;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
        } else {
            targetChangesStore = new ChangesStore();
            changesStore.put(checkpointId, targetChangesStore);
        }

        targetChangesStore.writeChange(changeBlockId, change);
    }

    public LinkedList<Serializable> readChanges(final long checkpointId, final long changeBlockId) {
        LinkedList<Serializable> theChanges = null;
        ChangesStore targetChangesStore = null;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
            theChanges = targetChangesStore.readChanges(changeBlockId);
        }

        return theChanges;
    }

    /*
     * FIXME: deletion of ChangesStore elements is poorly defined must redesign
     * this part of the ChangeLogHistory and consequently the API of
     * ChangesStore For while, this is not a major concern because for every
     * checkpoint that is collected from the ChangeLogHistory I will simply
     * clear the corresponding ChangesStore entry.
     */
    public void deleteChanges(final long checkpointId, final long changeBlockId) throws NoSuchElementException {
        ChangesStore targetChangesStore = null;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
            targetChangesStore.deleteChanges(changeBlockId);
        } else {
            throw new NoSuchElementException();
        }
    }

    public long changesStoreReset(final long checkpointId) {
        ChangesStore targetChangesStore = null;
        long numberOfChanges = 0;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
            numberOfChanges = targetChangesStore.resetChangeStore();
        }

        return numberOfChanges;

    }

    public long changesStoreNextChangeBlockId(final long checkpointId) {
        ChangesStore targetChangesStore = null;
        long nextBlockId = 0;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
            nextBlockId = targetChangesStore.nextChangeBlockId();
        }

        return nextBlockId;

    }

    public boolean changesStoreHasNextChange(final long checkpointId) {
        ChangesStore targetChangesStore = null;
        boolean hasChanges = false;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
            hasChanges = targetChangesStore.hasNextChange();
        }

        return hasChanges;
    }

    public Serializable changesStoreNextChange(final long checkpointId) throws NoSuchElementException {
        ChangesStore targetChangesStore = null;
        Serializable theChange = null;

        if (changesStore.containsKey(checkpointId)) {
            targetChangesStore = changesStore.get(checkpointId);
            theChange = targetChangesStore.nextChange();
        } else {
            throw new NoSuchElementException();
        }

        return theChange;
    }

}
