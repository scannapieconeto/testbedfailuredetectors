/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica;

/**
 * This class implements a generic Treplica exception.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TreplicaException extends Exception {
    private static final long serialVersionUID = 3976249244665406950L;

    /**
     * Constructs a new <code>TreplicaException</code> object with
     * <code>null</code> as its detail message.
     * <p>
     */
    public TreplicaException() {
        super();
    }

    /**
     * Constructs a new <code>TreplicaException</code> object with the specified
     * detail message.
     * <p>
     *
     * @param message
     *            the detail message.
     */
    public TreplicaException(final String message) {
        super(message);
    }

    /**
     * Constructs a new <code>TreplicaException</code> object with the specified
     * detail message and cause.
     * <p>
     *
     * @param message
     *            the detail message.
     * @param cause
     *            the cause.
     */
    public TreplicaException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new <code>TreplicaException</code> object with the specified
     * cause and a detail message of <code>cause.toString()</code>.
     * <p>
     *
     * @param cause
     *            the cause.
     */
    public TreplicaException(final Throwable cause) {
        super(cause);
    }
}
