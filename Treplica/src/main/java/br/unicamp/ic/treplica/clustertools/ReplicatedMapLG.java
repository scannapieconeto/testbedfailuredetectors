/*
 * Cluster-Tools - Tools for running cluster experiments
 *
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This file is part of Cluster-Tools.
 *
 * Cluster-Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.clustertools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.examples.ReplicatedMap;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.LoadGeneratorConfig;
import br.unicamp.ic.treplica.utils.Replica;

public class ReplicatedMapLG implements ILoad {

    // Config class.
    private static final LoadGeneratorConfig LOAD_GENERATOR_CONFIG = JarConfig.INST.getLoadGenerator();

    // Config parameters.
    private static final int NUMBER_OF_REPLICAS = LOAD_GENERATOR_CONFIG.getNumberOfReplicas();
    private static final String STABLE_MEDIA = LOAD_GENERATOR_CONFIG.getStableMedia();

    private final ReplicatedMap<String, String> replicatedMap;
    private long mapKey;
    private final int myReplicaId = Replica.getInstance().getMyId();
    private final List<String> lgLogEntries;
    private static final String LOG_CONTENT = "%s %d %d %d %d\n";
    private static final String MAP_CONTENT = "%s %s\n";
    public static final String LOG_PREFIX = "LAST_MAP_SIZE: ";

    /**
     * Constructor.
     *
     * @param nProcesses
     *            Number of processes.
     * @param stableMedia
     *            Stable media location.
     * @throws TreplicaException
     */
    public ReplicatedMapLG(final int nProcesses, final String stableMedia) throws TreplicaException {
        this.replicatedMap = new ReplicatedMap<String, String>(nProcesses, stableMedia);
        this.mapKey = 0;
        this.lgLogEntries = new ArrayList<>();
    }

    @Override
    public void run() {
        this.replicatedMap.put(
                this.myReplicaId + "-" + this.mapKey++,
                "" + System.currentTimeMillis());

        // Add log entry every put.
        addLogEntry();
    }

    private void addLogEntry() {
        if (this.replicatedMap == null) {
            return;
        }

        final long currentTimeMillis = System.currentTimeMillis();

        this.lgLogEntries.add(String.format(LOG_CONTENT,
                new Date(currentTimeMillis),
                currentTimeMillis,
                this.myReplicaId,
                this.mapKey,
                this.replicatedMap.size()));
    }

    @Override
    public List<String> getLogEntries() {
        // Register final entry.
        final long currentTimeMillis = System.currentTimeMillis();
        String content = String.format(LOG_CONTENT,
                new Date(currentTimeMillis),
                currentTimeMillis,
                this.myReplicaId,
                this.mapKey,
                this.replicatedMap.size());

        content = LOG_PREFIX + content;
        this.lgLogEntries.add(content);

        return this.lgLogEntries;
    }

    @Override
    public List<String> getDataStructureContent() {
        final List<String> list = new ArrayList<>();

        if (this.replicatedMap != null) {
            for (final Map.Entry<String, String> entry : this.replicatedMap.entrySet()) {
                if (entry == null) {
                    continue;
                }

                list.add(String.format(MAP_CONTENT,
                        entry.getKey(),
                        entry.getValue()));
            }
        }

        return list;
    }

    @Override
    public void shutdown() {
        if (this.replicatedMap != null) {
            this.replicatedMap.shutdown();
        }
    }

    /**
     * Main
     */
    public static void main(final String[] args) throws Exception {
        final ILoad replicatedMapLG = new ReplicatedMapLG(NUMBER_OF_REPLICAS, STABLE_MEDIA);
        new LoadGenerator().go(replicatedMapLG);

        // Self-destruction to abort all pending requests.
        System.exit(0);
    }
}
