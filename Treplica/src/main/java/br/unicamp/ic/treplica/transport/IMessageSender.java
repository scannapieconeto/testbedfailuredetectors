package br.unicamp.ic.treplica.transport;

import java.io.Serializable;
import java.net.DatagramSocket;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;

public interface IMessageSender {

    /**
     * Method responsible for handling message send events.
     *
     * @param mIP
     * @param mPort
     * @param uSocket
     * @param message
     * @param destination
     * @throws TreplicaSerializationException
     * @throws TreplicaIOException
     */
    void sendMessage(final String mIP, final int mPort, final DatagramSocket uSocket, final Serializable message,
            final ITransportId destination) throws TreplicaSerializationException, TreplicaIOException;

    /**
     * Shutdown.
     */
    void shutdown();
}
