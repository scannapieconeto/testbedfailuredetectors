/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import br.unicamp.ic.treplica.paxos.ledger.Ledger;
import br.unicamp.ic.treplica.transport.ITransportId;

/**
 * This class implements the coordinator abstraction of Paxos. The coordinator
 * role is performed by the leader and it has the responsibility of conducting
 * all ballots. It also advertises the result of old ballots to processes
 * (double acting as distinct learner).
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Coordinator {

    private static final int ACTIVE = 0;
    private static final int INACTIVE = 1;
    private static final int ACTIVATING = 2;

    private int state;
    private Secretary secretary;
    private int classicMajority;
    private int fastMajority;
    private int roundtrip;
    private HashMap<Long, CoordinatorBallot> ballots;
    private HashSet<Integer> activationAnswers;
    private long activationTimestamp;
    private long activationPoint;
    private boolean useFast;
    private PaxosMessage any;
    private long anyTimestamp;

    /**
     * Creates a coordinator that accesses the ledger and sends messages through
     * the provided secretary. A coordinator also needs to know the number of
     * processes that establish a majority, how long it should wait for replies
     * from other processes and if it should create fast ballots.
     * <p>
     *
     * @param secretary
     *            the secretary that handles I/O.
     * @param classicMajority
     *            the number of processes that establish a classic Paxos
     *            majority.
     * @param fastMajority
     *            the number of processes that establish a fast Paxos majority.
     * @param roundtrip
     *            the time to wait for replies from the other processes.
     * @param useFast
     *            indicates if this coordinator will start fast ballots.
     */
    public Coordinator(final Secretary secretary, final int classicMajority, final int fastMajority,
            final int roundtrip, final boolean useFast) {
        state = INACTIVE;
        this.secretary = secretary;
        this.classicMajority = classicMajority;
        this.fastMajority = fastMajority;
        this.roundtrip = roundtrip;
        this.useFast = useFast;
        any = null;
    }

    /**
     * Activates this coordinator. This method should be called whenever this
     * process becomes a leader.
     * <p>
     */
    public void activate() {
        if (state == INACTIVE) {
            activationTimestamp = 0;
            startActivation();
            state = ACTIVATING;
        }
    }

    /**
     * Deactivates this coordinator. This method should be called whenever this
     * process stops being a leader.
     * <p>
     */
    public void deactivate() {
        state = INACTIVE;
    }

    /**
     * Processes a coordinator message received by the protocol.
     * <p>
     *
     * @param message
     *            the message received.
     * @param sender
     *            the message sender.
     */
    public void processMessage(final PaxosMessage message, final ITransportId sender) {
        if (state == ACTIVE) {
            switch (message.getType()) {
                case PaxosMessage.PASS:
                    processPass(message, sender);
                    break;
                case PaxosMessage.LAST_VOTE:
                    processLastVote(message);
                    break;
                case PaxosMessage.LARGER_BALLOT:
                    processLargerBallot(message);
                    break;
            }
        } else if (state == ACTIVATING) {
            switch (message.getType()) {
                case PaxosMessage.LAST_VOTE_INACTIVE:
                    processLastVoteInactive(message);
                    break;
                case PaxosMessage.LARGER_BALLOT:
                    processLargerBallot(message);
                    break;
            }
        }
    }

    /**
     * Processes a <code>PaxosMessage.PASS</code> message received by the
     * coordinator.
     * <p>
     *
     * When asking for a proposal to pass, the sender of a PASS message sets the
     * desired decree number. This happens to allow the sender to discover if
     * its proposal was actually passed without requiring retransmission of the
     * PASS message. If the requested decree number is already taken a SUCCESS
     * message is sent if it is decided, or it is ignored if it is active but
     * undecided. If the requested decree number is free (inactive) a
     * BEGIN_BALLOT message is sent to start the final phase of the protocol.
     * <p>
     *
     * @param message
     *            the message received.
     * @param sender
     *            the message sender.
     */
    private void processPass(final PaxosMessage message, final ITransportId sender) {
        Ledger ledger = secretary.getLedger();
        long decreeNumber = message.getDecreeNumber();
        if (ledger.isDecided(decreeNumber)) {
            PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.SUCCESS, decreeNumber, null,
                    null, ledger.read(decreeNumber));
            secretary.queueMessage(reply, sender);
        } else {
            CoordinatorBallot ballot = ballots.get(decreeNumber);
            if (ballot == null) {
                if (ledger.getInactiveLastTried().isFast() || decreeNumber < activationPoint) {
                    startCompleteBallot(decreeNumber, message.getProposal());
                } else {
                    startPresetBallot(decreeNumber, message.getProposal());
                }
            }
        }
    }

    /**
     * Starts a ballot with the same ballot number this coordinator used for
     * activation. At least a majority of processes has agreed to vote in this
     * ballot number, so this ballot starts in the PENDING_SUCCESS state.
     *
     * @param decreeNumber
     *            the decree number of the ballot.
     * @param proposal
     *            the proposal to be voted.
     */
    private void startPresetBallot(final long decreeNumber, final Proposal proposal) {
        Ledger ledger = secretary.getLedger();
        BallotNumber ballotNumber = ledger.getLastTried(decreeNumber);
        CoordinatorBallot ballot = new CoordinatorBallot(CoordinatorBallot.PENDING_SUCCESS, proposal);
        ballots.put(decreeNumber, ballot);
        PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.BEGIN_BALLOT, decreeNumber,
                ballotNumber, null, proposal);
        secretary.queueMessage(reply);
    }

    /**
     * Starts a ballot from scratch, using a new ballot number larger than any
     * other this coordinator has used. Even when using fast ballots, the ballot
     * created by this method is always classic, as a complete ballot starts by
     * asking last votes for a majority.
     *
     * @param decreeNumber
     *            the decree number of the ballot.
     * @param proposal
     *            the proposal to be voted.
     */
    private void startCompleteBallot(final long decreeNumber, final Proposal proposal) {
        Ledger ledger = secretary.getLedger();
        BallotNumber ballotNumber = ledger.generateNextClassicBallotNumber();
        ledger.setLastTried(decreeNumber, ballotNumber);
        CoordinatorBallot ballot = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE, proposal);
        ballots.put(decreeNumber, ballot);
        PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.NEXT_BALLOT, decreeNumber,
                ballotNumber, null, null);
        secretary.queueMessage(reply);
    }

    /**
     * Processes a <code>PaxosMessage.LAST_VOTE</code> message received by the
     * coordinator. Messages referring to an inexistent ballot or to the wrong
     * ballot number are ignored.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processLastVote(final PaxosMessage message) {
        Ledger ledger = secretary.getLedger();
        long decreeNumber = message.getDecreeNumber();
        BallotNumber ballotNumber = ledger.getLastTried(decreeNumber);
        if (ballotNumber.equals(message.getBallotNumber())) {
            CoordinatorBallot ballot = ballots.get(decreeNumber);
            if (ballot != null && ballot.getState() == CoordinatorBallot.WAITING_LAST_VOTE) {
                ballot.registerLastVote(message.getSender(), message.getVote());
                int majority = ballotNumber.isFast() ? fastMajority : classicMajority;
                if (ballot.receivedLastVote() >= majority) {
                    PaxosMessage reply = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.BEGIN_BALLOT,
                            decreeNumber, ballotNumber, null, ballot.getWinningProposal());
                    secretary.queueMessage(reply);
                    ballot.setState(CoordinatorBallot.PENDING_SUCCESS);
                }
            }
        }
    }

    /**
     * Processes a <code>PaxosMessage.LAST_VOTE_INACTIVE</code> message received
     * by the coordinator. Messages referring to the wrong ballot number are
     * ignored.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processLastVoteInactive(final PaxosMessage message) {
        Ledger ledger = secretary.getLedger();
        BallotNumber ballotNumber = ledger.getInactiveLastTried();
        if (ballotNumber.equals(message.getBallotNumber())) {
            activationAnswers.add(message.getSender());
            if (activationPoint < message.getDecreeNumber()) {
                activationPoint = message.getDecreeNumber();
            }
            activationTimestamp = System.currentTimeMillis();
        }

        final int majority = ballotNumber.isFast() ? fastMajority : classicMajority;

        if (activationAnswers.size() >= majority) {
            for (long i = ledger.firstUndecidedDecree(); i < activationPoint; i++) {
                if (!ledger.isDecided(i)) {
                    createBallotIfNecessary(i);
                }
            }
            if (ledger.getInactiveLastTried().isFast()) {
                any = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.ANY, activationPoint,
                        ledger.getInactiveLastTried(), null, null);
                secretary.queueMessage(any);
                anyTimestamp = System.currentTimeMillis();
            }
            state = ACTIVE;
        }
    }

    /**
     * Creates and registers a new ballot if one isn't already registered. The
     * new ballot is started in the WAITING_LAST_VOTE state but no BEGIN_BALLOT
     * messages are sent by this method.
     * <p>
     *
     * @param decreeNumber
     *            the decree number of the ballot.
     */
    private void createBallotIfNecessary(final long decreeNumber) {
        CoordinatorBallot ballot = ballots.get(decreeNumber);
        if (ballot == null) {
            ballot = new CoordinatorBallot(CoordinatorBallot.WAITING_LAST_VOTE);
            ballots.put(decreeNumber, ballot);
        }
    }

    /**
     * Processes a <code>PaxosMessage.LARGER_BALLOT</code> message received by
     * the coordinator. The fact this message is received indicates another
     * coordinator is or was present with a possibly larger inactive last vote.
     * If this coordinator still believes it is active, it must re-execute
     * activation to get accurate last vote inactive promises.
     * <p>
     *
     * @param message
     *            the message received.
     */
    private void processLargerBallot(final PaxosMessage message) {
        Ledger ledger = secretary.getLedger();
        if (ledger.getInactiveLastTried().compareTo(message.getBallotNumber()) < 0) {
            ledger.ensureHigherBallotNumber(message.getBallotNumber());
            deactivate();
            activate();
        }
    }

    /**
     * Process a clock tick generated by the protocol. Depending on the current
     * state of this coordinator, it activates one of the two recurring
     * activities: <code>startActivation</code> or <code>restartBallots</code>.
     * <p>
     */
    public void processTick() {
        if (state == ACTIVATING) {
            startActivation();
        } else if (state == ACTIVE) {
            restartBallots();
            if (secretary.getLedger().getInactiveLastTried().isFast()
                    && (System.currentTimeMillis() - anyTimestamp) > 10 * roundtrip) {
                secretary.queueMessage(any);
                anyTimestamp = System.currentTimeMillis();
            }
        }
    }

    /**
     * Starts (or restarts) the activation process.
     * <p>
     */
    private void startActivation() {
        Ledger ledger = secretary.getLedger();
        if ((System.currentTimeMillis() - activationTimestamp) > roundtrip) {
            ballots = new HashMap<Long, CoordinatorBallot>();
            activationAnswers = new HashSet<Integer>();
            activationPoint = ledger.lastActiveDecree() + 1;

            final BallotNumber ballot = useFast ? ledger.generateNextFastBallotNumber()
                    : ledger.generateNextClassicBallotNumber();

            ledger.setInactiveLastTried(ballot);
            PaxosMessage message = new PaxosMessage(secretary.getPaxosId(), PaxosMessage.NEXT_BALLOT_INACTIVE,
                    Ledger.NULL_COUNTER, ledger.getInactiveLastTried(), null, null);
            secretary.queueMessage(message);
            activationTimestamp = System.currentTimeMillis();
        }
    }

    /**
     * Restart all stale ballots. A stale ballot is one that have not received a
     * message for roundtrip milliseconds.
     * <p>
     */
    private void restartBallots() {
        Ledger ledger = secretary.getLedger();
        long now = System.currentTimeMillis();
        int sent = 0;
        for (Iterator<Entry<Long, CoordinatorBallot>> i = ballots.entrySet().iterator(); i.hasNext() && sent < 100;) {
            Entry<Long, CoordinatorBallot> entry = i.next();
            long decreeNumber = entry.getKey();
            CoordinatorBallot ballot = entry.getValue();
            if (ledger.isDecided(decreeNumber)) {
                i.remove();
            } else if ((now - ballot.getTimestamp()) > roundtrip) {
                startCompleteBallot(decreeNumber, ballot.getWinningProposal());
                sent++;
            }
        }
    }

}
