package br.unicamp.ic.treplica.transport.opponent.strategy;

import java.util.Date;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import br.unicamp.ic.treplica.transport.opponent.OpponentCommand;
import br.unicamp.ic.treplica.utils.FileHandler;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.Replica;
import lombok.extern.slf4j.Slf4j;

/**
 * This class is responsible for failing and recovering processes.
 */
@Slf4j
public final class ProcessFailureStrategy extends OpponentStrategy {

    // Constants.
    private static final String WRITE_EVENT_FORMAT = "%s %s %s %s\n";

    // Config.
    private static final boolean ONLY_LEADER_FAILS = OPPONENT_CONFIG.isProcessFailureLeaderOnly();
    private static final int[] SAMPLE_SPACE = OPPONENT_CONFIG.getProcessStrategySampleSpace();

    // Files.
    private static final String OUTPUT_DIR = JarConfig.INST.getFailureDetector().getOutputDir();
    private static final String CRASH_RECOVER_EVENT_FILE = OUTPUT_DIR + "crashRecoverEvent";

    private final EnumeratedIntegerDistribution enumeratedIntegerDistribution;
    private boolean processIsRunning;
    private volatile OpponentCommand currentCommand;
    private volatile boolean warmupDone = false;

    public ProcessFailureStrategy() {
        this.processIsRunning = true;
        this.currentCommand = OpponentCommand.NO_FAILURE;
        this.enumeratedIntegerDistribution = new EnumeratedIntegerDistribution(SAMPLE_SPACE);
        log.debug("Built ProcessFailureStrategy");
    }

    @Override
    public void setupStrategy() {
        super.setupStrategy();
        log.debug("Setup ProcessFailureStrategy");
    }

    @Override
    public OpponentCommand getOpponentCommand() {
        if (!warmupDone) {
            return OpponentCommand.NO_FAILURE;
        }

        return this.currentCommand;
    }

    @Override
    public void runOpponentStrategy() throws InterruptedException {
        warmupDone = true;

        // If the process is the current leader or all processes should fail.
        if (!ONLY_LEADER_FAILS || Replica.getInstance().isLeader()) {
            handleProcessState();
        }
    }

    /*
     * Private Methods.
     */

    private void handleProcessState() throws InterruptedException {
        int currentSample = 0;

        while (true) {
            currentSample = this.enumeratedIntegerDistribution.sample();

            log.debug("PROCESS_OPPONENT_SAMPLE_SPACE: sleeping for " + currentSample);
            Thread.sleep(currentSample * 1_000);

            if (this.processIsRunning) {
                // If process is running, then FAIL.
                this.currentCommand = OpponentCommand.DROP;
                this.processIsRunning = false;
                log.debug("Failed!");
            } else {
                // If process is not running, then RECOVER.
                this.currentCommand = OpponentCommand.NO_FAILURE;
                this.processIsRunning = true;
                log.debug("Recovered!");
            }

            writeEvent();
        }
    }

    private void writeEvent() {
        final long currentTimeMillis = System.currentTimeMillis();

        final String content = String.format(WRITE_EVENT_FORMAT,
                new Date(currentTimeMillis),
                currentTimeMillis,
                Replica.getInstance().getMyId(),
                this.processIsRunning ? "ON" : "OFF");

        // Write Crash Recover Event file.
        FileHandler.writeFile(CRASH_RECOVER_EVENT_FILE, true, content);
    }
}
