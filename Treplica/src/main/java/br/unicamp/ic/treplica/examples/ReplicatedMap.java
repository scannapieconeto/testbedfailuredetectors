/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.examples;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import br.unicamp.ic.treplica.Action;
import br.unicamp.ic.treplica.StateMachine;
import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;

/**
 * This class implements a replicated and persistent map using Treplica.
 * <p>
 *
 * This map implements all operations of a map, but changes can only be made to
 * the map itself and not to the keys, values or entries collections.
 * <p>
 *
 * FIXME: Revise and check return values and exceptions, now that Treplica
 * supports them.
 *
 * @author Gustavo Maciel Dias Vieira
 *
 * @param <K>
 *            the type of keys maintained by this map
 * @param <V>
 *            the type of mapped values
 */
public class ReplicatedMap<K, V> implements Map<K, V> {

    private final StateMachine stateMachine;

    /**
     * Constructor.
     *
     * @param nProcesses
     *            Number of processes.
     * @param stableMedia
     *            Stable media location.
     * @throws TreplicaIOException
     * @throws TreplicaSerializationException
     */
    public ReplicatedMap(final int nProcesses, final String stableMedia)
            throws TreplicaIOException, TreplicaSerializationException {
        this(new HashMap<K, V>(), nProcesses, stableMedia);
    }

    /**
     * Constructor.
     *
     * @param map
     *            Internal map.
     * @param nProcesses
     *            Number of processes.
     * @param stableMedia
     *            Stable media location.
     * @throws TreplicaIOException
     * @throws TreplicaSerializationException
     */
    public ReplicatedMap(final Map<K, V> map, final int nProcesses, final String stableMedia)
            throws TreplicaIOException, TreplicaSerializationException {
        stateMachine = StateMachine.createPaxosSM((Serializable) map, nProcesses, stableMedia);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#clear()
     */
    @Override
    public void clear() {
        try {
            stateMachine.execute(new ClearAction<K, V>());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see Map#containsKey(Object)
     */
    @Override
    public boolean containsKey(final Object key) {
        return getMap().containsKey(key);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#containsValue(java.lang.Object)
     */
    @Override
    public boolean containsValue(final Object value) {
        return getMap().containsValue(value);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#entrySet()
     */
    @Override
    public Set<Entry<K, V>> entrySet() {
        return getUnmodifiableMap().entrySet();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#get(java.lang.Object)
     */
    @Override
    public V get(final Object key) {
        return getMap().get(key);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return getMap().isEmpty();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#keySet()
     */
    @Override
    public Set<K> keySet() {
        return getUnmodifiableMap().keySet();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#put(java.lang.Object, java.lang.Object)
     */
    @Override
    public V put(final K key, final V value) {
        try {
            stateMachine.execute(new PutAction<K, V>(key, value));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#putAll(java.util.Map)
     */
    @Override
    public void putAll(final Map<? extends K, ? extends V> m) {
        try {
            stateMachine.execute(new PutAllAction<K, V>(m));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#remove(java.lang.Object)
     */
    @Override
    public V remove(final Object key) {
        try {
            stateMachine.execute(new RemoveAction<K, V>(key));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#size()
     */
    @Override
    public int size() {
        return getMap().size();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Map#values()
     */
    @Override
    public Collection<V> values() {
        return getUnmodifiableMap().values();
    }

    /**
     * Graceful shutdown.
     */
    public void shutdown() {
        if (this.stateMachine != null) {
            this.stateMachine.shutdown();
        }
    }

    /*
     * Private Methods.
     */

    private Map<K, V> getMap() {
        @SuppressWarnings("unchecked")
        HashMap<K, V> map = (HashMap<K, V>) stateMachine.getState();
        return map;
    }

    private Map<K, V> getUnmodifiableMap() {
        return Collections.unmodifiableMap(getMap());
    }

    /*
     * Actions.
     */

    private abstract static class MapAction<K, V> implements Action, Serializable {
        private static final long serialVersionUID = -6869113999902100650L;

        @Override
        @SuppressWarnings("unchecked")
        public Object executeOn(final Object stateMachine) {
            return executeOnMap((Map<K, V>) stateMachine);
        }

        public abstract Object executeOnMap(Map<K, V> map);
    }

    private static class ClearAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = 2254640730942764981L;

        @Override
        public Object executeOnMap(final Map<K, V> map) {
            map.clear();
            return null;
        }
    }

    private static class PutAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = 5571337399619932368L;

        private K key;
        private V value;

        PutAction(final K key, final V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public Object executeOnMap(final Map<K, V> map) {
            map.put(key, value);
            return null;
        }
    }

    private static class PutAllAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = -6483055420159223231L;

        private Map<? extends K, ? extends V> m;

        PutAllAction(final Map<? extends K, ? extends V> m) {
            this.m = m;
        }

        @Override
        public Object executeOnMap(final Map<K, V> map) {
            map.putAll(m);
            return null;
        }
    }

    private static class RemoveAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = 2851481535406574278L;

        private Object key;

        RemoveAction(final Object key) {
            this.key = key;
        }

        @Override
        public Object executeOnMap(final Map<K, V> map) {
            map.remove(key);
            return null;
        }
    }

    /**
     * Main.
     *
     * @param args
     *            Arguments.
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Usage: ReplicatedMap <nprocess> <stablemedia>\n"
                    + "       <nprocess>    - number of replicas\n" + "       <stablemedia> - where state is saved");
            System.exit(0);
        }

        final Map<String, String> map = new ReplicatedMap<String, String>(Integer.parseInt(args[0]), args[1]);
        final BufferedReader terminal = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("> ");
            String command = terminal.readLine();
            if (command == null || command.length() == 0) {
                continue;
            }
            if ("q".equals(command)) {
                System.exit(0);
            } else if ('g' == command.charAt(0) && command.length() > 2) {
                String key = command.split(" ")[1];
                System.out.println(map.get(key));
            } else if ('p' == command.charAt(0) && command.length() > 2) {
                String key = command.split(" ")[1];
                String value = command.split(" ", 3)[2];
                map.put(key, value);
                System.out.println("Key added.");
            } else if ('r' == command.charAt(0) && command.length() > 2) {
                String key = command.split(" ")[1];
                map.remove(key);
                System.out.println("Key removed.");
            } else if ("c".equals(command)) {
                map.clear();
                System.out.println("All keys removed.");
            } else if ("l".equals(command)) {
                for (Entry<String, String> entry : map.entrySet()) {
                    System.out.println(entry.getKey() + ": " + entry.getValue());
                }
            } else if ("s".equals(command)) {
                System.out.println(map.size());
            }
        }
    }
}
