package br.unicamp.ic.treplica.election.aguilera;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.election.IFailureDetector;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;

/**
 * Failure detector algorithm based on paper 'Heartbeat: A Timeout-Free Failure
 * Detector for Quiescent Reliable Communication' (AguileraChenToueg1997).
 */
public class AguileraFD extends IFailureDetector {

    private final ScheduledExecutorService leaderElectionExecutor = Executors.newScheduledThreadPool(1);

    private final Map<Integer, AguileraFDElement> processesMap = new ConcurrentHashMap<Integer, AguileraFDElement>();

    private final Set<AguileraFDElement> processSet = new TreeSet<>(new AguileraFDElement.IdComparator());

    // I am alive message.
    private final AguileraFDMsg iAmAliveMsg;

    /**
     * Constructor.
     *
     * @param delta
     *            Delta in milliseconds.
     * @param client
     *            Election client.
     * @param transport
     *            Transport.
     */
    public AguileraFD(final int delta, final ElectionClient client, final ITransport transport) {
        // Call super class constructor.
        super(delta, client, transport);

        // Set message.
        this.iAmAliveMsg = new AguileraFDMsg(this.myId);
    }

    /*
     * IFailureDetector implementation.
     */

    @Override
    protected void onTimeoutEvent() throws TreplicaIOException, TreplicaSerializationException {
        /*
         * The process will multicast a heartbeat message every 'onTimeoutDelta'
         * milliseconds.
         */
        this.transport.sendMessage(this.iAmAliveMsg);
    }

    @Override
    protected void onMessageEvent(final IMessage message) throws TreplicaIOException, TreplicaSerializationException {
        /*
         * When a message is received, basically, increments the counter of this process.
         */
        final int msgSender = ((AguileraFDMsg) message.getPayload()).getSender();

        final AguileraFDElement processElem = this.processesMap.get(msgSender);

        if (null != processElem) {
            processElem.incrementCounter();
        } else {
            this.processesMap.put(msgSender, new AguileraFDElement(msgSender));
        }
    }

    @Override
    protected void runChild() {
        /*
         * Executor responsible for inferring the elected leader based on vector's content.
         */
        this.leaderElectionExecutor.scheduleAtFixedRate(getOnLeaderElectionRunnable(),
                0, this.onTimeoutDelta, TimeUnit.MILLISECONDS);
    }

    @Override
    protected void shutdownChild() {
        this.leaderElectionExecutor.shutdown();
    }

    /*
     * Private Methods.
     */

    private Runnable getOnLeaderElectionRunnable() {
        // Anonymous class.
        return new Runnable() {

            @Override
            public void run() {
                try {
                    /*
                     * Detect the new leader.
                     */
                    onDetectLeaderEvent(processesMap.values());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        };
    }

    /**
     * Attempt to detect the new leader. Try to elect the trustworthy process
     * with lower identifier.
     */
    private void onDetectLeaderEvent(final Collection<AguileraFDElement> processes) {
        this.processSet.clear();
        this.processSet.addAll(processes);

        // Get trustworthy process with lower identifier (use a TreeSet and a
        // comparator to do it).
        for (final AguileraFDElement processElem : this.processSet) {

            // Update trustworthy.
            processElem.updateTrustworthy();

            if (processElem.isTrustworthy()) {

                if (processElem.getProcessId() != this.getLeader()) {
                    setLeader(processElem.getProcessId());
                }

                break;
            }
        }
    }
}
