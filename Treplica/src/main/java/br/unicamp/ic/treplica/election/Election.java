/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright �� 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.election;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.election.aguilera.AguileraFD;
import br.unicamp.ic.treplica.election.chen.ChenFD;
import br.unicamp.ic.treplica.election.larreaepoch.LarreaEpochFD;
import br.unicamp.ic.treplica.election.larreavanilla.LarreaVanillaFD;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.transport.ITransport;
import br.unicamp.ic.treplica.transport.TransportFactory;
import br.unicamp.ic.treplica.transport.TransportFactory.TransportFactoryCaller;
import br.unicamp.ic.treplica.utils.JarConfig;

/**
 * This class implements a leader election service. This service is a $\Omega$
 * failure detector. It just guarantees that exists a process p and a time t
 * such as after t all processes trust p.
 * <p>
 *
 * The current implementation uses a delta to indicate the interval between
 * expected leader I-am-alive messages.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Election {

    private static final JarConfig.FailureDetectorConfig FD_CONFIG = JarConfig.INST.getFailureDetector();

    private IFailureDetector fd = null;
    private Thread fdThread = null;
    private ITransport fdTransport;

    /*
     * Failure Detector Algorithms.
     */
    public enum FDAlgorithm {
        LARREA_VANILLA("LarreaVanillaFD"),
        LARREA_EPOCH("LarreaEpochFD"),
        AGUILERA("AguileraFailureDetector"),
        CHEN("ChenFailureDetector"),
        MALKHI("MalkhiFailureDetector");

        private final String description;

        FDAlgorithm(final String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }
    }

    /**
     * Creates a leader election service that sends messages through the
     * provided secretary and has the provided delta.
     *
     * @param algorithm
     *            the failure detection algorithm.
     * @param delta
     *            the delta between I-am-alive messages in milliseconds.
     * @param client
     *            the client of this leader election service.
     * @param maxNumberOfProcesses
     *            maximum number of processes.
     */
    public Election(final FDAlgorithm algorithm, final int delta, final ElectionClient client,
            final int maxNumberOfProcesses) {
        try {
            if (fdThread == null) {
                fdTransport = TransportFactory.getTransport(
                        FD_CONFIG.getIp(), FD_CONFIG.getPort(),
                        TransportFactoryCaller.FAILURE_DETECTOR);

                fdThread = getFailureDetectorThread(algorithm, delta, client, maxNumberOfProcesses, fdTransport);
                fdThread.setDaemon(true);
                fdThread.start();
            }
        } catch (TreplicaIOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Graceful shutdown.
     */
    public void shutdown() {
        if (this.fdTransport != null) {
            this.fdTransport.shutdown();
        }

        if (this.fd != null) {
            this.fd.shutdown();
        }
    }

    /*
     * Private Methods.
     */

    private Thread getFailureDetectorThread(final FDAlgorithm algorithm, final int delta, final ElectionClient client,
            final int maxNumberOfProcesses, final ITransport transport) {
        switch (algorithm) {
            case LARREA_EPOCH:
                this.fd = new LarreaEpochFD(delta, client, transport);
                return new Thread(this.fd, FDAlgorithm.LARREA_EPOCH.getDescription());
            case AGUILERA:
                this.fd = new AguileraFD(delta, client, transport);
                return new Thread(this.fd, FDAlgorithm.AGUILERA.getDescription());
            case CHEN:
                this.fd = new ChenFD(delta, client, transport);
                return new Thread(this.fd, FDAlgorithm.CHEN.getDescription());
            case LARREA_VANILLA:
            default:
                this.fd = new LarreaVanillaFD(delta, client, transport);
                return new Thread(this.fd, FDAlgorithm.LARREA_VANILLA.getDescription());
        }
    }
}
