package br.unicamp.ic.treplica.election.larreavanilla;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.election.IFailureDetector;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;

/**
 * Failure detector algorithm based on paper 'Optimal Implementation of the
 * Weakest Failure Detector for Solving Consensus' (LarreaFernandezArevalo2000).
 */
// TODO In order to prevent this from happening an infinite number of times, pi
// also increases the value of the timeout period.
public class LarreaVanillaFD extends IFailureDetector {

    // This variable can be accessed by more than one thread.
    private volatile long leaderTimestampInNanos;

    // I am alive message.
    private final LarreaVanillaFDMsg iAmAliveMsg;

    /**
     * Constructor.
     *
     * @param delta
     *            Delta in milliseconds.
     * @param client
     *            Election client.
     * @param transport
     *            Transport.
     */
    public LarreaVanillaFD(final int delta, final ElectionClient client, final ITransport transport) {
        // Call super class constructor.
        super(delta, client, transport);

        // Set message.
        this.iAmAliveMsg = new LarreaVanillaFDMsg(this.myId);

        this.leaderTimestampInNanos = System.nanoTime();
    }

    /*
     * IFailureDetector implementation.
     */

    @Override
    protected void onTimeoutEvent() throws TreplicaIOException, TreplicaSerializationException {
        if (this.myId == getLeader()) {
            /*
             * Task 1 (LarreaFernandezArevalo2000):
             *
             * If this process is the current leader, multicast an I-AM-ALIVE
             * message.
             */
            this.transport.sendMessage(this.iAmAliveMsg);
        } else if ((System.nanoTime() - this.leaderTimestampInNanos) > this.onTimeoutDeltaInNanos) {
            /*
             * Task 2 (LarreaFernandezArevalo2000):
             *
             * If leader's message does not arrive until the timeout, the
             * process considers itself leader.
             */
            setLeader(this.myId);
            this.transport.sendMessage(this.iAmAliveMsg);
        }
    }

    @Override
    protected void onMessageEvent(final IMessage message) throws TreplicaIOException, TreplicaSerializationException {
        final int msgSender = ((LarreaVanillaFDMsg) message.getPayload()).getSender();

        /*
         * Task 3 (LarreaFernandezArevalo2000):
         *
         * If a message, from a process with lower id, arrives, considers this
         * process the current leader.
         */

        if (msgSender < getLeader()) {
            setLeader(msgSender);
        }

        if (msgSender == getLeader()) {
            this.leaderTimestampInNanos = System.nanoTime();
        }
    }
}
