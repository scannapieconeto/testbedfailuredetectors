/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.paxos;

import java.io.Serializable;

/**
 * This class represents a vote in a ballot for some decree.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Vote implements Serializable {
    private static final long serialVersionUID = 1428096550386769903L;

    private BallotNumber ballotNumber;
    private Proposal proposal;

    /**
     * Creates a new vote.
     * <p>
     *
     * @param ballotNumber
     *            the ballot number for which this vote was cast.
     * @param proposal
     *            the proposal voted.
     * @throws NullPointerException
     *             if the ballot number is <code>null</code>.
     */
    public Vote(final BallotNumber ballotNumber, final Proposal proposal) {
        if (ballotNumber == null) {
            throw new NullPointerException();
        }
        this.ballotNumber = ballotNumber;
        this.proposal = proposal;
    }

    /**
     * Returns the ballot number for which this vote was cast.
     * <p>
     *
     * @return the ballot number for which this vote was cast.
     */
    public BallotNumber getBallotNumber() {
        return ballotNumber;
    }

    /**
     * Returns the proposal voted.
     * <p>
     *
     * @return the proposal voted.
     */
    public Proposal getProposal() {
        return proposal;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Vote) {
            Vote vote = (Vote) o;
            return ballotNumber.equals(vote.ballotNumber)
                    && (proposal == null ? vote.proposal == null : proposal.equals(vote.proposal));
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int proposalHashCode = proposal == null ? 0 : proposal.hashCode();
        return (ballotNumber.hashCode() << 16) + proposalHashCode;
    }

    /*
     * (non-Javadoc)
     *
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return Integer.toHexString(hashCode());
    }

}
