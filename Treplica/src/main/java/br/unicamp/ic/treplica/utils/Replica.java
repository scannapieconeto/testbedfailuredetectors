package br.unicamp.ic.treplica.utils;

import java.net.UnknownHostException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class Replica {

    // Singleton.
    private static Replica instance;

    private int myId;
    private boolean isLeader;

    // Private constructor.
    private Replica() {
        this.myId = 0;

        try {
            final String localHostname = java.net.InetAddress.getLocalHost().getHostName();
            this.myId = Integer.parseInt(localHostname.substring(3, 5));
        } catch (UnknownHostException | NumberFormatException ex) {
            ex.printStackTrace();
        }

        this.isLeader = false;

        log.debug("Replica's id is " + this.myId);
    }

    // Singleton logic.
    public static synchronized Replica getInstance() {
        if (instance == null) {
            instance = new Replica();
        }

        return instance;
    }

    public Integer getMyId() {
        return this.myId;
    }

    public void setIsLeader() {
        log.debug("Replica is Leader!");
        this.isLeader = true;
    }

    public void setIsNotLeader() {
        log.debug("Replica is not Leader!");
        this.isLeader = false;
    }

    public boolean isLeader() {
        return this.isLeader;
    }
}
