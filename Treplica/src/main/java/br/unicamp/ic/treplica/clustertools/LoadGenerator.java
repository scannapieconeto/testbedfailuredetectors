/*
 * Cluster-Tools - Tools for running cluster experiments
 *
 * Copyright © 2008 Gustavo Maciel Dias Vieira
 *
 * This file is part of Cluster-Tools.
 *
 * Cluster-Tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.clustertools;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import br.unicamp.ic.treplica.TreplicaException;
import br.unicamp.ic.treplica.utils.FileHandler;
import br.unicamp.ic.treplica.utils.JarConfig;
import br.unicamp.ic.treplica.utils.JarConfig.LoadGeneratorConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoadGenerator {

    // Executor.
    private final ScheduledExecutorService loadGeneratorExecutor = Executors.newScheduledThreadPool(1);

    // Config class.
    private static final LoadGeneratorConfig LOAD_GENERATOR_CONFIG = JarConfig.INST.getLoadGenerator();

    // Config parameters.
    private static final String OUTPUT_FILE = LOAD_GENERATOR_CONFIG.getOutputFile();
    private static final String CONTENT_OUTPUT_FILE = LOAD_GENERATOR_CONFIG.getOutputFile() + ".content";
    private static final long REQUEST_RATE = 1_000_000 / LOAD_GENERATOR_CONFIG.getRequestRate();
    private static final long START_EXECUTION = LOAD_GENERATOR_CONFIG.getStartExecution() * 1_000;
    private static final long EXECUTION_DURATION = LOAD_GENERATOR_CONFIG.getExecDuration() * 1_000;

    /**
     * Constructor.
     */
    public LoadGenerator() {
        printLoadGeneratorConfig();
    }

    public void go(final ILoad load) throws IOException, InterruptedException, TreplicaException {
        try {
            // Warming up.
            warmUp();

            // Run executor.
            loadGeneratorExecutor.scheduleAtFixedRate(load, 0, REQUEST_RATE, TimeUnit.MICROSECONDS);

            // Thread sleep.
            Thread.sleep(EXECUTION_DURATION);

            // Wrapping up.
            wrapUp(load);
        } catch (Exception ex) {
            log.debug("" + ex.getMessage() + "\n");
            writeFile(OUTPUT_FILE, "" + ex.getMessage() + "\n");
        }
    }

    /*
     * Private Methods.
     */

    private void warmUp() throws InterruptedException {
        log.debug(String.format("Warming up (%s seconds)...\n", LOAD_GENERATOR_CONFIG.getStartExecution()));

        // Thread sleep.
        Thread.sleep(START_EXECUTION);

        log.debug("Started execution!\n\n");
    }

    private void wrapUp(final ILoad load) {
        log.debug("Wrapping up... \n");

        loadGeneratorExecutor.shutdown();
        log.debug("loadGeneratorExecutor.shutdown()!");

        flushLogs(load);

        // Graceful shutdown.
        load.shutdown();
        log.debug("load.shutdown()!");
    }

    private void printLoadGeneratorConfig() {
        final StringBuilder format = new StringBuilder("SETTINGS:\n").append("outputFile = %s \n")
                .append("requestRate = %s requests/second \n").append("executionDuration = %s seconds \n")
                .append("startExecution = %s seconds\n");

        final String content = String.format(format.toString(), OUTPUT_FILE, LOAD_GENERATOR_CONFIG.getRequestRate(),
                LOAD_GENERATOR_CONFIG.getExecDuration(), LOAD_GENERATOR_CONFIG.getStartExecution());

        log.debug(content.toString());
        writeFile(OUTPUT_FILE, content.toString());
    }

    private void flushLogs(final ILoad load) {
        try {
            // Flush log.
            writeFile(OUTPUT_FILE, "\n\n##### LOG ENTRIES #####\n\n");
            flushEntriesToFile(OUTPUT_FILE, load.getLogEntries());
            log.debug("flushEntriesToFile(load.getLogEntries())!");

            flushEntriesToFile(CONTENT_OUTPUT_FILE, load.getDataStructureContent());
            log.debug("flushEntriesToFile(load.getDataStructureContent())!");
        } catch (Exception e) {
            log.debug("Error flushing logs: " + e.getMessage());
        }
    }

    private void flushEntriesToFile(final String file, final List<String> logEntries) {
        for (final String content : logEntries) {
            writeFile(file, content);
        }
    }

    private void writeFile(final String file, final String content) {
        FileHandler.writeFile(file, true, content);
    }
}
