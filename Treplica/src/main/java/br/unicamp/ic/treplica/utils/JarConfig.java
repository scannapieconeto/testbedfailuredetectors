package br.unicamp.ic.treplica.utils;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import br.unicamp.ic.treplica.election.Election.FDAlgorithm;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class JarConfig {

    private static final String CONFIG_FILE = "jar-config.yml";
    public static final JarConfig INST;

    static {
        JarConfig paramsConfig = null;

        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            paramsConfig = mapper.readValue(new File(CONFIG_FILE), JarConfig.class);

            // Check if FD algorithm can be mapped to a valid enumerator.
            FDAlgorithm.valueOf(paramsConfig.getFailureDetector().getAlgorithm());
        } catch (Exception e) {
            e.printStackTrace();
        }

        INST = paramsConfig;
    }

    private TreplicaConfig treplica;
    private FailureDetectorConfig failureDetector;
    private OpponentConfig opponent;
    private LoadGeneratorConfig loadGenerator;

    // Private constructor;
    private JarConfig() {
    }

    @Data
    public class TreplicaConfig {
        private String paxosAlgorithm;
        private int roundTrip;
        private String ip;
        private int port;
    }

    @Data
    public class FailureDetectorConfig {
        private String algorithm;
        private int delta;
        private String ip;
        private int port;
        private String outputDir;
    }

    @Data
    public class OpponentConfig {
        private boolean treplicaOpponentEnabled;
        private boolean fdOpponentEnabled;
        private int delayToStabilizeElection;
        private boolean processFailureLeaderOnly;
        private int[] processStrategySampleSpace;
        private boolean networkFailureLeaderOnly;
        private String networkStrategy;
        private int[] networkStrategySampleSpace;
        private int networkStrategyMean;
        private int networkStrategyStandardDeviation;
    }

    @Data
    public class LoadGeneratorConfig {
        private int numberOfReplicas;
        private String stableMedia;
        private String outputFile;
        private long requestRate;
        private long execDuration;
        private long startExecution;
    }
}
