package br.unicamp.ic.treplica.election.larreaepoch;

import java.io.Serializable;

class LarreaEpochFDMsg implements Serializable {

    private static final long serialVersionUID = -4975364741643456808L;
    private final int sender;
    private final long epochNumber;

    LarreaEpochFDMsg(final int sender, final long epochNumber) {
        this.sender = sender;
        this.epochNumber = epochNumber;
    }

    int getSender() {
        return this.sender;
    }

    long getEpochNumber() {
        return this.epochNumber;
    }
}
