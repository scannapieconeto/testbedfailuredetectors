package br.unicamp.ic.treplica.transport.opponent;

import java.io.Serializable;
import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.transport.IMessageSender;
import br.unicamp.ic.treplica.transport.ITransportId;
import br.unicamp.ic.treplica.transport.MessageSenderDecorator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class OpponentMessageSenderDecorator extends MessageSenderDecorator {

    // Creates thread pool.
    private final ExecutorService threadPool = Executors.newCachedThreadPool();

    private final Opponent opponent;

    /**
     * Constructor.
     *
     * @param decoratedMessageSender
     *            decorated message sender.
     */
    public OpponentMessageSenderDecorator(final IMessageSender decoratedMessageSender) {
        super(decoratedMessageSender);
        opponent = Opponent.getInstance();

        log.debug("Built OpponentMessageSenderDecorator");
    }

    @Override
    public void sendMessage(final String mIP, final int mPort, final DatagramSocket uSocket, final Serializable message,
            final ITransportId destination) throws TreplicaSerializationException, TreplicaIOException {
        final OpponentCommand command = this.opponent.handleRequest();

        if (command.isDrop()) {
            log.trace("dropping sendMessage(...)");
            // Never sends message.
            return;
        } else if (command.getDelay() > 0) {
            log.trace("delaying sendMessage(...)");

            // Delays to send message. Asynchronous processing.
            threadPool.execute(new DelayedSender(mIP, mPort, uSocket, message, destination, command.getDelay()));
            return;
        }

        // Sends message normally.
        decoratedMessageSender.sendMessage(mIP, mPort, uSocket, message, destination);
    }

    @Override
    public void shutdown() {
        this.threadPool.shutdown();
    }

    /*
     * Private Members.
     */

    /**
     * Inner class responsible for delaying message sent.
     */
    private class DelayedSender implements Runnable {

        private final String mIP;
        private final int mPort;
        private final DatagramSocket uSocket;
        private final Serializable message;
        private final ITransportId destination;
        private final long delay;

        DelayedSender(final String mIP, final int mPort, final DatagramSocket uSocket,
                final Serializable message, final ITransportId destination, final long delay) {
            this.mIP = mIP;
            this.mPort = mPort;
            this.uSocket = uSocket;
            this.message = message;
            this.destination = destination;
            this.delay = delay;
        }

        @Override
        public void run() {
            try {
                // Delay message.
                sleep(this.delay);

                // Send message.
                decoratedMessageSender.sendMessage(mIP, mPort, uSocket, message, destination);
            } catch (TreplicaIOException | TreplicaSerializationException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void sleep(final long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
        }
    }
}
