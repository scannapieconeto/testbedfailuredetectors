/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

import java.io.EOFException;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map.Entry;
import java.util.TreeMap;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;

/**
 * This class implements a change log using the file system.
 * <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class DiskChangeLog implements ChangeLog {

    private static final String CHANGE_SUFFIX = "changes";
    private static final String CHECKPOINT_SUFFIX = "checkpoint";

    private File directory;
    private boolean open;
    private long nextCheckpoint;
    private long nextChangeBlock;
    private FileDescriptor changesFileDescriptor;
    private ObjectOutputStream changesStream;

    /**
     * Creates a new change log, that restores data from and saves data to the
     * provided directory on the file system.
     * <p>
     *
     * @param directory
     *            the directory where this change log stores its data.
     */
    public DiskChangeLog(final String directory) {
        this.directory = new File(directory);
        open = false;
    }

    /*
     * (non-Javadoc)
     *
     * @see ChangeLog#open(ChangeLogClient)
     */
    @Override
    public void open(final ChangeLogClient client) throws TreplicaIOException, TreplicaSerializationException {
        if (open) {
            throw new IllegalStateException();
        }
        nextCheckpoint = 1;
        nextChangeBlock = 0;
        loadLastCheckpoint(client);
        loadChangeBlocks(client);
        try {
            FileOutputStream fileStream = new FileOutputStream(
                    generateChangeBlockName(nextCheckpoint - 1, nextChangeBlock));
            changesFileDescriptor = fileStream.getFD();
            changesStream = new ObjectOutputStream(fileStream);
        } catch (IOException e) {
            throw new TreplicaIOException(e);
        }
        open = true;
    }

    /*
     * (non-Javadoc)
     *
     * @see ChangeLog#close()
     */
    @Override
    public void close() throws TreplicaIOException {
        if (!open) {
            throw new IllegalStateException();
        }
        open = false;
        try {
            changesStream.flush();
            changesFileDescriptor.sync();
            changesStream.close();
        } catch (IOException e) {
            throw new TreplicaIOException(e);
        }

        nextCheckpoint = 1;
        nextChangeBlock = 0;
        changesFileDescriptor = null;
        changesStream = null;
    }

    /*
     * (non-Javadoc)
     *
     * @see ChangeLog#isOpen()
     */
    @Override
    public boolean isOpen() {
        return open;
    }

    /**
     * Generates an appropriate name for the changes block file, using the
     * provided checkpoint and changes block sequence numbers.
     * <p>
     *
     * @param checkpoint
     *            the checkpoint sequence number.
     * @param changes
     *            the changes block sequence number.
     * @return The changes block file.
     */
    private File generateChangeBlockName(final long checkpoint, final long changes) {
        String name = directory.getPath() + File.separator + checkpoint + "-" + changes + "." + CHANGE_SUFFIX;
        return new File(name);
    }

    /**
     * Generates an appropriate name for the checkpoint file, using the provided
     * checkpoint sequence number.
     * <p>
     *
     * @param checkpoint
     *            the checkpoint sequence number.
     * @return The checkpoint file.
     */
    private File generateCheckpointName(final long checkpoint) {
        String name = directory.getPath() + File.separator + checkpoint + "." + CHECKPOINT_SUFFIX;
        return new File(name);
    }

    /**
     * Locates the latest checkpoint stored in the change log directory, loads
     * the checkpoint, applies it to the client and updates the local checkpoint
     * sequence number.
     * <p>
     *
     * @param client
     *            the client to receive the loaded checkpoint.
     * @throws TreplicaIOException
     *             if an I/O error occurs.
     * @throws TreplicaSerializationException
     *             if a serialization error occurs.
     */
    private void loadLastCheckpoint(final ChangeLogClient client)
            throws TreplicaIOException, TreplicaSerializationException {
        File[] files = directory.listFiles();
        if (files == null) {
            return;
        }
        long lastChkptSequence = 0;
        File lastChkptFile = null;
        for (int i = 0; i < files.length; i++) {
            String name = files[i].getName();
            if (name.matches("\\d+\\." + CHECKPOINT_SUFFIX + "$")) {
                long sequence = Long.parseLong(name.split("\\.", 2)[0]);
                if (sequence > lastChkptSequence) {
                    lastChkptSequence = sequence;
                    lastChkptFile = files[i];
                }
            }
        }
        if (lastChkptFile != null) {
            try (final ObjectInputStream stream = new ObjectInputStream(new FileInputStream(lastChkptFile))) {
                final Serializable checkpoint = (Serializable) stream.readObject();
                client.processCheckpoint(checkpoint);
            } catch (IOException e) {
                throw new TreplicaIOException(e);
            } catch (ClassNotFoundException e) {
                throw new TreplicaSerializationException(e);
            }
            nextCheckpoint = lastChkptSequence + 1;
        }
    }

    /**
     * Locates all the changes blocks to the current loaded checkpoint stored in
     * the change log directory, loads and applies them to the client and
     * updates the local change block sequence number. If an error occurs when
     * reading a change block, this error is considered to be deterministic and
     * is ignored.
     * <p>
     *
     * @param client
     *            the client to receive the loaded changes.
     */
    private void loadChangeBlocks(final ChangeLogClient client) {
        File[] files = directory.listFiles();
        if (files == null) {
            return;
        }
        long lastChkptSequence = nextCheckpoint - 1;
        TreeMap<Long, File> changeBlocks = new TreeMap<Long, File>();
        for (int i = 0; i < files.length; i++) {
            String name = files[i].getName();
            if (name.matches(lastChkptSequence + "-\\d+\\." + CHANGE_SUFFIX + "$")) {
                changeBlocks.put(new Long(name.split("[\\-.]", 3)[1]), files[i]);
            }
        }
        if (changeBlocks.size() > 0) {
            long sequence = -1;
            for (Entry<Long, File> entry : changeBlocks.entrySet()) {
                sequence = entry.getKey();
                try (final ObjectInputStream stream = new ObjectInputStream(new FileInputStream(entry.getValue()))) {
                    while (true) {
                        try {
                            Serializable change = (Serializable) stream.readObject();
                            client.processChange(change);
                        } catch (EOFException e) {
                            break;
                        }
                    }
                } catch (IOException e) {
                } catch (ClassNotFoundException e) {
                }
            }
            nextChangeBlock = sequence + 1;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see ChangeLog#writeChange(Serializable)
     */
    @Override
    public void writeChange(final Serializable change) throws TreplicaIOException {
        if (!open) {
            throw new IllegalStateException();
        }
        try {
            changesStream.writeObject(change);
            changesStream.flush();
            changesFileDescriptor.sync();
        } catch (IOException e) {
            close();
            throw new TreplicaIOException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see ChangeLog#writeCheckpoint(Serializable)
     */
    @Override
    public void writeCheckpoint(final Serializable checkpoint) throws TreplicaIOException {
        if (!open) {
            throw new IllegalStateException();
        }

        try {
            FileOutputStream fileStream = new FileOutputStream(generateCheckpointName(nextCheckpoint));
            ObjectOutputStream checkpointStream = new ObjectOutputStream(fileStream);
            checkpointStream.writeObject(checkpoint);
            checkpointStream.flush();
            fileStream.getFD().sync();
            checkpointStream.close();
        } catch (IOException e) {
            close();
            throw new TreplicaIOException(e);
        }

        nextCheckpoint++;
        nextChangeBlock = 0;
        try {
            changesStream.flush();
            changesFileDescriptor.sync();
            changesStream.close();
            FileOutputStream fileStream = new FileOutputStream(
                    generateChangeBlockName(nextCheckpoint - 1, nextChangeBlock));
            changesFileDescriptor = fileStream.getFD();
            changesStream = new ObjectOutputStream(fileStream);
        } catch (IOException e) {
            close();
            throw new TreplicaIOException(e);
        }
    }
}
