package br.unicamp.ic.treplica.election.larreaepoch;

import br.unicamp.ic.treplica.TreplicaIOException;
import br.unicamp.ic.treplica.TreplicaSerializationException;
import br.unicamp.ic.treplica.election.IFailureDetector;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.transport.IMessage;
import br.unicamp.ic.treplica.transport.ITransport;

/**
 * Failure detector algorithm based on paper 'Optimal Implementation of the
 * Weakest Failure Detector for Solving Consensus' (LarreaFernandezArevalo2000).
 */
public class LarreaEpochFD extends IFailureDetector {

    // Counters.
    private long myEpoch;
    private long leaderEpoch;

    // This variable can be accessed by more than one thread.
    private volatile long leaderTimestampInNanos;

    /**
     * Constructor.
     *
     * @param delta
     *            Delta in milliseconds.
     * @param client
     *            Election client.
     * @param transport
     *            Transport.
     */
    public LarreaEpochFD(final int delta, final ElectionClient client, final ITransport transport) {
        // Call super class constructor.
        super(delta, client, transport);

        setMyEpoch(client.getPaxosId());
        this.leaderEpoch = 0;

        this.leaderTimestampInNanos = System.nanoTime();
    }

    /*
     * IFailureDetector implementation.
     */

    @Override
    protected void onTimeoutEvent() throws TreplicaIOException, TreplicaSerializationException {
        if (this.myId == getLeader()) {
            /*
             * If this process is the current leader, increment its epoch and
             * multicast an I-AM-ALIVE message.
             */
            this.leaderEpoch = ++this.myEpoch;

            this.transport.sendMessage(new LarreaEpochFDMsg(this.myId, this.myEpoch));
        } else if ((System.nanoTime() - this.leaderTimestampInNanos) > this.onTimeoutDeltaInNanos) {
            /*
             * If leader's message does not arrive until the timeout, the
             * process considers itself leader.
             */
            this.leaderEpoch = this.myEpoch;
            setLeader(this.myId);

            this.transport.sendMessage(new LarreaEpochFDMsg(this.myId, this.myEpoch));
        }
    }

    @Override
    protected void onMessageEvent(final IMessage message) throws TreplicaIOException, TreplicaSerializationException {
        final LarreaEpochFDMsg messageFD = (LarreaEpochFDMsg) message.getPayload();
        final int msgSender = messageFD.getSender();
        final long msgEpoch = messageFD.getEpochNumber();

        /*
         * If a message, from a process with higher epoch, arrives, considers
         * this process the current leader.
         *
         * If a message, from a process with the same epoch and lower id,
         * arrives, considers this process the current leader.
         */
        if ((msgEpoch > this.leaderEpoch && msgSender != getLeader())
                || (msgEpoch == this.leaderEpoch && msgSender < getLeader())) {
            setLeader(msgSender);
        }

        if (msgSender == getLeader()) {
            this.leaderEpoch = msgEpoch;
            this.leaderTimestampInNanos = System.nanoTime();
        }
    }

    /**
     * IMPORTANT: Giving an advantage to processes '14' and '15' in order to simplify
     * the metrics analysis.
     */
    private void setMyEpoch(final int processId) {
        switch (processId) {
            case 14:
                this.myEpoch = 20_000_000;
                break;

            case 15:
                this.myEpoch = 10_000_000;
                break;

            default:
                this.myEpoch = 0;
        }
    }
}
