/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.transport;

import java.io.Serializable;

/**
 * This interface represents a transport id, used to identify an endpoint of a
 * transport. Any concrete implementation of transport id has to be comparable
 * (consistent with equals) to establish a total order among the endpoints and
 * it must be serializable so it can be send inside messages.
 * <p>
 *
 * To create a comparable class fully consistent with equals it is necessary to
 * provide implementations for the <code>compareTo</code>, <code>equals</code>
 * and <code>hashCode</code> methods.
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface ITransportId extends Serializable, Comparable<ITransportId> {

}
