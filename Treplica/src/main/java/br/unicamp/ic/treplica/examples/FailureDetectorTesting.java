package br.unicamp.ic.treplica.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import br.unicamp.ic.treplica.election.Election;
import br.unicamp.ic.treplica.election.Election.FDAlgorithm;
import br.unicamp.ic.treplica.paxos.ElectionClient;
import br.unicamp.ic.treplica.utils.Replica;

public final class FailureDetectorTesting {

    // Private constructor.
    private FailureDetectorTesting() {
    }

    // Entry point
    public static void main(final String... args) throws UnknownHostException {
        if (args.length != 3) {
            System.out.println("Usage: FailureDetectorTesting <maxProcesses> <fdAlgorithm> <fdDelta>\n"
                    + "       <maxProcesses> - max number of processes\n"
                    + "       <fdAlgorithm>   - failure detector algorithm:\n"
                    + "                      LARREA_VANILLA\n"
                    + "                      LARREA_EPOCH\n"
                    + "                      AGUILERA\n"
                    + "                      CHEN\n"
                    + "       <fdDelta> - failure detector delta");

            System.exit(0);
        }

        final int processId = Replica.getInstance().getMyId();
        System.out.println("Process id is: " + processId);

        final FakeElectionClient fakeElectionClient = new FakeElectionClient(processId);
        final int maxProcesses = Integer.parseInt(args[0]);

        final FDAlgorithm fdAlgorithm = FDAlgorithm.valueOf(args[1]);
        System.out.println("Current FD algorithm is: " + fdAlgorithm);

        final int fdDelta = Integer.parseInt(args[2]);

        new Election(fdAlgorithm, fdDelta, fakeElectionClient, maxProcesses);

        // Reads terminal commands.
        terminalReader();
    }

    private static void terminalReader() {
        final BufferedReader terminal = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");

            String command;
            try {
                command = terminal.readLine();
            } catch (IOException e) {
                continue;
            }

            if (command == null || command.length() == 0) {
                continue;
            }

            if ("q".equals(command)) {
                System.exit(0);
            } else {
                System.out.println("Type 'q' to exit!");
            }
        }
    }

    private static class FakeElectionClient implements ElectionClient {
        private final int paxosId;

        FakeElectionClient(final int paxosId) {
            this.paxosId = paxosId;
        }

        @Override
        public void leaderChange(final int newLeader) {
            System.out.println(String.format("My (%1$s) current leader is: %2$s", getPaxosId(), newLeader));
        }

        @Override
        public int getPaxosId() {
            return this.paxosId;
        }
    }
}
