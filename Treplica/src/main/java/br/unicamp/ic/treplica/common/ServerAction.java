/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.ic.treplica.common;

/**
 * @author Luiz Eduardo Buzato
 */

public enum ServerAction {

    WRITE_CHANGE(1),
    WRITE_CHECKPOINT(2),
    LOAD_LAST_CHECKPOINT(3),
    LOAD_CHANGE_BLOCKS(4),
    INIT(5),
    WAITING(6);

    private final int value;

    ServerAction(final int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

}
