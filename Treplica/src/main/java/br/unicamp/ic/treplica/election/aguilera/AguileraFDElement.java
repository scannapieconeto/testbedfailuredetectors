package br.unicamp.ic.treplica.election.aguilera;

import java.util.Comparator;

class AguileraFDElement {

    private final int processId;

    // This variable is accessed by more than one thread at the same time.
    private volatile long counter;

    private long previousCounter;
    private boolean isTrustworthy;

    AguileraFDElement(final int processId) {
        this.processId = processId;

        this.counter = 0;
        this.previousCounter = 0;
        this.isTrustworthy = true;
    }

    int getProcessId() {
        return this.processId;
    }

    void incrementCounter() {
        ++this.counter;
    }

    boolean isTrustworthy() {
        return this.isTrustworthy;
    }

    void updateTrustworthy() {
        this.isTrustworthy = ((this.counter - this.previousCounter) > 0);
        this.previousCounter = this.counter;
    }

    /**
     * Inner class. IdComparator.
     */
    static class IdComparator implements Comparator<AguileraFDElement> {

        @Override
        public int compare(final AguileraFDElement element1, final AguileraFDElement element2) {
            // Applying the ascending order, so that the process with lower
            // identifier is the first one.
            return Long.compare(element1.getProcessId(), element2.getProcessId());
        }

    }
}
