## Generate the Testbed Failure Detector JAR file

1. Clone this repository by running **git clone https://scannapieconeto@bitbucket.org/scannapieconeto/testbedfailuredetectors.git**.
2. Download and install Maven (https://maven.apache.org/). Make sure the JAVA_HOME environment variable is properly set.
3. Run the command **mvn clean package -DskipTests** inside directory **testbedfailuredetectors/Treplica**
4. The JAR file generated is **testbedfailuredetectors/Treplica/target/treplica-jar-with-dependencies.jar**

---

## Edit the Testbed Failure Detector config file

Edit file **testbedfailuredetectors/Treplica/jar-config.yml** with a special attention to the following parameters:

- **algorithm: "${FAILURE_DETECTOR_ALGORITHM}"** failure detector possible algorithms: LARREA_VANILLA, LARREA_EPOCH, AGUILERA, CHEN.
- **delta: ${FAILURE_DETECTOR_DELTA}** failure detector delta (in milliseconds).
- **treplicaOpponentEnabled: ${TREPLICA_OPPONENT_ENABLED}** indicates whether the treplica opponent is enabled or disabled.
- **fdOpponentEnabled: ${FD_OPPONENT_ENABLED}** indicates whether the failure detector opponent is enabled or disabled.
- **processFailureLeaderOnly: ${PROCESS_FAILURE_LEADER_ONLY}** if true, only the current leader's process will fail. Otherwise, all processes fail.
- **processStrategySampleSpace: ${PROCESS_OPPONENT_SAMPLE_SPACE}** indicates the frequency the leader will fail and recover (in seconds). Use only non-negative values.
- **networkFailureLeaderOnly: ${NETWORK_FAILURE_LEADER_ONLY}** if true, only the current leader's network will fail. Otherwise, all processes fail.
- **networkStrategy: "${OPPONENT_NETWORK_STRATEGY}"** network strategy possible values: NO_NETWORK_STRATEGY, ENUMERATED_DISTRIBUTION_NETWORK_STRATEGY, NORMAL_DISTRIBUTION_NETWORK_STRATEGY
- **networkStrategySampleSpace: ${NETWORK_OPPONENT_SAMPLE_SPACE}** (for ENUMERATED_DISTRIBUTION_NETWORK_STRATEGY and NORMAL_DISTRIBUTION_NETWORK_STRATEGY) possible delay/loss values (in milliseconds). Negative discrete values means drop and 0 means no delay whatsoever.
- **networkStrategyMean: ${NETWORK_OPPONENT_MEAN}** (for NORMAL_DISTRIBUTION_NETWORK_STRATEGY) mean and stardard deviation (in milliseconds).
- **networkStrategyStandardDeviation: ${NETWORK_OPPONENT_STANDARD_DEVIATION}** (for NORMAL_DISTRIBUTION_NETWORK_STRATEGY) mean and stardard deviation (in milliseconds).
- **execDuration: ${ITERATION_DURATION}** experiment iteration execution duration (in seconds).

---

## Machines Setup

The Testbed Failure Detector was developed to run on Linux-based machines. Make sure all machines used meet the instructions below:

1. Have NTP (Network Time Protocol) service running.
2. Have IP Multicast configured.
3. Have all directories specified in **testbedfailuredetectors/Treplica/jar-config.yml** created.

---

## Run the Testbed Failure Detector

In order to run the Testbed Failure Detector execute the following steps:

1. Copy both generated **testbedfailuredetectors/Treplica/target/treplica-jar-with-dependencies.jar** and edited **testbedfailuredetectors/Treplica/jar-config.yml** to a directory on the Linux machine.
2. Run command **java -cp treplica-jar-with-dependencies.jar br.unicamp.ic.treplica.clustertools.ReplicatedMapLG** on each machine of the cluster.

Further details of all the commands executed before and after each experiment, including the ones used to extract the Quality of Service metrics, can be seen in directory **testbedfailuredetectors/Treplica/ansible**, specially files **run_experiment.yml** and **run_experiment_iteration.yml**
